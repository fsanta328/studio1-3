﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OpponentController : PlayerControls
{
	private float m_lastUpdateTime;
	private float m_timePerUpdate = 0.16f;
	public Rigidbody m_opponentRigidbody;
	private Vector3 m_lastVel;

	override public void Start()
	{
		base.Start ();
		m_opponentRigidbody = this.GetComponent <Rigidbody> ();
	}

	override public void Update()
	{
		if (m_currentState == PlayerState.Dead)
			return;

		//play character running anim
		if (m_lastVel.magnitude >= .5f)
		{
			m_playerScript.SetAnimation (Animate.Walk);
		}

		float animFloat = m_playerScript.m_animator.GetFloat ("PlayerAnim");

		//play idle anim
		if ((m_lastVel == Vector3.zero) && (animFloat<3)) 
		{
			m_playerScript.SetAnimation (Animate.Idle);
		}

		//interpolation
		float finishRate = (Time.time - m_lastUpdateTime) / m_timePerUpdate;

		if (finishRate <= 1.0) 
		{
			transform.position = Vector3.Lerp (m_startPos, m_destinationPos, finishRate);
			transform.rotation = Quaternion.Slerp (m_startRot, m_destinationRot, finishRate);
		}

		//extrapolation
//		else 
//		{
//			transform.position = transform.position + (m_lastVel * Time.deltaTime);
//		}

//		transform.position = m_destinationPos;
//		transform.rotation = m_destinationRot;
	}

	public void SetOpponentPosition (float posX, float posY, float posZ, float velX, float velZ, float rotY)
	{
		m_startPos = transform.position;
		m_startRot = transform.rotation;
		m_destinationPos = new Vector3 (posX, posY, posZ);
		//m_destinationRot = Quaternion.Euler (0, Mathf.Rad2Deg*rotY, 0);
		//Debug.Log ("opponent rotation is " + Mathf.Rad2Deg*rotY);
		print ("host setting opponent rotation" + transform.rotation.y);

		m_lastVel = new Vector3 (velX, 0, velZ);

		if (m_lastVel.sqrMagnitude != 0)
		{
			m_destinationRot = Quaternion.LookRotation (m_lastVel.normalized); 
		}
		m_lastUpdateTime = Time.time;
	}

//	internal void SetAnimation(Animate a_animate)
//	{
//		m_playerScript.m_animator().SetFloat ("PlayerAnim", (int)a_animate);	
//	}

//	public override void Dead ()
//	{
//		if (!Is_playerAlive ()) 
//		{
//			m_playerScript.SetAnimation (Animate.Die);
//
//		}
//		return;
//	}
		
//	public override void TakenDamage (float a_DamageAmount, int aIndexOfPlayer)
//	{
//		gameObject.GetComponent <PlayerControls> ().SetAnimation (Animate.Injured);
//
//		Debug.Log ("health remaining" + m_currentHealth);
//		if (!Is_playerAlive ()) 
//		{
//			m_currentHealth = 0;
//		}
//
//		else 
//		{
//			m_currentHealth -= a_DamageAmount;
//			//SetHealthBar();
//		}
//	}

	public override void SetHealthBar ()
	{
		float BarHealth = m_currentHealth / m_MaxPlayerHealth;
		m_HealthBar.transform.localScale = new Vector3 (Mathf.Clamp (BarHealth,0f,1f) , m_HealthBar.transform.localScale.y,m_HealthBar.transform.localScale.z);
	}

	public void HidePlayer() 
	{
		gameObject.GetComponent <SkinnedMeshRenderer>().enabled = false;
		gameObject.SetActive (false);
	}

//	override public void PlayPickUpAnim(GameObject x)
//	{
//		Debug.Log ("playing pickup" + x.name);
//		m_playerScript.m_animator().ResetTrigger ("Throw");
//
//		// character will perform pick up animation..
//		m_playerScript.SetAnimation (Animate.PickUp);
//		//m_playerScript.m_animator().SetFloat ("PlayerAnim", 3.5f);
//		Debug.Log ("played pickup" + name);		
//		m_playerScript.m_crate = x;
//		PickBox ();
//	}

	//void getBytes()
	//this function will return pos,rot in bytes
	override public List<byte> GetBytes()
	{
		m_myByteList.Clear ();

		//1 byte message version
		//1 byte type of message
		//24 bytes for 6 floats
		//26 bytes total per message
		m_myByteList.Add (m_messageVersion);
		m_myByteList.Add ((byte)'U');
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.position.x));
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.position.y));
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.position.z));
		m_myByteList.AddRange (System.BitConverter.GetBytes (m_lastVel.x));
		m_myByteList.AddRange (System.BitConverter.GetBytes (m_lastVel.z));
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.rotation.y));
		print ("opponent rotation" + transform.rotation.y);
		//MultiplayerManager.Instance.ShowMPStatus ("converted opponent bytes");
		return m_myByteList;
	}

	//readBytes(byte[])
	//change byte array into pos, rot, etc
	override public void ReadBytes (byte[] a_byteMessage, int a_startPoint)
	{
		//read through byte[] grab correct parts to udpates myself
		//offset startpoint by the section it is in * number of bytes to skip
		a_startPoint +=2;

		//Debug.Log (a_byteMessage.Length+ "importantinfo for client   "+ a_section);
		float xPos = System.BitConverter.ToSingle (a_byteMessage, a_startPoint);
		float yPos = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 4);
		float zPos = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 8);
		float xVel = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 12);
		float zVel = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 16);
		float yRot = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 20);

		m_startPos = transform.position;
		m_startRot = transform.rotation;
		m_destinationPos = new Vector3 (xPos, yPos, zPos);
		m_lastVel = new Vector3 (xVel, 0, zVel);

		if (m_lastVel.sqrMagnitude != 0)
		{
			m_destinationRot = Quaternion.LookRotation (m_lastVel.normalized); 
		}
		m_lastUpdateTime = Time.time;
		//m_destinationRot = Quaternion.LookRotation (m_lastVel.normalized); 
		//Quaternion.Euler (0, Mathf.Rad2Deg*yRot, 0);
	}
}
