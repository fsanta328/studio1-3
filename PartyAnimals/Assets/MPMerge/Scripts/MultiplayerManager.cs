﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi.Multiplayer;

//using UnityEngine.SocialPlatforms;
//using GooglePlayGames.BasicApi;

public class MultiplayerManager : RealTimeMultiplayerListener
{
	private static MultiplayerManager m_instance = null;

	private uint m_minOpponents2 = 1;
	private uint m_maxOpponents2 = 1;

	private uint m_minOpponents4 = 3;
	private uint m_maxOpponents4 = 3;

	private uint m_gameMode = 0;

	public string m_gameHost;

	public MPLobbyListener m_lobbyListener;

	private byte _protocolVersion = 1;
	// Byte + Byte + 3 floats for position + 2 floats for velcocity + 1 float for rotZ
	private int m_updateMessageLength = 26;
	private List<byte> m_updateMessage;

	public MPUpdateListener m_updateListener;
	private int m_roundsPlayed = 0;
	private int m_maxRounds = 3;
	public bool m_twoPlayer = true;

	private MultiplayerManager ()
	{
		m_updateMessage = new List<byte> (m_updateMessageLength);
		//PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder ();
		PlayGamesPlatform.DebugLogEnabled = true;
		PlayGamesPlatform.Activate ();
	}

	public static MultiplayerManager Instance 
	{
		get 
		{
			if (m_instance == null) 
			{
				m_instance = new MultiplayerManager();
			}
			return m_instance;
		}
	}

	//start game for 2 players
	public void SignInAndStartMPGame2() 
	{
		if (! PlayGamesPlatform.Instance.localUser.authenticated) 
		{
			PlayGamesPlatform.Instance.localUser.Authenticate((bool success) => 
				{
					if (success) 
					{
						ShowMPStatus (("We're signed in! Welcome " + PlayGamesPlatform.Instance.localUser.userName));
						m_twoPlayer = true;
						StartMatchMaking2();	
					}

					else
					{
						ShowMPStatus("Player is not signed in");
						Debug.Log ("player is not signed in.");
					}
				});
		} 

		else 
		{
			ShowMPStatus ("You're already signed in.");
			Debug.Log ("You're already signed in.");
			m_twoPlayer = true;
			StartMatchMaking2();	
		}
	}

	//start game for 4 players
	public void SignInAndStartMPGame4() 
	{
		if (! PlayGamesPlatform.Instance.localUser.authenticated) 
		{
			PlayGamesPlatform.Instance.localUser.Authenticate((bool success) => 
				{
					if (success) 
					{
						ShowMPStatus (("We're signed in! Welcome " + PlayGamesPlatform.Instance.localUser.userName));
						m_twoPlayer = false;
						StartMatchMaking4();	
					}

					else
					{
						ShowMPStatus("Player is not signed in");
						Debug.Log ("player is not signed in.");
					}
				});
		} 

		else 
		{
			ShowMPStatus ("You're already signed in.");
			Debug.Log ("You're already signed in.");
			m_twoPlayer = false;
			StartMatchMaking4();	
		}
	}

	public void SignOut() 
	{
		ShowMPStatus ("User signed out");
		PlayGamesPlatform.Instance.SignOut ();
	}

	public bool IsAuthenticated() 
	{
		return PlayGamesPlatform.Instance.localUser.authenticated;
	}

	public void TrySilentSignIn() 
	{
		if (! PlayGamesPlatform.Instance.localUser.authenticated) 
		{
			PlayGamesPlatform.Instance.Authenticate ((bool success) => 
				{
					if (success) 
					{
						ShowMPStatus ("Silently signed in! Welcome " + PlayGamesPlatform.Instance.localUser.userName);
						Debug.Log ("Silently signed in! Welcome " + PlayGamesPlatform.Instance.localUser.userName);
					} 

					else 
					{
						ShowMPStatus ("silent sign in failed.");
						Debug.Log ("silent sign in failed.");
					}
				}, true);
		} 

		else 
		{
			ShowMPStatus ("already silent sign in");
			Debug.Log("already silent sign in");
		}
	}

	private void StartMatchMaking2() 
	{
		PlayGamesPlatform.Instance.RealTime.CreateQuickGame (m_minOpponents2, m_maxOpponents2, m_gameMode, this);
	}

	private void StartMatchMaking4() 
	{
		PlayGamesPlatform.Instance.RealTime.CreateQuickGame (m_minOpponents4, m_maxOpponents4, m_gameMode, this);
	}

	private void StartInviteSession()
	{
		PlayGamesPlatform.Instance.RealTime.CreateWithInvitationScreen (m_minOpponents2, m_maxOpponents2, m_gameMode, this);
	}

	//revert to private
	public void ShowMPStatus(string message)
	{
		//Debug.Log(message);
		if (m_lobbyListener != null) 
		{
			m_lobbyListener.SetLobbyStatusMessage(message);
		}
	}

	public void OnRoomSetupProgress (float percent)
	{
		ShowMPStatus ("We are " + percent + "% done with setup");
	}

	public void OnRoomConnected (bool success)
	{
		if (success) 
		{
			ShowMPStatus ("Room connection sucessful!");
			//m_lobbyListener.HideLobby ();
			//m_lobbyListener = null;
			SetRoomHost ();
			SceneManager.LoadScene (1);
		} 

		else
		{
			ShowMPStatus ("Error connecting to the room.");
		}
	}

	public void OnLeftRoom ()
	{
		ShowMPStatus("We have left the room.");
		if (m_updateListener != null) 
		{
			m_updateListener.LeftRoomConfirmed();
		}
	}

	public void OnParticipantLeft (Participant participant)
	{
		//disable participant character?
		//check if all participants left end game
	}

	public void OnPeersConnected (string[] participantIds)
	{
		foreach (string participantID in participantIds)
		{
			ShowMPStatus ("Player " + participantID + " has joined.");
			m_updateListener.PlayerLeftRoom (participantID);
		}
	}

	public void OnPeersDisconnected (string[] participantIds)
	{
		foreach (string participantID in participantIds) 
		{
			ShowMPStatus ("Player " + participantID + " has left.");
		}
	}

	public void OnRealTimeMessageReceived (bool isReliable, string senderId, byte[] data)
	{
		//ShowMPStatus ("We have received some gameplay messages from participant ID:" + senderId);
		//byte messageVersion = (byte)data[0];
		//if (messageVersion != currentBuildVersion) 
		//{
			//update game
		//}
		//char messageType = (char)data[1];

		//check if i am host and decrypt bytes of player info
		//apply this information to my game
		if (senderId != GetAllPlayers () [0].ParticipantId)//Host
		{
			//ShowMPStatus ("recieved message from client");
			//cratecrash singleton instance.HostNetworkUpdate(data);
			CrateCrashManager.Instance.HostNetworkUpdate (data, senderId);
		}

		else //client
		{ 
			//ShowMPStatus ("recieved message from host");
			//cratecrash singleton instance.ClientNetworkUpdate(data);
			CrateCrashManager.Instance.ClientNetworkUpdate (data, isReliable);
			//			if (m_updateListener != null) 
			//			{
			//ShowMPStatus (posX + " " + posY + " " + posZ);
			//Debug.Log ("Recieved update" + " " + posX + " "+ posY + " "+ posZ);
			//m_updateListener.UpdateReceived(senderId, posX, posY, posZ, velX, velZ, rotY);
			//			}
		}
	}

	public List<Participant> GetAllPlayers()
	{
		return PlayGamesPlatform.Instance.RealTime.GetConnectedParticipants ();
	}

	public string GetMyParticipantID()
	{
		return  PlayGamesPlatform.Instance.RealTime.GetSelf().ParticipantId;
	}

	// method to send information about the player
	//	public void SendMyUpdate(float a_posX, float a_posY, float a_posZ, float a_velX, float a_velZ, float a_rotY) 
	//	{
	//		//ShowMPStatus ("client sending message to host");
	//		m_updateMessage.Clear ();
	//		m_updateMessage.Add (_protocolVersion);
	//
	//		//type of message u Update
	//		m_updateMessage.Add ((byte)'U');
	//		m_updateMessage.AddRange (System.BitConverter.GetBytes (a_posX)); 
	//		m_updateMessage.AddRange (System.BitConverter.GetBytes (a_posY));
	//		m_updateMessage.AddRange (System.BitConverter.GetBytes (a_posZ));
	//		m_updateMessage.AddRange (System.BitConverter.GetBytes (a_velX));
	//		m_updateMessage.AddRange (System.BitConverter.GetBytes (a_velZ));
	//		m_updateMessage.AddRange (System.BitConverter.GetBytes (a_rotY));
	//		byte[] messageToSend = m_updateMessage.ToArray();
	//		//Debug.Log ("My update sent" + " " + a_posX + " "+ a_posY + " "+ a_posZ); 
	//
	//		//MESH NETWORK MESSAGE
	//		//PlayGamesPlatform.Instance.RealTime.SendMessageToAll (false, messageToSend);
	//
	//		//HOST NETWORK MESSAGE
	//		PlayGamesPlatform.Instance.RealTime.SendMessage(false, m_gameHost, messageToSend);
	//	}

	//	public void SendHostUpdateToClients(float a_posX1, float a_posY1, float a_posZ1, float a_velX1, float a_velZ1, float a_rotY1, 
	//								float a_posX2, float a_posY2, float a_posZ2, float a_velX2, float a_velZ2, float a_rotY2)
	//	{
	//		ShowMPStatus ("host sending message");
	//
	//		m_updateMessage.Clear ();
	//		m_updateMessage.Add (_protocolVersion);
	//
	//		m_updateMessage.Add ((byte)'U');
	//
	//		//encryption for host information
	//		m_updateMessage.AddRange (System.BitConverter.GetBytes (a_posX1)); 
	//		m_updateMessage.AddRange (System.BitConverter.GetBytes (a_posY1));
	//		m_updateMessage.AddRange (System.BitConverter.GetBytes (a_posZ1));
	//		m_updateMessage.AddRange (System.BitConverter.GetBytes (a_velX1));
	//		m_updateMessage.AddRange (System.BitConverter.GetBytes (a_velZ1));
	//		m_updateMessage.AddRange (System.BitConverter.GetBytes (a_rotY1));
	//
	//		//encryption of opponent #1 info
	//		m_updateMessage.AddRange (System.BitConverter.GetBytes (a_posX2)); 
	//		m_updateMessage.AddRange (System.BitConverter.GetBytes (a_posY2));
	//		m_updateMessage.AddRange (System.BitConverter.GetBytes (a_posZ2));
	//		m_updateMessage.AddRange (System.BitConverter.GetBytes (a_velX2));
	//		m_updateMessage.AddRange (System.BitConverter.GetBytes (a_velZ2));
	//		m_updateMessage.AddRange (System.BitConverter.GetBytes (a_rotY2));
	//
	//		byte[] messageToSend = m_updateMessage.ToArray();
	//		PlayGamesPlatform.Instance.RealTime.SendMessageToAll (false, messageToSend);
	//	}

	public void SendHostUpdateToAll(byte[] a_messageToSend)
	{
		PlayGamesPlatform.Instance.RealTime.SendMessageToAll (false, a_messageToSend);
	}

	public void SendClientUpdate(byte[] a_messageToSend)
	{
		PlayGamesPlatform.Instance.RealTime.SendMessage (false, m_gameHost, a_messageToSend);
	}

	public void SendRelaibaleClientUpdate (byte[] a_messageToSend)
	{
		PlayGamesPlatform.Instance.RealTime.SendMessage (true, m_gameHost, a_messageToSend);
	}

	public void SendRelaibaleHostUpdateToAll (byte[] a_messageToSend)
	{
		PlayGamesPlatform.Instance.RealTime.SendMessageToAll (true, a_messageToSend);
	}

	public void SetRoomHost()
	{
		m_gameHost = GetAllPlayers ()[0].ParticipantId;
	}

	public void LeaveGame() 
	{
		PlayGamesPlatform.Instance.RealTime.LeaveRoom ();
	}

	public void RoundCount()
	{
		m_roundsPlayed++;
		if (m_roundsPlayed > m_maxRounds) 
		{
			//call function in cratecrashMNGR to show final standings
			//leave room?
		}
	}
	//make funciton to keep track of round count also 
	//also add a check inside if round count is bigger than X end match
}