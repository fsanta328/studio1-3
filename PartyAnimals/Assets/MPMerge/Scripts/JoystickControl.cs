﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public enum Dimension
{
	ThirdDimension,
	SecondDimension,
};

public class JoystickControl : MonoBehaviour , IDragHandler, IPointerUpHandler, IPointerDownHandler
{
	private Image bgImg;
	private Image stickImg;
	public Vector3 inputDirection { set; get;}
	public Dimension a_dimension;

	private void Start()
	{
		bgImg = GetComponent <Image> ();
		stickImg = transform.GetChild(0).GetComponent <Image> ();
	}
	public virtual void OnDrag (PointerEventData eventData)
	{
		Vector2 pos = Vector2.zero;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle (bgImg.rectTransform, eventData.position, eventData.pressEventCamera, out pos)) 
		{
			pos.x = (pos.x / bgImg.rectTransform.sizeDelta.x);
			pos.y = (pos.y / bgImg.rectTransform.sizeDelta.y);

			float x = (bgImg.rectTransform.pivot.x == 1) ? pos.x * 2 + 1 : pos.x * 2 - 1;
			float y = (bgImg.rectTransform.pivot.y == 1) ? pos.y * 2 + 1 : pos.y * 2 - 1;

			switch (a_dimension) 
			{
			case Dimension.ThirdDimension:
				{
					inputDirection = new Vector3 (x, 0, y);
					inputDirection = (inputDirection.magnitude > 1) ? inputDirection.normalized : inputDirection;

					stickImg.rectTransform.anchoredPosition = new Vector3 (inputDirection.x * (bgImg.rectTransform.sizeDelta.x / 3),
						inputDirection.z * (bgImg.rectTransform.sizeDelta.y / 3));
					break;
				}
			case Dimension.SecondDimension:
				{
					inputDirection = new Vector3 (x, y, 0);
					inputDirection = (inputDirection.magnitude > 1) ? inputDirection.normalized : inputDirection;

					stickImg.rectTransform.anchoredPosition = new Vector3 (inputDirection.x * (bgImg.rectTransform.sizeDelta.x / 3),
						inputDirection.y * (bgImg.rectTransform.sizeDelta.y / 3));
					break;
				}
			}
		}	 
	}

	public virtual void OnPointerDown (PointerEventData eventData)
	{
		OnDrag (eventData);
	}

	public virtual void OnPointerUp (PointerEventData eventData)
	{
		inputDirection = Vector3.zero;
		stickImg.rectTransform.anchoredPosition = Vector3.zero;
	}

	public Vector3 GetPosition()
	{
		return inputDirection;
	}
}
