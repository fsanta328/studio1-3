﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GooglePlayGames.BasicApi.Multiplayer;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum EventType
{
	begin,//b use to sync when to begin match
	pickup,//p
	throwing,//t
	punch,//a
	death,//d
	special,//s switch for what type of powerup it is
	constricted,//c switch for when hit by a power up to constrict movement
	injured,//i
	leaderboard,//l
	end,//e
};

public class CrateCrashManager : MonoBehaviour , MPUpdateListener
{
	// MAKE INTERFACE FOR GAME MANAGERS and have this class implement it

	public GameObject[] m_playerPrefab;
	public GameObject[] m_opponentPrefab;

	public GameObject m_myMPCharacter;
	private Rigidbody m_myMPRigidbody;

	private float m_timeRemaining;
	public Transform[] m_playerSpawnPoints;

	private bool m_multiplayerReady;
	public string m_myParticipantID;
	//private Vector3 m_startPos = new Vector3 (10, 1, 5);
	//private float m_offset = 3f;
	private Dictionary<string, OpponentController> m_opponentScripts;
	private float m_nextBroadcastTime = 0;
	private List<byte> m_listToConvert = new List<byte>();

	private PlayerControls m_playerControls;

	public List <Participant> a_allPlayers;
	public bool m_isHost = false;
	//private List <GameObject> m_crates;
	//private List <GameObject> m_nCrates;
	//private List <GameObject> m_sCrates;
	private byte m_messageVersion = 1;
	private static CrateCrashManager m_ccInstance;

	public static CrateCrashManager Instance { get { return m_ccInstance; } }

	//NEW added
	public Image[] m_healthBars;
	//public int a_playersAlive;
	public int playersReady;
	public List<int> playersList;
	public List<int> m_losersList;
	public bool m_matchReady = false;
	public GameUI m_gameUI;
	//string[] m_levelNames = { "Cave", "Forest", "Fridge" };

	//make a singleton for crateCrashManager
	private void Awake()
	{
		if (m_ccInstance != null && m_ccInstance != this)
		{
			Destroy(this.gameObject);
		}

		else
		{
			m_ccInstance = this;
		}
	}

	void Start()
	{
		//a_playersAlive = 0;
		playersReady = 0;
		playersList = new List<int> ();
		m_losersList = new List<int> ();
		SetUpMultiPlayerGame ();
		m_timeRemaining = 60f;
	}

	void SetUpMultiPlayerGame()
	{
		Debug.Log ("start size list" + playersList.Count);
		MultiplayerManager.Instance.m_updateListener = this;
		m_myParticipantID = MultiplayerManager.Instance.GetMyParticipantID ();
		a_allPlayers = MultiplayerManager.Instance.GetAllPlayers ();
		Debug.Log("all player size" + a_allPlayers.Count);

		m_opponentScripts = new Dictionary<string, OpponentController> (a_allPlayers.Count - 1);

		for (int i = 0; i < a_allPlayers.Count; i++) 
		{
			string a_nextParticipantID = a_allPlayers [i].ParticipantId;
			//a_playersAlive++;
			playersList.Add (i);
			//check if this ID is my ID
			//instantiate my player in scene
			if (a_nextParticipantID == m_myParticipantID)
			{
				m_myMPCharacter = (Instantiate (m_playerPrefab[i], m_playerSpawnPoints[i].position, Quaternion.identity) as GameObject);
				m_myMPRigidbody = m_myMPCharacter.GetComponent <Rigidbody> ();
				m_playerControls = m_myMPCharacter.GetComponent <PlayerControls> ();
				//MultiplayerManager.Instance.ShowMPStatus ("I Spawned");
				//m_myMPCharacter.tag = "Player";

				//check if particpant ID is first in list, assign as "host"
				if (a_nextParticipantID == a_allPlayers [0].ParticipantId) 
				{
					m_isHost = true;
					crateCrash.Instance.InstantiateCrates ();
					//foreach (GameObject item in crateCrash.Instance.cratepool[0]) 
					//{
					//	if (item.activeInHierarchy)
					//	{
					//		m_crates.Add (item);
					//		m_nCrates.Add (item);
					//	}
					//}
					//
					//foreach (GameObject item in crateCrash.Instance.cratepool[1]) 
					//{
					//	if(item.activeInHierarchy)
					//	{
					//		m_crates.Add (item);
					//		m_nCrates.Add (item);
					//	}
					//}
				}
				m_playerControls.posInList = i;
			}

			//instantiate other characters and attach dummy script to be able to update them on my local game
			else
			{
				GameObject opponentChar = (Instantiate (m_opponentPrefab[i], m_playerSpawnPoints[i].position, Quaternion.identity) as GameObject);
				OpponentController a_opponentScript = opponentChar.GetComponent <OpponentController> ();
				m_opponentScripts[a_nextParticipantID] = a_opponentScript;
				m_opponentScripts [a_nextParticipantID].posInList = i;
			}
		}
		Debug.Log ("Playerlistcount" + playersList.Count);

		m_multiplayerReady = true;
		Debug.Log ("match ready is " + m_matchReady);
		if (m_isHost) 
		{
			//send host message that he is ready
			byte[] a_byteMessage;
			Debug.Log("Sending ready message to clients from setup MP");
			m_listToConvert.Clear ();
			m_listToConvert.Add (m_messageVersion);
			m_listToConvert.Add ((byte)'B');
			a_byteMessage = m_listToConvert.ToArray ();
			HostSendSpecialMessage (a_byteMessage, 0);
		}

		else 
		{
			//send client message to host that he is ready
			Debug.Log("Sending ready message to host");
			byte[] a_byteMessage;
			m_listToConvert.Clear ();
			a_byteMessage = m_listToConvert.ToArray ();
			ClientSpecialMessage (EventType.begin, a_byteMessage);
		}
		//MultiplayerManager.Instance.ShowMPStatus ("MP setup complete");
	}

	void Update()
	{
		//once all players are "ready" start the game
		//does list of all players change if a player leaves?
		Debug.Log("match ready update" + m_matchReady);
		if (m_matchReady)
		{
			m_timeRemaining -= Time.deltaTime;
			Debug.Log ("time left" + m_timeRemaining);
			//Debug.Log ("players remaining" + a_playersAlive);
			//setup sudden death if more than 1 player remains after time has depleted
			//		if (m_timeRemaining <= 0) 
			//		{
			//Debug.Log ("time out");
			//			if (a_playersAlive >= 2) 
			//			{
			//				//sudden death
			//				Debug.Log("sudden death");
			//			}
			//
			//			else
			//			{
			//				SceneManager.LoadScene (2);
			//			}
			//}
			
			if (m_isHost) 
			{
				//			if (a_playersAlive < 2) 
				//			{
				//				//send message to all to end match, delcare winner and load next scene/map etc
				//				//SceneManager.LoadScene (2);
				//				m_listToConvert.Clear ();
				//				m_listToConvert.Add (m_messageVersion);
				//				m_listToConvert.Add ((byte)'E');
				//				byte[] a_byteMessage = m_listToConvert.ToArray ();
				//			}
				//
				//			else
				SendHostUpdate ();
			}

			else
			{
				SendClientData ();
			}
		}

//		m_timeRemaining -= Time.deltaTime;
//		Debug.Log ("time left" + m_timeRemaining);
//		Debug.Log ("players remaining" + a_playersAlive);
//		//setup sudden death if more than 1 player remains after time has depleted
//		if (m_timeRemaining <= 0) 
//		{
//			//Debug.Log ("time out");
//			if (a_playersAlive >= 2) 
//			{
//				//sudden death
//				Debug.Log("sudden death");
//			}
//
//			else
//			{
//				SceneManager.LoadScene (2);
//			}
//		//}
//
//		if (m_isHost) 
//		{
//			if (a_playersAlive < 2) 
//			{
//				//send message to all to end match, delcare winner and load next scene/map etc
//				//SceneManager.LoadScene (2);
//				m_listToConvert.Clear ();
//				m_listToConvert.Add (m_messageVersion);
//				m_listToConvert.Add ((byte)'E');
//				byte[] a_byteMessage = m_listToConvert.ToArray ();
//			}
//
//			else
//				SendHostUpdate ();
//		}
//
//		else
//		{
//			SendClientData ();
//		}
	}

	//	void SendClientData()
	//	{
	//		if (Time.time > m_nextBroadcastTime) 
	//		{
	//			MultiplayerManager.Instance.SendMyUpdate (m_myMPCharacter.transform.position.x, 
	//														m_myMPCharacter.transform.position.y, 
	//														m_myMPCharacter.transform.position.z,
	//														m_myMPRigidbody.velocity.x,
	//														m_myMPRigidbody.velocity.z,
	//														m_myMPCharacter.transform.rotation.eulerAngles.y);
	//			m_nextBroadcastTime = Time.time + .16f;
	//		}
	//	}

	//function the client uses to send data to the host
	void SendClientData()
	{
		if (Time.time > m_nextBroadcastTime)
		{
			//converts important information about client into list of bytes
			//that list is added to the list to send
			//converts list to send into byte[]
			m_listToConvert.Clear ();
			m_listToConvert.AddRange (m_playerControls.GetBytes ());
			byte[] messageToSend = m_listToConvert.ToArray();
			MultiplayerManager.Instance.SendClientUpdate (messageToSend);
			//MultiplayerManager.Instance.ShowMPStatus ("client sent byte[]");
			m_nextBroadcastTime = Time.time + .16f;
		}
	}

	//change send host to send events to all
	void SendHostUpdate()
	{	
		if (Time.time > m_nextBroadcastTime) 
		{
			m_listToConvert.Clear ();
			//iterate through objects in scene to convert their data to list<byte>
			//add individual objects list<byte> to big list
			//convert big list to byte[]
			//pass byte[] into SendHostUpdateToAll();
			foreach (Participant players in a_allPlayers) 
			{
				if (players.ParticipantId == m_myParticipantID) 
				{
					//MultiplayerManager.Instance.ShowMPStatus ("about to convert my host bytes");
					m_listToConvert.AddRange (m_playerControls.GetBytes ());
				}

				else
				{
					m_listToConvert.AddRange (m_opponentScripts[players.ParticipantId].GetBytes ());
					//MultiplayerManager.Instance.ShowMPStatus ("about to send player bytes");
				}
			}

			//TODO
			//implement conversion of pos,rot to bytes for crates/powerups
			//iterate through all the objects then add their byte list to large list
			//MultiplayerManager.Instance.ShowMPStatus ("about to send crate bytes");
			//Debug.Log ("box in list position 0 0" + crateCrash.Instance.cratepool[0][0]);
			//			for (int i = 0; i <crateCrash.Instance.cratepool[0].Count; i++) 
			//			{
			//				m_listToConvert.AddRange (crateCrash.Instance.cratepool[0][i].GetComponent <Crate> ().GetBytes ());
			//				MultiplayerManager.Instance.ShowMPStatus ("sending normal crates bytes");
			//			}
			//
			//			for (int i = 0; i <crateCrash.Instance.cratepool[1].Count; i++) 
			//			{
			//				m_listToConvert.AddRange (crateCrash.Instance.cratepool[1][i].GetComponent <Crate> ().GetBytes ());
			//				MultiplayerManager.Instance.ShowMPStatus ("sending special crates bytes");
			//			}

			for (int i = 0; i < crateCrash.Instance.m_listOfAllCrates.Count; i++) 
			{
				m_listToConvert.AddRange (crateCrash.Instance.m_listOfAllCrates[i].GetBytes ());
			}
			//			foreach (GameObject item in crateCrash.Instance.cratepool[1]) 
			//			{
			//				if (item.activeInHierarchy) 
			//				{
			//					m_listToConvert.AddRange (item.GetComponent <Crate> ().GetBytes ());
			//					MultiplayerManager.Instance.ShowMPStatus ("sending special crates bytes");
			//
			//				}
			//			}
			//			foreach (GameObject a_crate in m_nCrates) 
			//			{
			//				m_listToConvert.AddRange (a_crate.GetComponent <Crate>().GetBytes ());
			//				MultiplayerManager.Instance.ShowMPStatus ("sending list of normal crates bytes");
			//			}
			//
			//			foreach (GameObject a_crate in m_sCrates) 
			//			{
			//				m_listToConvert.AddRange (a_crate.GetComponent <Crate>().GetBytes ());
			//				MultiplayerManager.Instance.ShowMPStatus ("sending list of special crates bytes");
			//			}

			byte[] messageToSend = m_listToConvert.ToArray();
			//Debug.Log (messageToSend.Length + "this is length of message from host");
			//once all other list have been added to the large list
			//convert large list into byte[]
			//MultiplayerManager.Instance.ShowMPStatus ("about to send host update");
			//MultiplayerManager.Instance.ShowMPStatus ("host sent byte[]");

			MultiplayerManager.Instance.SendHostUpdateToAll(messageToSend);
			m_nextBroadcastTime = Time.time + .16f;
		}
	}

	//host sends special message with data and player number of the list of players
	public void HostSendSpecialMessage(byte[] a_data, int a_indexOfSource)
	{
		m_listToConvert.Clear ();
		byte messageVersion = (byte)a_data[0];
		char messageType = (char)a_data[1];
		byte[] messageToSend;

		//switches between different types of events
		switch (messageType)
		{
			//case for begin match
			case 'B':
				Debug.Log ("ready players host sendspec" + playersReady);
				playersReady++;
				Debug.Log ("ready players host sendspec added" + playersReady);
				if (playersReady >= a_allPlayers.Count) 
				{
					Debug.Log ("sending match ready message to clients");
					//send message to clients to set matchReady to true
					m_matchReady = true;
					m_listToConvert.AddRange (a_data);
					m_listToConvert.AddRange (System.BitConverter.GetBytes (m_matchReady));
					m_listToConvert.AddRange (System.BitConverter.GetBytes (a_indexOfSource));

					messageToSend = m_listToConvert.ToArray();
					MultiplayerManager.Instance.SendRelaibaleHostUpdateToAll(messageToSend);
					return;
				}
//				Debug.Log ("sending host ready message to clients");
//				m_listToConvert.AddRange (a_data);
//				m_listToConvert.AddRange (System.BitConverter.GetBytes (m_matchReady));
//				m_listToConvert.AddRange (System.BitConverter.GetBytes (a_indexOfSource));
//
//				messageToSend = m_listToConvert.ToArray();
//				MultiplayerManager.Instance.SendRelaibaleHostUpdateToAll(messageToSend);
				break;

			//case for pickup
			case 'P':
				m_listToConvert.AddRange (a_data);
				m_listToConvert.AddRange (System.BitConverter.GetBytes (a_indexOfSource));
				
				messageToSend = m_listToConvert.ToArray();
				MultiplayerManager.Instance.SendRelaibaleHostUpdateToAll(messageToSend);
				break;

			//case for throw
			case 'T':
				m_listToConvert.AddRange (a_data);
				m_listToConvert.AddRange (System.BitConverter.GetBytes (a_indexOfSource));	

				messageToSend = m_listToConvert.ToArray();
				MultiplayerManager.Instance.SendRelaibaleHostUpdateToAll(messageToSend);
				break;

			//case for attacking/punch
			case 'A':
				break;

			//case for opponent death
			case 'D':
				//add index of source into list of losers?
				m_listToConvert.AddRange (a_data);
				m_listToConvert.AddRange (System.BitConverter.GetBytes (a_indexOfSource));

				messageToSend = m_listToConvert.ToArray();
				MultiplayerManager.Instance.SendRelaibaleHostUpdateToAll(messageToSend);
				break;

			//case for special item spawned
			case 'S':
				//char powerUpIndex = a_data [2];
				m_listToConvert.AddRange (a_data);
				m_listToConvert.AddRange (System.BitConverter.GetBytes (a_indexOfSource));
				Debug.Log ("host sending special power spawn message");
				messageToSend = m_listToConvert.ToArray();
				MultiplayerManager.Instance.SendRelaibaleHostUpdateToAll(messageToSend);
				break;

			//case for special item effect collision
			case 'C':
				m_listToConvert.AddRange (a_data);
				m_listToConvert.AddRange (System.BitConverter.GetBytes (a_indexOfSource));
				Debug.Log ("host sending special power collision message");
				messageToSend = m_listToConvert.ToArray();
				MultiplayerManager.Instance.SendRelaibaleHostUpdateToAll(messageToSend);
				break;

			//case for snare/trap
//			case 'S':
//				m_listToConvert.AddRange (a_data);
//				m_listToConvert.AddRange (System.BitConverter.GetBytes (a_indexOfSource));
//
//				messageToSend = m_listToConvert.ToArray();
//				MultiplayerManager.Instance.SendRelaibaleHostUpdateToAll(messageToSend);
//				break;
			//case for sleepytime
//			case 'Z':
//				m_listToConvert.AddRange (a_data);
//				m_listToConvert.AddRange (System.BitConverter.GetBytes (a_indexOfSource));
//
//				messageToSend = m_listToConvert.ToArray();
//				MultiplayerManager.Instance.SendRelaibaleHostUpdateToAll(messageToSend);
//				break;

			//case for injured
			case 'I':
				m_listToConvert.AddRange (a_data);
				m_listToConvert.AddRange (System.BitConverter.GetBytes (a_indexOfSource));

				messageToSend = m_listToConvert.ToArray();
				MultiplayerManager.Instance.SendRelaibaleHostUpdateToAll(messageToSend);
				break;

			//case for show leaderboard match
			case 'L':
				m_listToConvert.AddRange (a_data);
				m_listToConvert.AddRange (System.BitConverter.GetBytes (a_indexOfSource));
				Debug.Log ("host sending leaderboard message"); 
				messageToSend = m_listToConvert.ToArray();
				MultiplayerManager.Instance.SendRelaibaleHostUpdateToAll(messageToSend);
				break;

			//case for end match
			case 'E':
				m_listToConvert.AddRange (a_data);
				m_listToConvert.AddRange (System.BitConverter.GetBytes (a_indexOfSource));

				messageToSend = m_listToConvert.ToArray();
				MultiplayerManager.Instance.SendRelaibaleHostUpdateToAll(messageToSend);
				break;

			default:
				break;
		}
		//byte[] messageToSend = m_listToConvert.ToArray();
		//MultiplayerManager.Instance.SendRelaibaleHostUpdateToAll(messageToSend);
	}

	//void clientDied()
	//send special event to host
	public void ClientSpecialMessage(EventType a_eventType, byte[] a_data)
	{
		m_listToConvert.Clear ();
		switch (a_eventType)
		{
			//begin event
			case EventType.begin:
				m_listToConvert.Add (m_messageVersion);
				m_listToConvert.Add ((byte)'B');
				//m_listToConvert.AddRange (a_data);
				Debug.Log ("Sending im ready event message as client");
				break;

			//pickup event
			case EventType.pickup:
				m_listToConvert.Add (m_messageVersion);
				m_listToConvert.Add ((byte)'P');
				m_listToConvert.AddRange (a_data);
				Debug.Log ("Sending pickup event message as client");
				break;

			//throw event
			case EventType.throwing:
				m_listToConvert.Add (m_messageVersion);
				m_listToConvert.Add ((byte)'T');
				m_listToConvert.AddRange (a_data);
				Debug.Log ("Sending throw event message as client");
				break;

			//punch event
			case EventType.punch:
				m_listToConvert.Add (m_messageVersion);
				m_listToConvert.Add ((byte)'A');
				Debug.Log ("Sending punch event message as client");
				break;

			//death event
			case EventType.death:
				m_listToConvert.Add (m_messageVersion);
				m_listToConvert.Add ((byte)'D');
				Debug.Log ("Sending death event message as client");
				break;

			//special power event
			case EventType.special:
				m_listToConvert.Add (m_messageVersion);
				m_listToConvert.Add ((byte)'S');
				m_listToConvert.AddRange (a_data);
				Debug.Log ("Sending special power event message as client");
				break;

			//leaderboard event
			case EventType.leaderboard:
				m_listToConvert.Add (m_messageVersion);
				m_listToConvert.Add ((byte)'L');
				m_listToConvert.AddRange (a_data);
				Debug.Log ("Sending leaderboard event message as client");
				break;

			//trap event
//			case EventType.trap:
//				m_listToConvert.Add (m_messageVersion);
//				m_listToConvert.Add ((byte)'S');
//				Debug.Log ("Sending trap event message as client");
//				break;
			//sleeptime event
//			case EventType.sleepytime:
//				m_listToConvert.Add (m_messageVersion);
//				m_listToConvert.Add ((byte)'Z');
//				Debug.Log ("Sending sleepytime event message as client");
//				break;

			//injury event
			case EventType.injured:
				m_listToConvert.Add (m_messageVersion);
				m_listToConvert.Add ((byte)'I');
				Debug.Log ("Sending injury event message as client");
				break;

			default:
				break;
		}
		byte[] messageToSend = m_listToConvert.ToArray();
		MultiplayerManager.Instance.SendRelaibaleClientUpdate(messageToSend);
	}

	//sendhostupdate()
	//	void SendHostUpdate()
	//	{
	//		if (Time.time > m_nextBroadcastTime) 
	//		{
	//			//encrypt in order information of objects, include time frame
	//			//first encrypt host player information
	//			//then encrypt player 2,3...crates, powerups
	//			//MultiplayerManager.Instance.ShowMPStatus ("about to send H update");
	//			MultiplayerManager.Instance.SendHostUpdateToClients 
	//			(
	//				//ask each player to get their own bytes
	//				m_myMPCharacter.transform.position.x, m_myMPCharacter.transform.position.y, m_myMPCharacter.transform.position.z,
	//				m_myMPRigidbody.velocity.x, m_myMPRigidbody.velocity.z, m_myMPCharacter.transform.rotation.eulerAngles.y,
	//				m_opponentScripts [a_allPlayers [1].ParticipantId].transform.position.x, m_opponentScripts [a_allPlayers [1].ParticipantId].transform.position.y,
	//				m_opponentScripts [a_allPlayers [1].ParticipantId].transform.position.z,
	//				m_opponentScripts [a_allPlayers [1].ParticipantId].m_opponentRigidbody.velocity.x, m_opponentScripts [a_allPlayers [1].ParticipantId].m_opponentRigidbody.velocity.z,
	//				m_opponentScripts [a_allPlayers [1].ParticipantId].transform.rotation.eulerAngles.y
	//			);
	//			m_nextBroadcastTime = Time.time + .16f;
	//			MultiplayerManager.Instance.ShowMPStatus ("update sent");
	//		}
	//	}

	public void ClientNetworkUpdate(byte[] a_dataFromHost, bool a_reliable)
	{
		//Debug.Log (a_dataFromHost.Length + "this is length of message for client");
		//change to if statement to choose if message is reliable or not
		//if message is reliable client update network in switch statement
		if (a_reliable)
		{
			byte messageVersion = (byte)a_dataFromHost[0];
			char messageType = (char)a_dataFromHost[1];

			//find index of original sender in list of participants to apply on correct player
			int playerNumber = (int)a_dataFromHost [a_dataFromHost.Length - 4];
			Debug.Log (playerNumber + "is playerNum");

			//Debug.Log ("message reliable" + a_reliable);
			switch (messageType)
			{
				//update begin status for clients
				case 'B':
					Debug.Log (m_matchReady + " is match ready");
					Debug.Log("message content from host" + System.BitConverter.ToBoolean (a_dataFromHost, 2));
					m_matchReady = System.BitConverter.ToBoolean (a_dataFromHost, 2);
					Debug.Log(m_matchReady + " is match ready now");
					break;

				//update pickup animation for clients
				case 'P':
					//ID of crate to use in pickup anim
					int a_id = System.BitConverter.ToInt32 (a_dataFromHost, 2);

					//if original sender == myID call play animation on playerscript
					if (a_allPlayers [playerNumber].ParticipantId == m_myParticipantID) 
					{
						//a_allPlayers[playerNumber].Player.
						m_playerControls.PlayPickUpAnim (crateCrash.Instance.m_listOfAllCrates [a_id].gameObject);
						Debug.Log ("im playing playpickup from host message");
					}

					//play animation on opponent controllers
					else
					{
						m_opponentScripts[a_allPlayers[playerNumber].ParticipantId].PlayPickUpAnim (crateCrash.Instance.m_listOfAllCrates [a_id].gameObject);
						Debug.Log ("opponent playpickup from host message");
					}
					break;

				//update client pickup anim
				case 'T':
					//if original sender == myID call play animation on playerscript
					if (a_allPlayers [playerNumber].ParticipantId == m_myParticipantID) 
					{
						m_playerControls.PlayThrowAnim ();
						Debug.Log ("im playing throw from host message");
					}

					//play animation on opponent controllers
					else
					{
						m_opponentScripts[a_allPlayers[playerNumber].ParticipantId].PlayThrowAnim ();
						Debug.Log ("opponent throw from host message");
					}
					break;

				//update client attack/punch animation
				case 'A':
					//if original sender == myID call play animation on playerscript
					if (a_allPlayers [playerNumber].ParticipantId == m_myParticipantID) 
					{
						//a_allPlayers[playerNumber].Player.
						//m_playerScript.PlayThrowAnim ();
						Debug.Log ("im playing attack from host message");
					}

					//play animation on opponent controllers
					else
					{
						//m_opponentScripts[a_allPlayers[playerNumber].ParticipantId].PlayThrowAnim ();
						Debug.Log ("opponent attack from host message");
					}
					break;

				case 'D':
					//a_playersAlive--;
					//if original sender == myID call play animation on playerscript
					if (a_allPlayers [playerNumber].ParticipantId == m_myParticipantID) 
					{
						m_playerControls.SetAnimation (Animate.Die);
						m_playerControls.m_currentState = PlayerState.Dead;
						Debug.Log ("im playing death from host message");
					}

					//play animation on opponent controllers
					else
					{
						m_opponentScripts[a_allPlayers[playerNumber].ParticipantId].SetAnimation (Animate.Die);
						m_opponentScripts [a_allPlayers [playerNumber].ParticipantId].m_currentState = PlayerState.Dead;
						Debug.Log ("opponent death from host message");
					}
					break;

				case 'S':
					int powerIndexS = System.BitConverter.ToInt32 (a_dataFromHost, 2);
					//get the power up index to activate the correct power up that was sent in the message
					//int powerIndex = System.BitConverter.ToInt32 (a_dataFromHost, 2);
					Debug.Log("recieved powerup spawn message as client power index = " + powerIndexS);
					//if original sender == myID call play animation on playerscript
					if (a_allPlayers [playerNumber].ParticipantId == m_myParticipantID) 
					{
						//animation and spawning for freeze
						if (powerIndexS == 1)
						{
							//PowerUpManager.Instance.ActivatePowerUp (powerIndex, m_playerControls.gameObject);
							PowerUpManager.Instance.ActivatePowerUp (powerIndexS, m_playerControls.gameObject, a_dataFromHost);
							m_playerControls.SetAnimation (Animate.ThrowFreezeBall);						
							Debug.Log ("im playing freeze from host message");
						}

						//animation for other power ups
						else
						{
							Debug.Log ("im playing powerup from host message");
						}
					}

					//play animation on opponent controllers
					else
					{
						if (powerIndexS == 1) 
						{
							PowerUpManager.Instance.ActivatePowerUp (powerIndexS, m_opponentScripts [a_allPlayers [playerNumber].ParticipantId].gameObject, a_dataFromHost);
							m_opponentScripts [a_allPlayers [playerNumber].ParticipantId].SetAnimation (Animate.ThrowFreezeBall);
							Debug.Log ("opponent freeze from host message");
						}

						else
						{
							//play opponent power up
						}
					}
					break;

				case 'C':
					int powerIndexC = System.BitConverter.ToInt32 (a_dataFromHost, 2);
					//get the power up index to activate the correct power up collision that was sent in the message
					//int powerIndex = System.BitConverter.ToInt32 (a_dataFromHost, 2);
					Debug.Log("recieved powerup collision message as client power index = " + powerIndexC);
					//get duration of power up effect
					float a_duration = System.BitConverter.ToSingle (a_dataFromHost, 6);
					Debug.Log("powerup collision duration = " + a_duration);
					//if original sender == myID call play animation on playerscript
					if (a_allPlayers [playerNumber].ParticipantId == m_myParticipantID) 
					{
						//animation and spawning for freeze
						if (powerIndexC == 1)
						{
							//PowerUpManager.Instance.ActivatePowerUp (powerIndex, m_playerControls.gameObject, a_dataFromHost);
							Debug.Log ("duration is = " + a_duration);
							StartCoroutine (m_playerControls.Frozen (a_duration));						
							Debug.Log ("im frozen from host message");
						}

						//behaviour for other power ups
						else
						{
							Debug.Log ("im playing powerup from host message");
						}
					}

					//play animation on opponent controllers
					else
					{
						if (powerIndexC == 1) 
						{
							Debug.Log ("duration is = " + a_duration);
							StartCoroutine (m_playerControls.Frozen (a_duration));						
							Debug.Log ("opponent frozen from host message");
						}

						else
						{
							//call behaviour on opponent for other powerups
						}
					}
					break;

//				case 'S':
//					//if original sender == myID call play animation on playerscript
//					if (a_allPlayers [playerNumber].ParticipantId == m_myParticipantID) 
//					{
//						//m_playerScript.SetAnimation (Animate.Die);
//						Debug.Log ("im playing snare/trap from host message");
//					}
//
//					//play animation on opponent controllers
//					else
//					{
//						//m_opponentScripts[a_allPlayers[playerNumber].ParticipantId].SetAnimation (Animate.Die);
//						Debug.Log ("opponent snare/trap from host message");
//					}
//					break;
//				case 'Z':
//					//if original sender == myID call play animation on playerscript
//					if (a_allPlayers [playerNumber].ParticipantId == m_myParticipantID) 
//					{
//						m_playerScript.SetAnimation (Animate.Die);
//						Debug.Log ("im playing sleepytime from host message");
//					}
//
//					//play animation on opponent controllers
//					else
//					{
//						m_opponentScripts[a_allPlayers[playerNumber].ParticipantId].SetAnimation (Animate.Die);
//						Debug.Log ("opponent sleepytime from host message");
//					}
//					break;

				//case for injury
				case 'I':
					//if original sender == myID call play animation on playerscript
					if (a_allPlayers [playerNumber].ParticipantId == m_myParticipantID) 
					{
						//m_playerScript.SetAnimation (Animate.Injured);
						m_playerControls.SetTriggerAnimation(TriggerAnimation.Injured);
						Debug.Log ("im playing injury from host message");
					}

					//play animation on opponent controllers
					else
					{
						m_opponentScripts [a_allPlayers [playerNumber].ParticipantId].SetTriggerAnimation (TriggerAnimation.Injured);//m_playerScript.m_animator .SetTrigger ("GotHit");
						//m_opponentScripts[a_allPlayers[playerNumber].ParticipantId].SetAnimation (Animate.Injured);
						Debug.Log ("opponent injury from host message");
					}
					break;

				//show leaderboard 
				case 'L':
					List<int> losersList = new List<int> ();
					losersList.Add (System.BitConverter.ToInt32 (a_dataFromHost, 2));
					losersList.Add (System.BitConverter.ToInt32 (a_dataFromHost,6));
					//losersList.Add (System.BitConverter.ToInt32 (a_dataFromHost, 10));
					//losersList.Add (System.BitConverter.ToInt32 (a_dataFromHost, 14));
					Debug.Log ("losers list element 1 =" + losersList [0]);
					Debug.Log ("losers list element 2  =" + losersList [0]);
					//Debug.Log ("UI state" + m_playerControls.m_endUIHolder.activeInHierarchy);
					//m_playerControls.m_endUIHolder.SetActive (true);
					m_gameUI.SetStandings(losersList);
					Debug.Log("starting coroutine as client");
					StartCoroutine(EndUIDelay());
					break;

				//end game message
				case 'E':
					SceneManager.LoadScene (2);
					break;
			}
		}
		//assign correct byte information to correct items in the list of players,crates, etc
		//EG first 6 bytes for player 1, update him to new position
		//bytes 12-X update crates 
		//information for host
		//		float posX1 = System.BitConverter.ToSingle(a_dataFromHost, 2);
		//		float posY1 = System.BitConverter.ToSingle(a_dataFromHost, 6);
		//		float posZ1 = System.BitConverter.ToSingle(a_dataFromHost, 10);
		//		float velX1 = System.BitConverter.ToSingle(a_dataFromHost, 14);
		//		float velZ1 = System.BitConverter.ToSingle(a_dataFromHost, 18);
		//		float rotY1 = System.BitConverter.ToSingle(a_dataFromHost, 22);
		//
		//		//information for opponent1
		//		float posX2 = System.BitConverter.ToSingle(a_dataFromHost, 26);
		//		float posY2 = System.BitConverter.ToSingle(a_dataFromHost, 30);
		//		float posZ2 = System.BitConverter.ToSingle(a_dataFromHost, 34);
		//		float velX2 = System.BitConverter.ToSingle(a_dataFromHost, 38);
		//		float velZ2 = System.BitConverter.ToSingle(a_dataFromHost, 42);
		//		float rotY2 = System.BitConverter.ToSingle(a_dataFromHost, 46);
		//int c = 0;

		else
		{
			int startPos= 0;
			foreach (Participant players in a_allPlayers) 
			{
				if (players.ParticipantId == m_myParticipantID) 
				{
					//m_playerControls.ReadBytes (a_dataFromHost, startPos);
					//c++;
				}

				else
				{
					m_opponentScripts[players.ParticipantId].ReadBytes (a_dataFromHost, startPos);
					//c++;
				}
				startPos += 26;
				//MultiplayerManager.Instance.ShowMPStatus ("updated objects in client game");
			}

			for (int i = 0; i < crateCrash.Instance.m_listOfAllCrates.Count; i++) 
			{
				crateCrash.Instance.m_listOfAllCrates[i].ReadBytes (a_dataFromHost, startPos);
				startPos += 26;
			}
		}
		//how to iterate through giant host message and find correct spot for crate offset/start
		//add a check to see if it is active?
		//		for (int i = 0; i <crateCrash.Instance.cratepool[0].Count; i++) 
		//		{
		//			
		//			crateCrash.Instance.cratepool[0][i].GetComponent <Crate> ().ReadBytes(a_dataFromHost, startPos);
		//			MultiplayerManager.Instance.ShowMPStatus ("updating normal crates bytes");
		//			startPos += 26;
		//		}
		//
		//		for (int i = 0; i <crateCrash.Instance.cratepool[1].Count; i++) 
		//		{
		//			crateCrash.Instance.cratepool[1][i].GetComponent <Crate> ().ReadBytes(a_dataFromHost, startPos);
		//			MultiplayerManager.Instance.ShowMPStatus ("updating special crates bytes");
		//			startPos +=26;
		//				
		//		}
		//		int j = 0;
		//		int k = 0;
		//		foreach (GameObject item in crateCrash.Instance.cratepool[0]) 
		//		{
		//			if(item.activeInHierarchy)
		//			{
		//				(item.GetComponent <Crate>().ReadBytes (a_dataFromHost, j));
		//				j++;
		//			}
		//		}
		//
		//		foreach (GameObject item in crateCrash.Instance.cratepool[1]) 
		//		{
		//			if (item.activeInHierarchy) 
		//			{
		//				(item.GetComponent <Crate> ().ReadBytes (a_dataFromHost, k++));
		//				k++;
		//			}
		//		}
		//		foreach (GameObject a_crate in m_crates) 
		//		{
		//			if(a_crate.activeInHierarchy)
		//			{
		//				MultiplayerManager.Instance.ShowMPStatus ("recieved crates bytes");
		//				a_crate.GetComponent <Crate> ().ReadBytes (a_dataFromHost, j);
		//				j++;
		//			}
		//		}
		//HostMessageRecieved (posX1, posY1, posZ1, velX1, velZ1, rotY1, posX2, posY2, posZ2, velX2, velZ2, rotY2);
	}

	//	public void HostMessageRecieved (float a_posX1, float a_posY1, float a_posZ1, float a_velX1, float a_velZ1, float a_rotY1, 
	//										float a_posX2, float a_posY2, float a_posZ2, float a_velX2, float a_velZ2, float a_rotY2)
	//	{
	//		for (int i = 0; i < a_allPlayers.Count; i++) 
	//		{
	//			string a_nextParticipantID = a_allPlayers [i].ParticipantId;
	//
	//			if (a_nextParticipantID  == a_allPlayers[0].ParticipantId) 
	//			{
	//				m_opponentScripts [a_nextParticipantID].SetOpponentPosition (a_posX1, a_posY1, a_posZ1, a_velX1, a_velZ1, a_rotY1);
	//			}
	//
	//			if (a_nextParticipantID == a_allPlayers[1].ParticipantId)
	//			{
	//				m_myMPCharacter.transform.position = new Vector3 (a_posX2, a_posY2, a_posZ2);
	//				m_myMPCharacter.transform.rotation = Quaternion.Euler (0, a_rotY2, 0);
	//				m_myMPRigidbody.velocity = new Vector3 (a_velX2, 0, a_velZ2);
	//			}
	//		}
	//	}

	//	public void UpdateReceived(string senderId, float posX, float posY, float posZ, float velX, float velZ, float rotY) 
	//	{
	//		if (m_multiplayerReady) 
	//		{
	//			OpponentController opponent = m_opponentScripts[senderId];
	//			if (opponent != null) 
	//			{
	//				//Debug.Log ("Message update from opponent");
	//				opponent.SetOpponentInformation (posX, posY, posZ, velX, velZ, rotY);
	//				//Debug.Log (posX+" "+ posY +" "+ posZ +" upadte positions for opponent");
	//			}
	//		}
	//	}

	//update recieved from other player
	public void HostNetworkUpdate (byte[] datafromClient, string a_senderID) 
	{
		if (m_multiplayerReady) 
		{
			if (m_isHost)
			{
				//dictionary string to match particpantsender id
				//then apply xyz transforms
				//call set opponent for item retrieved from dictionary
				//add switch for message type then decrypt and call correct function from opponentcontroller
				byte messageVersion = (byte)datafromClient[0];
				char messageType = (char)datafromClient[1];
				byte[] messageToSend;
				//string mchar = messageType.ToString ();
				//Debug.Log (messageType + "is the type of message");
				//MultiplayerManager.Instance.ShowMPStatus ("about to enter switch for host update");
				//MultiplayerManager.Instance.ShowMPStatus (mchar);

				//switch statement to handle differnt types of messages Updates/events
				switch (messageType)
				{
					//update players ready and send message to start if all are ready
					case 'B':
						Debug.Log ("host has" + playersReady + " ready");
						playersReady++;
						Debug.Log ("host now has" + playersReady + " ready");
						//Debug.Log("all players count" + a_allPlayers.Count);

						if (playersReady >= a_allPlayers.Count) 
						{
							m_matchReady = true;
							byte[] a_byteMessage;
							Debug.Log("sending match ready message to clients in host network update");
							m_listToConvert.Clear ();
							m_listToConvert.Add (m_messageVersion);
							m_listToConvert.Add ((byte)'B');
							m_listToConvert.AddRange (System.BitConverter.GetBytes (m_matchReady));
							m_listToConvert.AddRange (System.BitConverter.GetBytes (0));
							a_byteMessage = m_listToConvert.ToArray ();
							//m_listToConvert.AddRange (datafromClient);
							Debug.Log ("host send match ready as " + m_matchReady);
							MultiplayerManager.Instance.SendRelaibaleHostUpdateToAll(a_byteMessage);
							return;
						}
						break;

					//update clients
					case 'U':
						float posX = System.BitConverter.ToSingle (datafromClient, 2);
						float posY = System.BitConverter.ToSingle (datafromClient, 6);
						float posZ = System.BitConverter.ToSingle (datafromClient, 10);
						float velX = System.BitConverter.ToSingle (datafromClient, 14);
						float velZ = System.BitConverter.ToSingle (datafromClient, 18);
						float rotY = System.BitConverter.ToSingle (datafromClient, 22);
						Debug.Log ("message recieved from client update" + GameObject.FindObjectOfType<OpponentController>());
						//MultiplayerManager.Instance.ShowMPStatus (posX.ToString ());

						m_opponentScripts [a_senderID].SetOpponentPosition (posX, posY, posZ, velX, velZ, rotY);
						break;
					
					//update pickup animation for clients
					case 'P':
						for (int i = 0; i < a_allPlayers.Count; i++) 
						{
							string a_nextParticipant = a_allPlayers [i].ParticipantId;
							if (a_senderID == a_nextParticipant) 
							{
								//send the message to all and update the animation
								HostSendSpecialMessage(datafromClient, i);

								int a_id = System.BitConverter.ToInt32 (datafromClient, 2);
								m_opponentScripts[a_nextParticipant].PlayPickUpAnim (crateCrash.Instance.m_listOfAllCrates [a_id].gameObject);
								Debug.Log ("host setting playpickup for clients");
								break;
							}
						}
						//read through byte message and play animation
						break;

					//update to throw animation for clients
					case 'T':
						for (int i = 0; i < a_allPlayers.Count; i++) 
						{
							string a_nextParticipant = a_allPlayers [i].ParticipantId;
							if (a_senderID == a_nextParticipant) 
							{
								//send message to all and update anim
								//m_listToConvert.Clear();
//								m_listToConvert.AddRange (datafromClient);
//								m_listToConvert.AddRange (System.BitConverter.GetBytes(m_opponentScripts [a_senderID].gameObject.transform.forward.x));
//								m_listToConvert.AddRange (System.BitConverter.GetBytes(m_opponentScripts [a_senderID].gameObject.transform.forward.y));
//								m_listToConvert.AddRange (System.BitConverter.GetBytes(m_opponentScripts [a_senderID].gameObject.transform.forward.z));
//								byte[] dataToSend = m_listToConvert.ToArray ();
								HostSendSpecialMessage(datafromClient, i);
								
								//float yRot = System.BitConverter.ToSingle (datafromClient, 2);
								//float xPos = System.BitConverter.ToSingle (datafromClient, 6);
								//float yPos = System.BitConverter.ToSingle (datafromClient, 10);
								//float zPos = System.BitConverter.ToSingle (datafromClient, 14);
								
								//m_opponentScripts[a_nextParticipant].m_playerScript.m_crate;
								m_opponentScripts [a_nextParticipant].PlayThrowAnim ();
								Debug.Log ("host setting throw for clients");
								break;
							}
						}
						break;
					
					//update to attack animation for clients
					case 'A':
						//attack
						break;

					//update to death animation for clients
					case 'D':
						//death
						//a_playersAlive--;
						for (int i = 0; i < a_allPlayers.Count; i++) 
						{
							string a_nextParticipant = a_allPlayers [i].ParticipantId;
							if (a_senderID == a_nextParticipant) 
							{
								//send the message to all and update the animation
								HostSendSpecialMessage(datafromClient, i);

								//int a_id = System.BitConverter.ToInt32 (datafromClient, 2);
								m_opponentScripts [a_nextParticipant].SetAnimation(Animate.Die);
								Debug.Log ("playlist count before remove" + playersList.Count);
								playersList.Remove(i);
								m_losersList.Add (i);
								Debug.Log ("playlist count" + playersList.Count);
								if (playersList.Count <= (a_allPlayers.Count - (a_allPlayers.Count - 1))) 
								{
									m_losersList.AddRange (playersList);
									//m_losersList.Add (0);
									Debug.Log ("Host send loserlist to client and add himself to the top");
									m_listToConvert.Clear ();
									m_listToConvert.Add (m_messageVersion);
									m_listToConvert.Add ((byte)'L');
									m_listToConvert.AddRange (ReturnLosersList(m_losersList));
									messageToSend = m_listToConvert.ToArray ();
									CrateCrashManager.Instance.HostSendSpecialMessage (messageToSend, 0);

									//enable end game UI
									//m_playerControls.m_endUIHolder.SetActive(true);
									m_playerControls.m_gameUI.SetStandings (m_losersList);
									
									//start coroutine
									Debug.Log("starting coroutine as host");
									StartCoroutine(EndUIDelay());

									//send end game event message
									//Debug.Log ("host sending end game event");
									//m_listToConvert.Clear ();
									//m_listToConvert.Add (m_messageVersion);
									//m_listToConvert.Add ((byte)'E');
									//messageToSend = m_listToConvert.ToArray ();
									//HostSendSpecialMessage (messageToSend, i);//add index?
									//SceneManager.LoadScene (2);
								}
								Debug.Log ("host setting death for client");
								break;
							}
						}
						break;

					//update to spawn freezeball from client
//					case 'F':
//						for (int i = 0; i < a_allPlayers.Count; i++) 
//						{
//							string a_nextParticipant = a_allPlayers [i].ParticipantId;
//							if (a_senderID == a_nextParticipant) 
//							{
//								//send message to all and update anim
//								HostSendSpecialMessage(datafromClient, i);
//								
//								//instantiate power up and play animation
//								m_opponentScripts [a_nextParticipant].SetAnimation (Animate.ThrowFreezeBall);
//								PowerUpManager.Instance.ActivatePowerUp (1, m_opponentScripts [a_nextParticipant].gameObject);
//								Debug.Log ("host setting freeze for clients");
//								break;
//							}
//						}
//						break;

					//update to spawn special from client
					case 'S':
						//spawn special power
						int powerIndex = System.BitConverter.ToInt32 (datafromClient, 2);
						for (int i = 0; i < a_allPlayers.Count; i++) 
						{
							string a_nextParticipant = a_allPlayers [i].ParticipantId;
							if (a_senderID == a_nextParticipant) 
							{
								Debug.Log ("spawn special from" + i);
								Debug.Log ("index of clien power up" + powerIndex);
								//float xPos = System.BitConverter.ToSingle (datafromClient, 4);
								//float yPos = System.BitConverter.ToSingle (datafromClient, 10);
								//float zPos = System.BitConverter.ToSingle (datafromClient, 14);
								//float xDir = System.BitConverter.ToSingle (datafromClient, 18);
								//float zDir = System.BitConverter.ToSingle (datafromClient, 22);

								//spawn freeze and play animation
								if (powerIndex == 1) 
								{
									HostSendSpecialMessage (datafromClient, i);
									
									m_opponentScripts [a_nextParticipant].SetAnimation (Animate.ThrowFreezeBall);
									PowerUpManager.Instance.ActivatePowerUp (powerIndex, m_opponentScripts [a_nextParticipant].gameObject, datafromClient);
									Debug.Log ("host setting freeze special for opponents");
									break;
								}

								//spawn sleepytime/trap
								else
								{
									HostSendSpecialMessage (datafromClient, i);

									//m_opponentScripts [a_nextParticipant].SetAnimation (Animate.ThrowFreezeBall);
									PowerUpManager.Instance.ActivatePowerUp (powerIndex, m_opponentScripts [a_nextParticipant].gameObject);
									Debug.Log ("host setting other special for clients");
									break;
								}
							}
						}
						break;

					//update to spawn sleepytime from client
//					case 'Z':
//						//spawn sleepytime power
//						break;

					//update to play injured animation from client
					case 'I':
						for (int i = 0; i < a_allPlayers.Count; i++) 
						{
							string a_nextParticipant = a_allPlayers [i].ParticipantId;
							if (a_senderID == a_nextParticipant) 
							{
								//send the message to all and update the animation
								HostSendSpecialMessage(datafromClient, i);

								//int a_id = System.BitConverter.ToInt32 (datafromClient, 2);
								m_opponentScripts [a_nextParticipant].SetTriggerAnimation (TriggerAnimation.Injured);//.m_playerScript.m_animator.SetTrigger("GotHit");
								//m_opponentScripts [a_nextParticipant].SetAnimation(Animate.Injured);
								Debug.Log ("host setting injury for client");
								break;
							}
						}
						break;

					default:
						break;
				}
			}
		}
	}

	public IEnumerator EndUIDelay()
	{
		Debug.Log ("rnd is num");
		yield return new WaitForSeconds (4f);

		Debug.Log ("changing scenes");
		int rnd = Random.Range (2, 5);
		Debug.Log ("rnd is" + rnd);
		SceneManager.LoadScene (rnd);
	}

	public List<byte> ReturnLosersList (List<int> a_losersList)
	{
		List<byte> blist = new List<byte> ();	
		for (int i = 0; i < a_losersList.Count; i++) 
		{
			blist.AddRange (System.BitConverter.GetBytes(a_losersList[i]));
		}
		return blist;
	}

	void LeaveMPGame() 
	{
		MultiplayerManager.Instance.LeaveGame();
	}

	public void LeftRoomConfirmed ()
	{
		MultiplayerManager.Instance.m_updateListener = null;
		SceneManager.LoadScene (0);
	}

	public void PlayerLeftRoom (string participantId)
	{
		if (m_opponentScripts[participantId] != null) 
		{
			m_opponentScripts[participantId].HidePlayer();
		}
	}

	public void HidePlayer()
	{
	}
}
