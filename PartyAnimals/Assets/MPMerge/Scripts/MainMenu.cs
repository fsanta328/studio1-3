﻿using UnityEngine;
using System.Collections;
//using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour , MPLobbyListener
{
	//public Sprite m_background;
	private bool _showLobbyDialog;
	private string _lobbyMessage;

	public Text m_lobbyMessageText;
	private static MainMenu m_instance;

	//NEW added
	public GameObject m_mainMenuHolder;
	public GameObject m_optionsMenuHolder;
	public Slider[] m_volumeSliders;
	public int[] m_screenWidths;
	int m_activeScreenResIndex;
	public Toggle m_fullscreenToggle;

	void Awake()
	{
		DontDestroyOnLoad (this.gameObject);

		if (!m_instance)
		{
			m_instance = this;
		} 

		else
		{
			Destroy (this.gameObject);
		}
	}

	// Use this for initialization
	void Start () 
	{
		m_lobbyMessageText = m_lobbyMessageText.GetComponent <Text> ();
		m_lobbyMessageText.text = "1st load in";
		MultiplayerManager.Instance.TrySilentSignIn();
	}
	
	// Update is called once per frame
	void Update () 
	{
		//sign out button
//		if ((CrossPlatformInputManager.GetButtonDown ("SignOut") == true) && (MultiplayerManager.Instance.IsAuthenticated ()))
//		{
//			MultiplayerManager.Instance.SignOut ();
//			m_lobbyMessageText.text = "user signed out";
//		}
//
//		//sign in & search for room
//		if (CrossPlatformInputManager.GetButtonDown ("Multiplayer") == true) 
//		{
//			_lobbyMessage = "Starting a multi-player game";
//			_showLobbyDialog = true;
//			m_lobbyMessageText.text = _lobbyMessage;
//			MultiplayerManager.Instance.m_lobbyListener = this;
//			MultiplayerManager.Instance.SignInAndStartMPGame();
//		}
//
//		if (CrossPlatformInputManager.GetButtonDown ("Quit") == true)
//		{
//			MultiplayerManager.Instance.LeaveGame ();
//		}
	}

	public void SetLobbyStatusMessage(string message) 
	{
		_lobbyMessage = message;
		m_lobbyMessageText.text = _lobbyMessage;
	}

	public void HideLobby() 
	{
		_lobbyMessage = "";
		m_lobbyMessageText.text = _lobbyMessage;
		_showLobbyDialog = false;
	}
}
