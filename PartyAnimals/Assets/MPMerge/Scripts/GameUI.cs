﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameUI : MonoBehaviour 
{
	public GameObject m_endUI;
	public Text[] m_placings2;
	public Text[] m_placings4;
	public GameObject m_optionHolder;

	public void SetStandings (List<int> a_losersList)
	{
		m_endUI.SetActive (true);
		string[] players = { "Croc", "Panda", "Rabbit", "Werewolf"};

		if (MultiplayerManager.Instance.m_twoPlayer) 
		{
			for (int i = 0; i < a_losersList.Count; i++) 
			{
				m_placings2 [i].text = "#" + (a_losersList.Count - i) + " - " + players [a_losersList [i]];
			}
		}

		else
		{
			for (int i = 0; i < a_losersList.Count; i++) 
			{
				m_placings4 [i].text = "#" + (a_losersList.Count - i) + " - " + players [a_losersList [i]];
			}
		}
	}

	public void OpenOptions()
	{
		m_optionHolder.SetActive (true);
	}

	public void CloseOptions()
	{
		m_optionHolder.SetActive (false);
	}

	public void QuitToMenu()
	{
		MultiplayerManager.Instance.LeaveGame();
		SceneManager.LoadScene (0);
	}

	public void QuitApp()
	{
		MultiplayerManager.Instance.LeaveGame();
	}
}
