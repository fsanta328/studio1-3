﻿public interface MPLobbyListener 
{
	void SetLobbyStatusMessage(string message);
	void HideLobby();
}

public interface MPUpdateListener 
{
	//void UpdateReceived(string participantId, float posX, float posY, float posZ, float velX, float velZ, float rotY);
	//update for animation
	//reliable updates to end game or other actions?
	void HostNetworkUpdate (byte[] a_datafromClient, string a_senderID);
	void ClientNetworkUpdate (byte[] a_dataFromHost, bool a_reliable);
//	void HostMessageRecieved (float a_posX1, float a_posY1, float a_posZ1, float a_velX1, float a_velZ1, float a_rotY1, 
//								float a_posX2, float a_posY2, float a_posZ2, float a_velX2, float a_velZ2, float a_rotY2);
	void LeftRoomConfirmed();
	void PlayerLeftRoom(string participantId);
}