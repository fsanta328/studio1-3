﻿using UnityEngine;
using System.Collections;

public enum Animate 
{
	Idle = 0,
	Walk = 2,
	PickUp = 4,
	Injured = 6,
	ThrowFreezeBall = 8,
	Die = 10,
};

public enum TriggerAnimation
{
	Injured,
	ThrowCrate,
};