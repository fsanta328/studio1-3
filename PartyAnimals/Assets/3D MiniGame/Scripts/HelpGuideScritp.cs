﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HelpGuideScritp : MonoBehaviour 
{
	public float m_timerStartingValue;
	public Text m_virtualTimer;
	public GameObject[] m_playerControls;
	private GameObject m_selectedCharacter;

	// Update is called once per frame
	void Update () 
	{
		m_timerStartingValue -= Time.deltaTime;

		m_virtualTimer.text = "Waiting Time " + (int)m_timerStartingValue;

		if (m_timerStartingValue <= 0.0f) 
		{
			Ready ();
		}
	}

	public void Ready()
	{
		foreach (GameObject a_ui in m_playerControls) 
		{
			a_ui.SetActive (true);
		}

		if (GameObject.FindGameObjectWithTag ("Character") != null) 
		{
			foreach (GameObject Character in GameObject.FindGameObjectsWithTag ("Character")) 
			{
				Character.GetComponent<PlayerControls> ().enabled = true;
				Character.GetComponent<PlayerHealth> ().enabled = true;
			}
			this.gameObject.SetActive (false);
		}

		else 
		{
			this.gameObject.SetActive (false);
		}
	}
}
