﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SleepyTime : MonoBehaviour, PowerUp
{
	public int m_itemNum;
	public bool m_active = false;
	float Duration = 3;
	float m_lifeSpan;
	public CapsuleCollider m_playerCapsuleCollider;

	private List<GameObject> m_characters = new List<GameObject>();
	private List<GameObject> m_affects = new List<GameObject> ();

	//NEW added
	public List<byte> m_myByteList = new List<byte>();
	private byte m_messageVersion = 1;
	private Rigidbody controller;
	private Vector3 m_startPos;
	private Vector3 m_destinationPos;
	private Quaternion m_startRot;
	private Quaternion m_destinationRot;
	private bool isActive = false;
	public GameObject m_powerOwner;
	public ParticleSystem m_particle;
	private int m_powerIndex =0;

	void Start()
	{
		//NEW added
		m_particle = GetComponent<ParticleSystem>();
		controller = GetComponent <Rigidbody>();

		m_startPos = transform.position;
		m_startRot = transform.rotation;

		m_destinationPos = transform.position;
		m_destinationRot = transform.rotation;

		m_lifeSpan = 4f;
	}

	void Update()
	{
		m_lifeSpan -= Time.deltaTime;

		if (m_lifeSpan <= 0) 
		{
			Destroy (this.gameObject);
		}

		if (m_active == true) 
		{
			PowerBehaviour ();
		}
	}
		
	// AT postion spawn , anything inside the raidus will be disable the character controller. 
	void OnTriggerStay(Collider a_collision)
	{
		//do only on host
		if (CrateCrashManager.Instance.m_isHost)	
		{
			if ((a_collision.gameObject != m_powerOwner) && (m_active == true)) 
			{
				//find who it hit through index
				//then send message
				//hostspecialmessage bytearray and index (who was hit)
				//client who recieves message call frozen() function
				//reverse similar of Special event message
				byte[] a_byteMessage;
				int playerIndex = a_collision.gameObject.GetComponent<PlayerControls> ().posInList;
				//1 byte version
				//1 byte type
				//8 bytes for 2 int
				//10byte message
				m_myByteList.Clear ();
				m_myByteList.Add (m_messageVersion);
				m_myByteList.Add ((byte)'C');
				m_myByteList.AddRange (System.BitConverter.GetBytes (m_powerIndex));
				m_myByteList.AddRange (System.BitConverter.GetBytes (Duration));
				a_byteMessage = m_myByteList.ToArray ();
				CrateCrashManager.Instance.HostSendSpecialMessage (a_byteMessage, playerIndex);

				StartCoroutine(a_collision.gameObject.GetComponent<PlayerControls> ().Frozen(Duration));
				//a_collision.gameObject.GetComponent<OpponentController>().m_currentState = PlayerState.Frozen;
				Debug.Log("state of hit opponent = " + a_collision.gameObject.GetComponent<PlayerControls>().m_currentState);
				m_particle.Stop();
				m_active = false;
			}
		}
//		GameObject a_character = a_collision.gameObject;
//
//		if ((a_character.tag == "Character") && (m_active == true)) 
//		{
//			//Add to the list
//			m_characters.Add (a_character);
//
//			PlayerControls a_playerControls = a_character.GetComponent<PlayerControls> ();
//
//			//When character is sleep, do certain animation
//			a_playerControls.m_playerScript.SetAnimation(Animate.Idle);
//
//			//if character carring crate, make character lose crate
//			a_playerControls.TossCrate();
//
//			//disable player's controls
//			a_playerControls.enabled = false;
//
//			//enable VFX
//			foreach (Transform child in a_character.transform) 
//			{
//				if (child.gameObject.name == "ZZZZVFX") 
//				{
//					m_affects.Add (child.gameObject);
//					child.gameObject.SetActive (true);
//				}
//			}
//
//			//disable the effects of power on each characters after certain period of time is passed
//			StartCoroutine(DisablePowerBehavior());
//		}
	}
		
//	IEnumerator DisablePowerBehavior() 
//	{
//		yield return new WaitForSeconds(Duration); 
//
//		//get each character that was caught in the sleep
//		foreach (GameObject a_character in m_characters) 
//		{
//			//enable the controls of the player
//			a_character.GetComponent<PlayerControls> ().enabled = true; 
//		}
//
//		//get each affect in the list
//		foreach (GameObject a_affect in m_affects) 
//		{
//			//disable affect
//			a_affect.SetActive (false);
//		}
//
//		//destroy this power after certain time is passed
//		Destroy (this.gameObject);
//	}

	public void ActivatePower(GameObject a_owner)
	{
		m_active = true;
		m_powerOwner = a_owner;
	}

	public void ActivatePower(GameObject a_owner, Transform a_transform)
	{
		m_powerOwner = a_owner;
		m_active = true;
		//SetDirection (a_transform.forward);
	}

	public void PowerBehaviour()
	{
		m_active = true;
	}


	public int SelectItem ()
	{
		return 0;
	}

	public void PowerUpDuration()
	{
	}

	public Transform ReadBytes (byte[] a_senderData)
	{
		return this.transform;
	}
}
