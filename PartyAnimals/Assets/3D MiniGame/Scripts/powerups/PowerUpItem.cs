﻿using UnityEngine;
using System.Collections;

//sprite swap looks like it is lagging, possible check to avoid new icon == previous icon
public class PowerUpItem : MonoBehaviour, PowerUp
{
	public Sprite[] m_powerUpIcons;
	SpriteRenderer m_iconRenderer;
	bool m_coroutineRunning = false;
	public int m_powerUpNum;

	// Use this for initialization
	void Start ()
	{
		m_iconRenderer = this.GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () 
	{	
		if (m_coroutineRunning == false) 
		{
			StartCoroutine (SwapIcon (1f));
		}
	}

	/// <summary>
	/// Randomly swaps the icon of the power up in 2D
	/// and a coroutine delays the swap of the sprite
	/// </summary>
	private IEnumerator SwapIcon(float a_time)
	{
		if (m_coroutineRunning)
			yield break;

		m_coroutineRunning = true;

		//Debug.Log ("swapping");
		yield return new WaitForSeconds (a_time);
		m_powerUpNum = Random.Range (0, 3);
		m_iconRenderer.sprite = m_powerUpIcons [m_powerUpNum];
		//Debug.Log ("swapped");
		m_coroutineRunning = false;
	}

	public void ActivatePower(GameObject a_owner)
	{
	}

	public void ActivatePower (GameObject a_owner, Transform a_transform)
	{
		throw new System.NotImplementedException ();
	}

	public int SelectItem ()
	{
		return m_powerUpNum;
	}

	public void PowerBehaviour()
	{
	}

	public void PowerUpDuration()
	{
	}

	public Transform ReadBytes (byte[] a_senderData)
	{
		return this.transform;
	}
}
