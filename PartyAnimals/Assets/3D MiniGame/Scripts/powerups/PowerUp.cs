﻿using UnityEngine;
using System.Collections;

public interface PowerUp
{
	//make into a class and add properties
	//id for power up
	int SelectItem ();
	void PowerUpDuration ();
	void ActivatePower (GameObject a_owner);
	void ActivatePower (GameObject a_owner, Transform a_transform);
	void PowerBehaviour();
	Transform ReadBytes(byte[] a_senderData);
}
