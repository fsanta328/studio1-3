﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Trap : MonoBehaviour, PowerUp
{
	public int m_itemNum;
	public bool m_active = false;
	public float m_trapDuration;

	internal CapsuleCollider m_playerCapsuleCollider;

	private List<GameObject> m_characters = new List<GameObject>();
	private List<GameObject> m_affects = new List<GameObject> ();

	//NEW added
	public List<byte> m_myByteList = new List<byte>();
	private byte m_messageVersion = 1;
	private Rigidbody controller;
	private Vector3 m_startPos;
	private Vector3 m_destinationPos;
	private Quaternion m_startRot;
	private Quaternion m_destinationRot;
	private bool isActive = false;

	//NEW added
	public GameObject m_owner;

	void Start()
	{
		Physics.IgnoreCollision (this.gameObject.GetComponent<BoxCollider> (), m_playerCapsuleCollider);

		//NEW added
		controller = GetComponent <Rigidbody>();

		m_startPos = transform.position;
		m_startRot = transform.rotation;

		m_destinationPos = transform.position;
		m_destinationRot = transform.rotation;

//		#if !UNITY_EDITOR
//		if (!CrateCrashManager.Instance.m_isHost)
//		{
//			controller.isKinematic = true;
//			GetComponent <BoxCollider>().enabled = false;
//		}
//		#endif
	}

	void Update()
	{
		if (m_active == true) 
		{
			PowerBehaviour ();
		}

//		#if !UNITY_EDITOR
		//NEW added
		//do only if it is not host
//		if (!CrateCrashManager.Instance.m_isHost)
//		{
//			transform.position = m_destinationPos;
//			transform.rotation = m_destinationRot;
//		}
//		#endif
	}

	void OnTriggerEnter(Collider a_collision)
	{
		GameObject a_character = a_collision.gameObject;

		//if there is any character near..
		if ((a_character.tag == "Character") && (m_active == true)) 
		{
			//Add to the list
			m_characters.Add (a_character);

			PlayerControls a_playerControls = a_character.GetComponent<PlayerControls> ();

			//When character is trap, do certain animation
			a_playerControls.m_playerScript.SetAnimation(Animate.Idle);

			//if character carring crate, make character lose crate
			a_playerControls.TossCrate();

			//disable player's controls
			a_playerControls.enabled = false;

			//enable VFX
			foreach (Transform child in a_character.transform) 
			{
				if (child.gameObject.name == "TrapVFX") 
				{
					m_affects.Add (child.gameObject);
					child.gameObject.SetActive (true);
				}
			}

			//disable the effects of power on each characters after certain period of time is passed
			StartCoroutine(DisablePowerBehavior());
		}
	}

	public void ActivatePower(GameObject a_owner)
	{
		//InstantiatePowerUp ();
		m_active = true;
		m_owner = a_owner;
	}

	public void ActivatePower (GameObject a_owner, Transform a_transform)
	{
		throw new System.NotImplementedException ();
	}

	public void PowerBehaviour()
	{
		m_active = true;
	}

	public int SelectItem ()
	{
		return 0;
	}

	public void PowerUpDuration()
	{
	}

	IEnumerator DisablePowerBehavior() 
	{
		yield return new WaitForSeconds(m_trapDuration); 

		//get each character that was caught in the trap
		foreach (GameObject a_character in m_characters) 
		{
			//enable the controls of the player
			a_character.GetComponent<PlayerControls> ().enabled = true; 
		}

		//get each affect in the list
		foreach (GameObject a_affect in m_affects) 
		{
			//disable affect
			a_affect.SetActive (false);
		}

		//destroy this power after certain time is passed
		Destroy (this.gameObject);
	}

	//NEW added
	public List<byte> GetBytes()
	{
		if (controller == null)
		{
			controller = GetComponent <Rigidbody> ();
		}

		isActive = gameObject.activeSelf;

		Debug.Log ("this trap power up is" + isActive);
		m_myByteList.Clear ();

		//1 byte message version
		//1 byte type of message
		//24 bytes for 6 floats
		//26 bytes
		m_myByteList.Add (m_messageVersion);
		m_myByteList.AddRange (System.BitConverter.GetBytes (isActive));
		//m_myByteList.Add ((byte)'U');
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.position.x));
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.position.y));
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.position.z));
		m_myByteList.AddRange (System.BitConverter.GetBytes (controller.velocity.x));
		m_myByteList.AddRange (System.BitConverter.GetBytes (controller.velocity.z));
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.rotation.y));

		//MultiplayerManager.Instance.ShowMPStatus ("converted my bytes");
		//MultiplayerManager.Instance.ShowMPStatus (controller.velocity.x.ToString ());
		return m_myByteList;
	}

	//readBytes(byte[])
	//change byte array into pos, rot, etc
	public void ReadBytes (byte[] a_byteMessage, int a_startPoint)
	{
		if (controller == null)
		{
			controller = GetComponent <Rigidbody> ();
		}
		//read through byte[] grab correct parts to udpates myself
		byte messageVersion = (byte)a_byteMessage[0];

		bool crateActive = (System.BitConverter.ToBoolean (a_byteMessage, a_startPoint+1));
		a_startPoint += 2;
		if (crateActive) 
		{
			Debug.Log ("trap being activated");
			this.gameObject.SetActive (true);
			float xPos = System.BitConverter.ToSingle (a_byteMessage, a_startPoint);
			float yPos = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 4);
			float zPos = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 8);
			float xVel = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 12);
			float zVel = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 16);
			float yRot = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 20);

			m_startPos = transform.position;
			m_startRot = transform.rotation;
			m_destinationPos = new Vector3 (xPos, yPos, zPos);
			m_destinationRot = Quaternion.Euler (0, yRot, 0);
		} 

		else 
		{
			Debug.Log ("trap being deactivated");
			this.gameObject.SetActive (false);
		}
	}

	public Transform ReadBytes (byte[] a_senderData)
	{
		return this.transform;
	}
}