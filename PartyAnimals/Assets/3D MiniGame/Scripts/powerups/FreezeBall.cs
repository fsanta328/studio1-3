﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FreezeBall : MonoBehaviour, PowerUp
{
	public int m_itemNum;
	public bool m_active = false;
	public Vector3 freezDirection;
	public float freezSpeed;
	public  bool isFreezed;
	float Duration = 3;
	float m_lifeSpan;
	internal CapsuleCollider m_playerCapsuleCollider;

	//NEW added
	public List<byte> m_myByteList = new List<byte>();
	private byte m_messageVersion = 1;
	private Rigidbody controller;
	private Vector3 m_startPos;
	private Vector3 m_destinationPos;
	private Quaternion m_startRot;
	private Quaternion m_destinationRot;
	private bool isActive = false;

	//NEW added
	public GameObject m_powerOwner;
	public ParticleSystem m_particle;
	private int m_powerIndex =1;

	void Start()
	{
		//NEW added
		m_particle = GetComponent<ParticleSystem>();
		controller = GetComponent <Rigidbody>();

		m_startPos = transform.position;
		m_startRot = transform.rotation;

		m_destinationPos = transform.position;
		m_destinationRot = transform.rotation;

		m_lifeSpan = 4f;
	}

	void Update()
	{
		m_lifeSpan -= Time.deltaTime;

		if (m_lifeSpan <= 0) 
		{
			Destroy (this.gameObject);
		}

		if (m_active == true) 
		{
			PowerBehaviour ();
		}

//		#if !UNITY_EDITOR
//		//NEW added
//		//do only if it is not host
//		if (!CrateCrashManager.Instance.m_isHost)
//		{
//			transform.position = m_destinationPos;
//			transform.rotation = m_destinationRot;
//		}
//		#endif
	}

	public void SetDirection(Vector3 Direction)
	{
		freezDirection = Direction;
	}

	void OnTriggerEnter(Collider a_collision)
	{
		//do only on host
		if (CrateCrashManager.Instance.m_isHost)	
		{
			if ((a_collision.gameObject != m_powerOwner) && (m_active == true)) 
			{
				//find who it hit through index
				//then send message
				//hostspecialmessage bytearray and index (who was hit)
				//client who recieves message call frozen() function
				//reverse similar of Special event message
				byte[] a_byteMessage;
				int playerIndex = a_collision.gameObject.GetComponent<PlayerControls> ().posInList;
				//1 byte version
				//1 byte type
				//8 bytes for 2 int
				//10byte message
				m_myByteList.Clear ();
				m_myByteList.Add (m_messageVersion);
				m_myByteList.Add ((byte)'C');
				m_myByteList.AddRange (System.BitConverter.GetBytes (m_powerIndex));
				m_myByteList.AddRange (System.BitConverter.GetBytes (Duration));
				a_byteMessage = m_myByteList.ToArray ();
				CrateCrashManager.Instance.HostSendSpecialMessage (a_byteMessage, playerIndex);

				StartCoroutine(a_collision.gameObject.GetComponent<PlayerControls> ().Frozen(Duration));
				//a_collision.gameObject.GetComponent<OpponentController>().m_currentState = PlayerState.Frozen;
				Debug.Log("state of hit opponent = " + a_collision.gameObject.GetComponent<PlayerControls>().m_currentState);
				m_particle.Stop();
				m_active = false;
			}
		}
		//add for clients?
//		else
//		{
//			m_particle.Stop();
//			m_active = false;
//		}
	}

	public void ActivatePower(GameObject a_owner)
	{
		m_powerOwner = a_owner;
		m_active = true;
		SetDirection (a_owner.transform.forward);
	}

	public void ActivatePower(GameObject a_owner, Transform a_transform)
	{
		m_powerOwner = a_owner;
		m_active = true;
		SetDirection (a_transform.forward);
	}

	public void PowerBehaviour()
	{
		transform.position += freezDirection * freezSpeed * Time.deltaTime;
		m_active = true;
	}
		
	public int SelectItem ()
	{
		return 0;
	}

	void DestroyThisObject()
	{
		Destroy(this.gameObject);
	}

	public void PowerUpDuration()
	{
		Destroy(this.gameObject, 5f);
	}

	IEnumerator PowerUpDurations(Collider a_collision) 
	{
		yield return new WaitForSeconds(Duration); 
		a_collision.gameObject.GetComponent<OpponentController> ().m_currentState = PlayerState.Idle;
		Debug.Log("state of hit opponent = " + a_collision.gameObject.GetComponent<OpponentController>().m_currentState);
	}

	//NEW added
	public List<byte> GetBytes()
	{
		if (controller == null)
		{
			controller = GetComponent <Rigidbody> ();
		}

		isActive = gameObject.activeSelf;

		Debug.Log ("this freeze power up is" + isActive);
		m_myByteList.Clear ();

		//1 byte message version
		//1 byte type of message
		//24 bytes for 6 floats
		//26 bytes
		m_myByteList.Add (m_messageVersion);
		m_myByteList.AddRange (System.BitConverter.GetBytes (isActive));
		//m_myByteList.Add ((byte)'U');
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.position.x));
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.position.y));
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.position.z));
		m_myByteList.AddRange (System.BitConverter.GetBytes (controller.velocity.x));
		m_myByteList.AddRange (System.BitConverter.GetBytes (controller.velocity.z));
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.rotation.y));

		//MultiplayerManager.Instance.ShowMPStatus ("converted my bytes");
		//MultiplayerManager.Instance.ShowMPStatus (controller.velocity.x.ToString ());
		return m_myByteList;
	}

	//readBytes(byte[])
	//change byte array into pos, rot, etc
	public Transform ReadBytes (byte[] a_byteMessage)
	{
		if (controller == null)
		{
			controller = GetComponent <Rigidbody> ();
		}
		//read through byte[] grab correct parts to udpates myself
		byte messageVersion = (byte)a_byteMessage[0];

		//bool crateActive = (System.BitConverter.ToBoolean (a_byteMessage, a_startPoint+1));
		//a_startPoint += 2;
//		if (crateActive) 
//		{
		Debug.Log ("freeze read bytes");
//			this.gameObject.SetActive (true);
//			float xPos = System.BitConverter.ToSingle (a_byteMessage, a_startPoint);
//			float yPos = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 4);
//			float zPos = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 8);
//			float xVel = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 12);
//			float zVel = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 16);
//			float yRot = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 20);

		int a_powerIndex = System.BitConverter.ToInt32 (a_byteMessage, 2);
		float xPos = System.BitConverter.ToSingle (a_byteMessage, 6);
		float yPos = System.BitConverter.ToSingle (a_byteMessage, 10);
		float zPos = System.BitConverter.ToSingle (a_byteMessage, 14);
		float xRot = System.BitConverter.ToSingle (a_byteMessage, 18);
		float zRot = System.BitConverter.ToSingle (a_byteMessage, 22);

		m_startPos = transform.position;
		m_startRot = transform.rotation;
		m_destinationPos = new Vector3 (xPos, yPos, zPos);
		m_destinationRot = Quaternion.Euler (xRot, 0, zRot);

		Vector3 objForward = new Vector3 (xRot, 0, zRot);

		m_destinationRot = Quaternion.LookRotation (objForward.normalized); 

		Transform a_transform = this.transform;
		a_transform.position = m_destinationPos;
		a_transform.rotation = m_destinationRot;
		return a_transform;
	}
}
