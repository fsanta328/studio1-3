﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PowerUpManager : MonoBehaviour 
{
	public static int powerUpUsed= 0;

	//function to assign id power to array of power ups
	public GameObject[] m_powerUpArray;
	public PowerUp m_powerUpI;
	public PlayerControls m_playerControls;
	public GameObject m_powerUpPos;

	//NEW added
	private static PowerUpManager m_POWInstance;
	protected byte m_messageVersion = 1;
	public static PowerUpManager Instance { get { return m_POWInstance; } }

	private Vector3 m_startPos;
	private Quaternion m_startRot;

	private void Awake()
	{
		if (m_POWInstance != null && m_POWInstance != this)
		{
			Destroy(this.gameObject);
		} 

		else 
		{
			m_POWInstance = this;
		}
	}

//	public void InstantiatePowerUp(int b_power, Transform b_transform)
//	{
//		
//	}
		
	public void ActivatePowerUp(int a_power, GameObject a_owner)
	{
		//instantiate power up as host and send message to opponents
		GameObject tempItem = m_powerUpArray [a_power];
		m_powerUpI = tempItem.gameObject.GetComponent (typeof(PowerUp)) as PowerUp;
		//InstantiatePowerUp (a_power, a_owner.transform);
		//Debug.Log ("powerup owner is" + a_owner);

		GameObject abc_power = Instantiate (m_powerUpArray [a_power], a_owner.transform.position, a_owner.transform.rotation) as GameObject;
		//abc_power.GetComponent<PowerUp>()
		abc_power.GetComponent<PowerUp>().ActivatePower(a_owner);
		// For save and load purposes
		powerUpUsed ++;
	}

	public void ActivatePowerUp(int a_power, GameObject a_owner, byte[] a_senderInfo)
	{
		//instantiate power up as host and send message to opponents
		GameObject tempItem = m_powerUpArray [a_power];
		m_powerUpI = tempItem.gameObject.GetComponent (typeof(PowerUp)) as PowerUp;
		Debug.Log ("powerup owner is" + a_owner);
		int a_powerIndex = System.BitConverter.ToInt32 (a_senderInfo, 2);

		Transform a_transform = m_powerUpI.ReadBytes (a_senderInfo);
		Debug.Log ("index from function" + a_power);
		Debug.Log ("power index from owner is" + a_powerIndex);
		Debug.Log ("transform pos =" + a_transform.position);

		GameObject abc_power = Instantiate (m_powerUpArray [a_powerIndex], 
								new Vector3(a_transform.position.x, a_transform.position.y + 1, a_transform.position.z),
								m_powerUpI.ReadBytes (a_senderInfo).rotation) as GameObject;
		
		Debug.Log ("instantiated" + abc_power.name);
		Debug.Log ("pos X =" + a_transform.position.x +" pos Y =" + a_transform.position.y + " pos Z =" + a_transform.position.z);
		Debug.Log ("rot X =" + a_transform.rotation.x +" rot Y =" + a_transform.rotation.y + " rot Z =" + a_transform.rotation.z);

		abc_power.GetComponent<PowerUp>().ActivatePower(a_owner, a_transform);

		// For save and load purposes
		powerUpUsed ++;
	}

//	public void ActivatePowerUp(int a_power, GameObject a_owner)
//	{
//		//instantiate power up as host and send message to opponents
//		GameObject tempItem = m_powerUpArray [a_power];
//		m_powerUpI = tempItem.gameObject.GetComponent (typeof(PowerUp)) as PowerUp;
//		//InstantiatePowerUp (a_power, a_owner.transform);
//		Debug.Log ("powerup owner is" + a_owner);
//
//		GameObject abc_power = Instantiate (m_powerUpArray [a_power], a_owner.transform.position, a_owner.transform.rotation) as GameObject;
//		abc_power.GetComponent<PowerUp>().ActivatePower(a_owner);
//		// For save and load purposes
//		powerUpUsed ++;
//	}

	public void PlayerActivatePowerUp()
	{
		//Debug.Log ("power button pressed");
		GameObject playerObj = GameObject.FindWithTag ("Character");
		//Debug.Log (playerObj.name);

		m_playerControls = playerObj.GetComponent <PlayerControls> ();
		if (m_playerControls.m_currentState != PlayerState.Dead) 
		{
			//Debug.Log ("power index" + m_playerControls.m_powerUpNum);
			int a_powerUpIndex = PlayerControls.m_powerUpNum;

			if ((a_powerUpIndex > m_powerUpArray.Length) || (a_powerUpIndex < 0)) 
			{
				Debug.Log ("power up num is not in array");
				return;
			}

			List<byte> a_byteList = new List<byte> ();
			byte[] a_byteMessage;
			a_byteList.Clear ();

			//			switch (a_powerUpIndex) 
			//			{
			//				case 0:
			//					a_byteList.AddRange (System.BitConverter.GetBytes (m_playerControls.gameObject.transform.rotation.y));
			//					a_byteMessage = a_byteList.ToArray ();
			//					CrateCrashManager.Instance.ClientSpecialMessage (EventType.trap, a_byteMessage);
			//					Debug.Log ("client sending trap message");
			//					break;
			//
			//				case 1:
			//					a_byteList.AddRange (System.BitConverter.GetBytes (m_playerControls.gameObject.transform.rotation.y));
			//					a_byteMessage = a_byteList.ToArray ();
			//					CrateCrashManager.Instance.ClientSpecialMessage (EventType.freezeball, a_byteMessage);
			//					Debug.Log ("client sending freeze message");
			//					break;
			//
			//				case 2:
			//					a_byteList.AddRange (System.BitConverter.GetBytes (m_playerControls.gameObject.transform.rotation.y));
			//					a_byteMessage = a_byteList.ToArray ();
			//					CrateCrashManager.Instance.ClientSpecialMessage (EventType.sleepytime, a_byteMessage);
			//					Debug.Log ("client sending sleepytime message");
			//					break;
			//
			//				default:
			//					break;
			//			}

			#if !UNITY_EDITOR
			if (!CrateCrashManager.Instance.m_isHost) 
			{
				a_byteList.AddRange (System.BitConverter.GetBytes (a_powerUpIndex));
				a_byteList.AddRange (System.BitConverter.GetBytes (m_playerControls.gameObject.transform.position.x));
				a_byteList.AddRange (System.BitConverter.GetBytes (m_playerControls.gameObject.transform.position.y));
				a_byteList.AddRange (System.BitConverter.GetBytes (m_playerControls.gameObject.transform.position.z));
				a_byteList.AddRange (System.BitConverter.GetBytes (m_playerControls.gameObject.transform.forward.x));
				a_byteList.AddRange (System.BitConverter.GetBytes (m_playerControls.gameObject.transform.forward.z));
				a_byteMessage = a_byteList.ToArray ();
				CrateCrashManager.Instance.ClientSpecialMessage (EventType.special, a_byteMessage);
				
				Debug.Log ("power message from client sent");
				PlayerControls.m_powerUpNum = -1;
			}

			else
			#endif
			{
				ActivatePowerUp (a_powerUpIndex, playerObj);
				//m_playerControls.SetAnimation (Animate.ThrowFreezeBall);
				//TODO
				//setting player state allows animation to play, but setting state in animation through events does not play
				m_playerControls.m_currentState = PlayerState.ThrowFreezeBall;
				m_playerControls.SetAnimation (Animate.ThrowFreezeBall);
				Debug.Log ("host pressed powerup");

				a_byteList.Add (m_messageVersion);
				a_byteList.Add ((byte)'S');
				a_byteList.AddRange (System.BitConverter.GetBytes (a_powerUpIndex));
				a_byteList.AddRange (System.BitConverter.GetBytes (m_playerControls.gameObject.transform.position.x));
				a_byteList.AddRange (System.BitConverter.GetBytes (m_playerControls.gameObject.transform.position.y));
				a_byteList.AddRange (System.BitConverter.GetBytes (m_playerControls.gameObject.transform.position.z));
				a_byteList.AddRange (System.BitConverter.GetBytes (m_playerControls.gameObject.transform.forward.x));
				a_byteList.AddRange (System.BitConverter.GetBytes (m_playerControls.gameObject.transform.forward.z));
				//add direction in message
				a_byteMessage = a_byteList.ToArray ();
				CrateCrashManager.Instance.HostSendSpecialMessage (a_byteMessage, 0);
				PlayerControls.m_powerUpNum = -1;
				//			switch (a_powerUpIndex) 
				//			{
				//				case 0:
				//					a_byteList.Add (m_messageVersion);
				//					a_byteList.Add ((byte)'S');
				//					a_byteList.AddRange (System.BitConverter.GetBytes (m_playerControls.gameObject.transform.rotation.y));
				//					a_byteMessage = a_byteList.ToArray ();
				//					CrateCrashManager.Instance.HostSendSpecialMessage (a_byteMessage, 0);
				//					//m_playerControls.SetAnimation (Animate.);
				//					Debug.Log ("host sending trap message");
				//					break;
				//
				//				case 1:
				//					a_byteList.Add (m_messageVersion);
				//					a_byteList.Add ((byte)'S');
				//				a_byteList.AddRange (System.BitConverter.GetBytes (a_powerUpIndex));
				//					a_byteList.AddRange (System.BitConverter.GetBytes (m_playerControls.gameObject.transform.rotation.y));
				//					a_byteMessage = a_byteList.ToArray ();
				//					CrateCrashManager.Instance.HostSendSpecialMessage (a_byteMessage, 0);
				//					m_playerControls.SetAnimation (Animate.ThrowFreezeBall);
				//					Debug.Log ("host sending freeze message");
				//					break;
				//
				//				case 2:
				//					a_byteList.Add (m_messageVersion);
				//					a_byteList.Add ((byte)'Z');
				//					a_byteList.AddRange (System.BitConverter.GetBytes (m_playerControls.gameObject.transform.rotation.y));
				//					a_byteMessage = a_byteList.ToArray ();
				//					CrateCrashManager.Instance.HostSendSpecialMessage (a_byteMessage, 0);
				//					//m_playerControls.SetAnimation (Animate.ThrowFreezeBall);
				//					Debug.Log ("host sending sleep message");
				//					break;
				//
				//				default:
				//					break;
				//			}
			}
		}
	}
}