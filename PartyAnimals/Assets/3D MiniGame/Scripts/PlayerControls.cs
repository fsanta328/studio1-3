﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public enum PlayerState 
{
	Idle,
	Moving,
	Hurt,
	Dead,
	Pickup,
	Carrying,
	Throw,
	ThrowFreezeBall,
	Frozen,
	Trapped,
	Asleep,
};

public class PlayerControls : MonoBehaviour 
{	
	public float m_speed = 10;
	public GameObject m_crateHoldingHand; 
	public RuntimeAnimatorController m_animatorController;

	public float drag = 0.5f;
	public float rotSpeed = 25.0f;
	private Transform camTransform;
	public List<byte> m_myByteList = new List<byte>();
	private Rigidbody controller;
	protected byte m_messageVersion = 1;
	protected Vector3 m_startPos;
	protected Vector3 m_destinationPos;
	protected Quaternion m_startRot;
	protected Quaternion m_destinationRot;

	internal PlayerScript m_playerScript;

	public static int m_powerUpNum = -1;

	public float m_currentHealth;
	public float m_MaxPlayerHealth;
	public Image m_HealthBar;
	public Vector3 myLastVel;
	public PlayerState m_currentState;
	public int posInList;

	//NEW added
	//public GameObject m_endUIHolder;
	public GameUI m_gameUI;

	virtual public void Start () 
	{
		m_playerScript = new PlayerScript (transform ,m_speed, m_crateHoldingHand);
		
		m_playerScript.m_joystickControl ();
		m_playerScript.GetAnimator ();
		m_playerScript.m_animator.runtimeAnimatorController = m_animatorController;
		m_currentHealth = m_MaxPlayerHealth;
		// This will going to place character at some where middle of the game scene
		//m_playerScript.SetPrimaryPosition(new Vector3( UnityEngine.Random.Range ( 47, 58 ), 4, UnityEngine.Random.Range( 36, 28 ) ));
		// move up a scale
		//transform.localScale += new Vector3( 1, 1, 1 );

		//Whenever the attack button is pressed, this will make sure the character will perform attack
		m_playerScript.ActionButton ("AttackButton", OnClickAttack);

		controller = GetComponent <Rigidbody>();
		controller.maxAngularVelocity = rotSpeed;
		controller.drag = drag;

		camTransform = Camera.main.transform;
		m_currentState = PlayerState.Idle;
		m_startPos = transform.position;
		m_startRot = transform.rotation;
		//m_endUIHolder = GameObject.FindGameObjectWithTag ("EndUI");
		m_gameUI = FindObjectOfType<GameUI> ();
	}

	virtual public void Update () 
	{
		#if !UNITY_EDITOR
		if (!CrateCrashManager.Instance.m_matchReady)
			return;
		#endif

		Debug.Log("Animation Float Value   :   " + m_playerScript.m_animator.GetFloat("PlayerAnim"));
		Debug.Log("current state is " + m_currentState);

		//if player is dead or in the state of injury..
		if (m_playerScript.m_animator.GetCurrentAnimatorStateInfo(0).IsTag("Die").Equals(true) || m_playerScript.m_animator.GetCurrentAnimatorStateInfo(0).IsTag("Injured").Equals(true))
			return;
		
		if (Input.GetKeyDown (KeyCode.C))
		{
			crateCrash.Instance.InstantiateCrates ();
		}

		if (Input.GetKey (KeyCode.H))
		{
			m_currentHealth -= 10;
			//StateMachine (PlayerState.Hurt);
			//Dead (); this funciton is already in the fall back animation as an event, to check if the character has health
		}

		if ((m_currentState == PlayerState.Dead) || (m_currentState == PlayerState.Frozen) || (m_currentState == PlayerState.Trapped) ||
			(m_currentState == PlayerState.ThrowFreezeBall))
			return;

		if ((m_currentState == PlayerState.Idle) || (m_currentState == PlayerState.Moving) || (m_currentState == PlayerState.Carrying))
		{
			//if player presses action button
			if (m_playerScript.m_actionButton == true) 
			{
				//if the character is not caring any crate
				if (m_playerScript.Is_HandAniamtion(Hands.NormalHands))
				{
					//Allow him to do pickup animation
					//StateMachine (PlayerState.Pickup);
					//getting all the boxes in the scene
					foreach (GameObject x in GameObject.FindGameObjectsWithTag("crate")) 
					{
						// get the distance between the character and each crate
						m_playerScript.m_position = x.transform.position - m_playerScript.GetPrimaryPosition ();

						m_playerScript.m_distance = m_playerScript.m_position.sqrMagnitude; 

						//if one of the box is near, or in radius
						//and crate is in ready state
						if ((m_playerScript.m_distance <= m_playerScript.m_maxDistance) && (x.GetComponent <Crate>().m_currentState == State.Ready)) 
						{
							List<byte> a_byteList = new List<byte>();
							byte[] a_byteMessage;
							a_byteList.Clear ();
							m_currentState = PlayerState.Pickup;
							Debug.Log ("before play pickupAnim" + m_currentState);
							#if !UNITY_EDITOR

							//check if this is client or not
							//if client is true call SendSpecialMessage as picking up box
							if (!CrateCrashManager.Instance.m_isHost)
							{
								//a_byteList = new List<byte>();
								a_byteList.AddRange (System.BitConverter.GetBytes (x.GetComponent <Crate> ().id));
								//Debug.Log ("Sending pickup event");
								a_byteMessage = a_byteList.ToArray ();
								CrateCrashManager.Instance.ClientSpecialMessage (EventType.pickup, a_byteMessage);
								m_playerScript.m_actionButton = false;
							}

							else
							#endif
							{
								//a_byteList.AddRange (System.BitConverter.GetBytes (x.GetComponent <Crate> ().id));
								//Debug.Log ("Sending pickup event");
								#if !UNITY_EDITOR
								{
								a_byteList.Add (m_messageVersion);
								a_byteList.Add ((byte)'P');
								a_byteList.AddRange (System.BitConverter.GetBytes (x.GetComponent <Crate> ().id));
								a_byteMessage = a_byteList.ToArray ();
								CrateCrashManager.Instance.HostSendSpecialMessage (a_byteMessage, 0);
								}
								#endif
								PlayPickUpAnim (x);
								Debug.Log ("after pickup anim" + m_currentState);
								//							List<byte> a_byteList = new List<byte>();
								//							a_byteList.Add (m_messageVersion);
								//							a_byteList.Add ((byte)'P');
								//							a_byteList.AddRange (System.BitConverter.GetBytes (x.GetComponent <Crate> ().id));
								//							byte[] a_byteMessage = a_byteList.ToArray ();
								//							CrateCrashManager.Instance.HostSendSpecialMessage (a_byteMessage, 0);
								//reset the button
								m_playerScript.m_actionButton = false;
							}
							//					m_animator ().ResetTrigger ("Throw");
							//
							//					// character will perform pick up animation..
							//					SetAnimation (Animate.PickUp);
							//
							//					m_crate = x;
							//
							//					//reset the button
							//					m_actionButton = false;
						}

						else
						{
							m_playerScript.m_position = Vector3.zero;
							m_playerScript.m_distance = 0;
							//reset the button
							m_playerScript.m_actionButton = false;
						}
					}
				}

				//if character is caring the crate
				else if(m_playerScript.Is_HandAniamtion(Hands.HandsWithCrate))
				{
					//make hum throw the crate
					//StateMachine (PlayerState.Throw);
					List<byte> a_byteList = new List<byte>();
					byte[] a_byteMessage;
					a_byteList.Clear ();

					m_currentState = PlayerState.Throw;
					Debug.Log ("before throw anim" + m_currentState);

					#if !UNITY_EDITOR

					//if statement for client
					//if true send direction and start point in message for the box to follow
					if (!CrateCrashManager.Instance.m_isHost)
					{
						a_byteList = new List<byte>();
						a_byteList.AddRange (System.BitConverter.GetBytes (m_crateHoldingHand.transform.position.x));
						a_byteList.AddRange (System.BitConverter.GetBytes (m_crateHoldingHand.transform.position.y));
						a_byteList.AddRange (System.BitConverter.GetBytes (m_crateHoldingHand.transform.position.z));
						a_byteList.AddRange (System.BitConverter.GetBytes (this.gameObject.transform.forward.x));
						a_byteList.AddRange (System.BitConverter.GetBytes (this.gameObject.transform.forward.z));

						Debug.Log ("Sending crate throw message" + transform.rotation.y +""+ m_crateHoldingHand.transform.position.x);
						a_byteMessage = a_byteList.ToArray (); 
						CrateCrashManager.Instance.ClientSpecialMessage (EventType.throwing, a_byteMessage);
					}

					else
					#endif
					{
						#if !UNITY_EDITOR
						{
						a_byteList.Add (m_messageVersion);
						a_byteList.Add ((byte)'T');
						a_byteMessage = a_byteList.ToArray ();
						CrateCrashManager.Instance.HostSendSpecialMessage (a_byteMessage, 0);
						}
						#endif
						//do throwing animation
						PlayThrowAnim ();
						Debug.Log ("after throw anim" + m_currentState);
						//TossCrate ();
					}
				}
			}

			// if player is using the joystick..
			else if (m_playerScript.m_joystickControl ().GetPosition () != Vector3.zero) 
			{
				//make character move
				m_playerScript.Move ();
			}

			// if player is not using joystick
			else if (m_playerScript.m_joystickControl ().GetPosition () == Vector3.zero) 
			{
				//character plays idle animation
				SetAnimation (Animate.Idle);
			}
		}
//		// if player is using the joystick..
//		if (m_playerScript.m_joystickControl ().GetPosition () != Vector3.zero) 
//		{
//			//make character move
//			StateMachine (PlayerState.Moving);
//		}
//		// if player is not using joystick
//		else if (m_playerScript.m_joystickControl ().GetPosition () == Vector3.zero) 
//		{
//			//make character do Idle state
//			StateMachine (PlayerState.Idle);
//		}
		//Whenever the Action button is pressed..
//		if (m_playerScript.m_actionButton == true || Input.GetKey(KeyCode.A)) 
//		{
//			//if the character is not caring any crate
//			if(m_playerScript.Is_HandAniamtion(Hands.NormalHands))
//			{
//				//Allow him to do pickup animation
//				StateMachine (PlayerState.Pickup);
//			}
//			//if character is caring the crate
//			else if(m_playerScript.Is_HandAniamtion(Hands.HandsWithCrate))
//			{
//				//make hum throw the crate
//				StateMachine (PlayerState.Throw);
//			}
//		}
	}

	public void SetPlayerState(PlayerState a_state)
	{
		m_currentState = a_state;		
	}

//	public void StateMachine(PlayerState a_playerState)
//	{
//		Debug.Log ("State:   " + a_playerState.ToString ());
//
//		switch (a_playerState) 
//		{
//
//		// if: current state is set to IDLE
//		case PlayerState.Idle:
//			{
//				// make character do idle animation
//				SetAnimation (Animate.Idle);
//				break;
//			}
//		// if: current state is set to MOVE
//		case PlayerState.Moving:
//			{
//				// call the moving method that will allow player to move character, and do the animaiton accordingly
//				m_playerScript.Move ();
//				break;
//			}
//		// if: current state of the character is set to INJURD
//		case PlayerState.Hurt:
//			{
//				SetTriggerAnimation (TriggerAnimation.Injured); // do the fallback animation
//				break;
//			}
//		// if: current state of character is set to DIE
//		case PlayerState.Dead:
//			{
//				//do the die animation
//				SetAnimation (Animate.Die); 
//				break;
//			}
//		// if: current state is set to PICK UP crate
//		case PlayerState.Pickup:
//			{
//				//getting all the boxes in the scene
//				foreach (GameObject x in GameObject.FindGameObjectsWithTag("crate")) 
//				{
//					// get the distance between the character and each crate
//					m_playerScript.m_position = x.transform.position - m_playerScript.GetPrimaryPosition ();
//
//					m_playerScript.m_distance = m_playerScript.m_position.sqrMagnitude; 
//
//					//if one of the box is near, or in radius
//					//and crate is in ready state
//					if ((m_playerScript.m_distance <= m_playerScript.m_maxDistance) && (x.GetComponent <Crate>().m_currentState == State.Ready)) 
//					{
//						List<byte> a_byteList = new List<byte>();
//						byte[] a_byteMessage;
//						a_byteList.Clear ();
//
//						#if !UNITY_EDITOR
//
//						//check if this is client or not
//						//if client is true call SendSpecialMessage as picking up box
//						if (!CrateCrashManager.Instance.m_isHost)
//						{
//							//a_byteList = new List<byte>();
//							a_byteList.AddRange (System.BitConverter.GetBytes (x.GetComponent <Crate> ().id));
//							//Debug.Log ("Sending pickup event");
//							a_byteMessage = a_byteList.ToArray ();
//							CrateCrashManager.Instance.ClientSpecialMessage (EventType.pickup, a_byteMessage);
//							m_playerScript.m_actionButton = false;
//						}
//
//						else
//						#endif
//						{
//							//a_byteList.AddRange (System.BitConverter.GetBytes (x.GetComponent <Crate> ().id));
//							//Debug.Log ("Sending pickup event");
//							a_byteList.Add (m_messageVersion);
//							a_byteList.Add ((byte)'P');
//							a_byteList.AddRange (System.BitConverter.GetBytes (x.GetComponent <Crate> ().id));
//							a_byteMessage = a_byteList.ToArray ();
//							CrateCrashManager.Instance.HostSendSpecialMessage (a_byteMessage, 0);
//							PlayPickUpAnim (x);
//
//							List<byte> a_byteList = new List<byte>();
//							a_byteList.Add (m_messageVersion);
//							a_byteList.Add ((byte)'P');
//							a_byteList.AddRange (System.BitConverter.GetBytes (x.GetComponent <Crate> ().id));
//							byte[] a_byteMessage = a_byteList.ToArray ();
//							CrateCrashManager.Instance.HostSendSpecialMessage (a_byteMessage, 0);
//
//							//reset the button
//							m_playerScript.m_actionButton = false;
//						}
//
//											m_animator ().ResetTrigger ("Throw");
//						
//											// character will perform pick up animation..
//											SetAnimation (Animate.PickUp);
//						
//											m_crate = x;
//						
//											//reset the button
//											m_actionButton = false;
//					}
//
//					else
//					{
//						m_playerScript.m_position = Vector3.zero;
//
//						m_playerScript.m_distance = 0;
//
//						//reset the button
//						m_playerScript.m_actionButton = false;
//					}
//				}
//				
//				break;
//			}
//		// if: current state is set to THROW crate
//		case PlayerState.Throw:
//			{
//				List<byte> a_byteList = new List<byte>();
//				byte[] a_byteMessage;
//				a_byteList.Clear ();
//				#if !UNITY_EDITOR
//
//				if statement for client
//				if true send direction and start point in message for the box to follow
//				if (!CrateCrashManager.Instance.m_isHost)
//				{
//					a_byteList = new List<byte>();
//					a_byteList.AddRange (System.BitConverter.GetBytes (m_crateHoldingHand.transform.position.x));
//					a_byteList.AddRange (System.BitConverter.GetBytes (m_crateHoldingHand.transform.position.y));
//					a_byteList.AddRange (System.BitConverter.GetBytes (m_crateHoldingHand.transform.position.z));
//					a_byteList.AddRange (System.BitConverter.GetBytes (this.gameObject.transform.forward.x));
//					a_byteList.AddRange (System.BitConverter.GetBytes (this.gameObject.transform.forward.z));
//
//					Debug.Log ("Sending crate throw message" + transform.rotation.y +""+ m_crateHoldingHand.transform.position.x);
//					a_byteMessage = a_byteList.ToArray (); 
//					CrateCrashManager.Instance.ClientSpecialMessage (EventType.throwing, a_byteMessage);
//				}
//
//				else
//				#endif
//				{
//					a_byteList.Add (m_messageVersion);
//					a_byteList.Add ((byte)'T');
//					a_byteMessage = a_byteList.ToArray ();
//					CrateCrashManager.Instance.HostSendSpecialMessage (a_byteMessage, 0);
//					//do throwing animation
//					PlayThrowAnim ();
//					//TossCrate ();
//				}
//
//				break;
//			}
//		set current state to frozen
//		case PlayerState.Frozen:
//			{
//				// make character do idle animation
//				SetAnimation (Animate.Idle);
//				break;
//			}
//		}
//	}

	virtual public void PlayPickUpAnim(GameObject x)
	{
		//Debug.Log ("playing pickup" + x.name);
		//m_playerScript.m_animator.ResetTrigger ("Throw");
		ResetTriggerAnimation(TriggerAnimation.ThrowCrate);

		// character will perform pick up animation..
		SetAnimation (Animate.PickUp);

		//Debug.Log ("played pickup" + name);		
		m_playerScript.m_crate = x;
		x.gameObject.GetComponent <Crate>().m_currentState = State.Ready;
		x.gameObject.GetComponent <Crate> ().m_owner = this.gameObject;
		//m_currentState = PlayerState.Carrying;
		//Debug.Log ("owner is" + this.gameObject);
	}

	public void PlayThrowAnim()
	{
		//m_playerScript.m_animator.SetTrigger ("Throw");
		SetTriggerAnimation(TriggerAnimation.ThrowCrate);
		m_playerScript.m_actionButton = false;
		m_currentState = PlayerState.Idle;
		//print ("client throw");
	}

	internal void TossCrate()
	{
		if(m_playerScript.Is_HandAniamtion(Hands.HandsWithCrate))
		{
			//make crate hodling hands leave the crate
			m_playerScript.m_crate.transform.parent.DetachChildren ();

			//enable the behavior of the crate
			m_playerScript.m_crate.GetComponent<Crate> ().m_crateBehavior = true;

			//making sure character hands are down, after the crate is tossed
			HandAnimation (Hands.NormalHands);
		}
	}

	// pickup and throw
	public void OnClickAttack()
	{
		m_playerScript.m_actionButton = true;
	}

	//method to pick certain item..
	//this method will be called while character will perform certain animation
	internal void PickBox()
	{
		//Debug.Log ("box picked");

		#if !UNITY_EDITOR

		//if playper picking up box is not host
		if (!CrateCrashManager.Instance.m_isHost) 
		{
			//?
		}

		else
		#endif
		{
			//disable certain component
			m_playerScript.m_crate.GetComponent<BoxCollider> ().enabled = false;

			//set the certain item as the child of certain gameobject
			m_playerScript.m_crate.transform.SetParent (m_crateHoldingHand.transform);

			//setting the positoin of certain gameobject according to it's parent gameobject
			m_playerScript.m_crate.transform.position = m_playerScript.m_crate.transform.parent.position;

			//disable gravity of the picked item
			m_playerScript.m_crate.GetComponent<Rigidbody> ().useGravity = false;

			// freeze all the constraints of certain item
			m_playerScript.m_crate.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezeAll;

			//enabling Crate script
			m_playerScript.m_crate.GetComponent<Crate> ().enabled = true;

			// addressing crate, to make sure the crate will not collide with the character who's holding it
			m_playerScript.m_crate.GetComponent<Crate> ().m_playerCapsuleCollider = this.GetComponent<CapsuleCollider> ();

			//this is to make sure if the crate is thrown, it is not accessable to other players..
			m_playerScript.m_crate.tag = "UsedCrate";

			//this will make sure if character is not holding any crate, the hand of that certain character are in normal state
			StartCoroutine(Is_boxInHands(0.05f));
		}
	}

	//method to throw box
	//this method will be called while character will perform certain animation
	internal void ThrowBox()
	{
		//print ("client throw box method");
		m_playerScript.m_crate.gameObject.GetComponent <Crate>().m_currentState = State.Thrown;
		//if there is a certain gameobject available
		if (m_playerScript.m_crate != null && m_crateHoldingHand.transform.childCount != 0) 
		{
			//rotate certain item according to certain diraction
			m_playerScript.m_crate.transform.eulerAngles = new Vector3 (0, transform.eulerAngles.y, 0);

			//throw crate
			TossCrate ();
		}

		else 
		{
			//This will make sure if the character is not holding crate, it would not act as holding a crate 
			HandAnimation(Hands.NormalHands);
		}
	}

	//Method to change the weight of certain layer
	//check if player is holding
	public void HandAnimation(Hands a_handsAnimation)
	{
		//Debug.Log ("hands animated");
		m_playerScript.m_animator.SetLayerWeight (1, (int)a_handsAnimation);
	}

	internal void DroppingTrap(Hands a_hands)
	{
		m_playerScript.m_animator .SetLayerWeight (2, (int)a_hands);
	}

	//TODO
	//Need an event (Dropping Potion)  that will set-layer-weight (3, 1);

	public void SetAnimation(Animate a_animation)
	{
		m_playerScript.SetAnimation (a_animation);
	}

	//this method will make certain if the character is not holding crate, it would not act as holding a crate 
	internal IEnumerator Is_boxInHands(float waitTime) 
	{
		yield return new WaitForSeconds(waitTime);

		//if there is no crate in the hands of the character, and the character is stilling doing the crate holding animation..
		if (m_playerScript.Is_HandAniamtion(Hands.HandsWithCrate) && m_crateHoldingHand.transform.childCount == 0)
		{
			//go back to normal hands animation layer
			HandAnimation(Hands.NormalHands);
		}
	}

	public void SetTriggerAnimation(TriggerAnimation a_animation)
	{
		m_playerScript.m_animator.SetTrigger (a_animation.ToString());
	}

	public void ResetTriggerAnimation(TriggerAnimation a_animation)
	{
		m_playerScript.m_animator.ResetTrigger (a_animation.ToString ());
	}
		
	//NEW added
	public void DisableCollider()
	{
	}

	public void EnableCollider()
	{
		
	}

	virtual public bool Is_playerAlive()
	{
		return m_currentHealth > 0;
	}
		
	public void Dead()
	{
		Debug.Log ("current state before sending death" + m_currentState);
		if (m_currentHealth <= 0) 
		{
			//this.m_currentState = PlayerState.Dead;

			#if (!UNITY_EDITOR)
			//CrateCrashManager.Instance.a_playersAlive--;
			//Debug.Log ("players remain after death function" + CrateCrashManager.Instance.a_playersAlive);

			if (!CrateCrashManager.Instance.m_isHost) 
			{
//				Debug.Log ("client player dead");
//				m_myByteList.Clear ();
//				byte[] a_byteMessage = m_myByteList.ToArray ();
//				m_currentState = PlayerState.Dead;
//				CrateCrashManager.Instance.ClientSpecialMessage (EventType.death, a_byteMessage);
//				//CrateCrashManager.Instance.a_playersAlive--;
//				//m_playerScript.SetAnimation (Animate.Die);
//				Debug.Log ("final state client" + m_currentState);
			} 

			else
			#endif
			{
				//find index of who died and replace into HostSendSpecialMessage
				Debug.Log ("Player " + posInList + " in host game is dead");
				m_currentState = PlayerState.Dead;
				#if (!UNITY_EDITOR)
				{
				m_myByteList.Clear ();
				m_myByteList.Add (m_messageVersion);
				m_myByteList.Add ((byte)'D');
				byte[] a_byteMessage = m_myByteList.ToArray ();
				CrateCrashManager.Instance.HostSendSpecialMessage (a_byteMessage, posInList); 
				}
				#endif
				//SetTriggerAnimation (TriggerAnimation.Die);

				m_playerScript.SetAnimation (Animate.Die);
				Debug.Log ("final state host" + m_currentState);

				if (CrateCrashManager.Instance.m_isHost) 
				{
					Debug.Log ("playerlist count before remove" + CrateCrashManager.Instance.playersList.Count);

					CrateCrashManager.Instance.m_losersList.Add (posInList);
					CrateCrashManager.Instance.playersList.Remove (posInList);

					Debug.Log ("playerlist count" + CrateCrashManager.Instance.playersList.Count);
					if (CrateCrashManager.Instance.playersList.Count <= CrateCrashManager.Instance.a_allPlayers.Count - (CrateCrashManager.Instance.a_allPlayers.Count - 1))
					{
						//send show leaderboards standing event message

						Debug.Log ("adding remaining player to loser list" + CrateCrashManager.Instance.playersList[0]);
						CrateCrashManager.Instance.m_losersList.AddRange (CrateCrashManager.Instance.playersList);
						m_myByteList.Clear ();
						m_myByteList.Add (m_messageVersion);
						m_myByteList.Add ((byte)'L');
						m_myByteList.AddRange (CrateCrashManager.Instance.ReturnLosersList (CrateCrashManager.Instance.m_losersList));
						byte[] messageToSend = m_myByteList.ToArray ();
						CrateCrashManager.Instance.HostSendSpecialMessage (messageToSend, 0);
						//enable end game UI
						m_gameUI.SetStandings (CrateCrashManager.Instance.m_losersList);
						Debug.Log("pass" + m_gameUI.name);
						CrateCrashManager.Instance.StartCoroutine("EndUIDelay");
						//SceneManager.LoadScene (2);
					}
				}
			} 
		}
		else
		{
			SetPlayerState (PlayerState.Idle);
		}
	}

	virtual public IEnumerator Frozen(float a_duration)
	{
		//#if !UNITY_EDITOR
		//check if index is not host
//		if (!CrateCrashManager.Instance.m_isHost)
//		{
//			List<byte> a_byteList = new List<byte>();
//			Debug.Log ("Sending frozen event as client");
//			byte[] a_byteMessage = a_byteList.ToArray ();
//			CrateCrashManager.Instance.ClientSpecialMessage (EventType.frozen, a_byteMessage);
//		}
//
//		else
//		//#endif
//		{
//			List<byte> a_byteList = new List<byte>();
//			a_byteList.Add (m_messageVersion);
//			a_byteList.Add ((byte)'F');
//			Debug.Log ("Sending injury event as host");
//			byte[] a_byteMessage = a_byteList.ToArray ();
//			CrateCrashManager.Instance.HostSendSpecialMessage (a_byteMessage, posInList);
//		}
		Debug.Log ("state is " + m_currentState);
		PlayerState a_originalState = m_currentState;
	
		Debug.Log ("freeze and in state " + m_currentState);
		m_currentState = PlayerState.Frozen;
		yield return new WaitForSeconds(a_duration);

		m_currentState = a_originalState;
		Debug.Log ("freeze end state " + m_currentState);
	}

	virtual public void TakenDamage(float a_DamageAmount)
	{
		//check if player is client send event message of being injured
		//else play injure animation and send to clients
		//gameObject.GetComponent <PlayerControls> ().SetAnimation (Animate.Injured);
		gameObject.GetComponent<PlayerControls> ().SetTriggerAnimation(TriggerAnimation.Injured);

		#if !UNITY_EDITOR
		//check if index is not host
		if (!CrateCrashManager.Instance.m_isHost)
		{
			List<byte> a_byteList = new List<byte>();
			Debug.Log ("Sending injury event as client");
			byte[] a_byteMessage = a_byteList.ToArray ();
			CrateCrashManager.Instance.ClientSpecialMessage (EventType.injured, a_byteMessage);
			m_currentHealth -= a_DamageAmount;
			SetHealthBar();
		}

		else
		#endif
		{
			#if !UNITY_EDITOR
			{
			List<byte> a_byteList = new List<byte>();
			a_byteList.Add (m_messageVersion);
			a_byteList.Add ((byte)'I');
			Debug.Log ("Sending injury event as host");
			byte[] a_byteMessage = a_byteList.ToArray ();
			CrateCrashManager.Instance.HostSendSpecialMessage (a_byteMessage, posInList);
			//m_playerScript.SetAnimation (Animate.Injured);
			}
			#endif
			SetTriggerAnimation(TriggerAnimation.Injured);

			m_currentHealth -= a_DamageAmount;
			SetHealthBar ();
		}
	}

	virtual public void SetHealthBar()
	{
		if (m_HealthBar == null) 
		{
			Debug.Log ("finding health bar");
			m_HealthBar = GameObject.FindGameObjectWithTag ("bar").GetComponent<Image> ();
		}
		float BarHealth = m_currentHealth / m_MaxPlayerHealth;
		m_HealthBar.transform.localScale = new Vector3 (Mathf.Clamp (BarHealth,0f,1f), 
														m_HealthBar.transform.localScale.y,
														m_HealthBar.transform.localScale.z); 
	}

	virtual public List<byte> GetBytes()
	{
		m_myByteList.Clear ();

		//1 byte message version
		//1 byte type of message
		//24 bytes for 6 floats
		//26 bytes
		m_myByteList.Add (m_messageVersion);
		m_myByteList.Add ((byte)'U');
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.position.x));
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.position.y));
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.position.z));
		m_myByteList.AddRange (System.BitConverter.GetBytes (m_playerScript.m_joystickControl().GetPosition ().x * m_speed));
		m_myByteList.AddRange (System.BitConverter.GetBytes (m_playerScript.m_joystickControl().GetPosition ().z * m_speed));
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.rotation.y));
		//Debug.Log ("sending my rotation"+ transform.rotation.y);

		return m_myByteList;
	}

	//New added
	//change byte array into pos, rot, etc
	virtual public void ReadBytes (byte[] a_byteMessage, int a_startPoint)
	{
		//read through byte[] grab correct parts to udpates myself
		a_startPoint+=2;
		float xPos = System.BitConverter.ToSingle (a_byteMessage, a_startPoint);
		float yPos = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 4);
		float zPos = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 8);
		float xVel = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 12);
		float zVel = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 16);
		float yRot = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 20);

		m_startPos = transform.position;
		m_startRot = transform.rotation;
		m_destinationPos = new Vector3 (xPos, yPos, zPos);

		//NEW change
		myLastVel = new Vector3 (xVel, 0, zVel);

		if (myLastVel.sqrMagnitude != 0)
		{
			m_destinationRot = Quaternion.LookRotation (myLastVel.normalized); 
		}
		//m_destinationRot = Quaternion.LookRotation (myLastVel.normalized);
		//m_destinationRot = Quaternion.Euler (0, yRot, 0);
		//Quaternion.LookRotation (m_lastVel.normalized); 
	}
}