﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuButtons : MonoBehaviour , MPLobbyListener
{
	public Text m_lobbyMessageText;
	private bool _showLobbyDialog;
	private string _lobbyMessage;
	private static MenuButtons m_instance;

	public GameObject m_mainMenuHolder;
	public GameObject m_optionsMenuHolder;
	public GameObject m_modeHolder;
	public GameObject m_quitCheckHolder;
	public GameObject m_creditsHolder;
	public GameObject m_statisticsHolder;

	public RectTransform m_credits;
	public Image m_fadePlane;
	public Slider[] m_volumeSliders;
	public Image m_infoHolder;

	void Awake()
	{
		//DontDestroyOnLoad (this.gameObject);
		if (!m_instance)
		{
			m_instance = this;
		} 

		else
		{
			Destroy (this.gameObject);
		}
	}

	void Start () 
	{
		m_lobbyMessageText = m_lobbyMessageText.GetComponent <Text> ();
		m_lobbyMessageText.text = "1st load in";
		MultiplayerManager.Instance.TrySilentSignIn();
	}

	public void MultiplayerMenu()
	{
		//change visuals to show "progress bar" of matchmaking
		m_lobbyMessageText.text = "Starting a multi-player game";
		m_mainMenuHolder.SetActive(false);
		m_modeHolder.SetActive (true);
	}

	public void TwoPlayerMP()
	{
		MultiplayerManager.Instance.m_lobbyListener = this;
		MultiplayerManager.Instance.SignInAndStartMPGame2 ();
	}

	public void FourPlayerMP()
	{
		MultiplayerManager.Instance.m_lobbyListener = this;
		MultiplayerManager.Instance.SignInAndStartMPGame4 ();
	}

	public void OptionsMenu()
	{
		m_mainMenuHolder.SetActive (false);
		m_optionsMenuHolder.SetActive (true);
	}

	public void StatisticsMenu()
	{
		m_mainMenuHolder.SetActive (false);
		m_optionsMenuHolder.SetActive (false);
		m_statisticsHolder.SetActive(true);
	}

	public void MainMenu()
	{
		m_mainMenuHolder.SetActive (true);
		m_optionsMenuHolder.SetActive (false);
		m_modeHolder.SetActive (false);
		m_quitCheckHolder.SetActive (false);
		m_statisticsHolder.SetActive(false);
	}

	public void Credits()
	{
		m_infoHolder.gameObject.SetActive (false);
		m_optionsMenuHolder.SetActive (false);
		StartCoroutine (Fade (Color.clear, Color.black, 1));
		m_creditsHolder.SetActive (true);
		StartCoroutine ("AnimateCredits");
		m_fadePlane.color = Color.clear;
	}

	public void ReturnToOptions()
	{
		m_optionsMenuHolder.SetActive (true);
		StopCoroutine ("AnimateCredits");
		m_fadePlane.color = Color.clear;
		m_creditsHolder.SetActive (false);
		m_infoHolder.gameObject.SetActive (true);
	}

	public void SignOut()
	{
		//check if user is already signed in, then allow sign out
		if (MultiplayerManager.Instance.IsAuthenticated ())
		{
			MultiplayerManager.Instance.SignOut ();
			//say that we signed out
			m_lobbyMessageText.text = "user signed out";
		}
	}

	public void QuitCheck ()
	{
		m_mainMenuHolder.SetActive (false);
		m_quitCheckHolder.SetActive (true);
	}

	public void GoToScene(string a_GoToScene)
	{
		SceneManager.LoadScene(a_GoToScene);
	}

	public void TogglePanel (GameObject panel) 
	{
		panel.SetActive (!panel.activeSelf);
	}

	public void Quit()
	{
		Application.Quit();
	}

	IEnumerator AnimateCredits()
	{
		float delayTime = 2f;
		float speed = .04f;
		float animationPercent = 0;
		int dir = 1;
		float endDelayTime = Time.time + 1 / speed + delayTime;

		while (animationPercent >= 0) 
		{
			animationPercent += Time.deltaTime * speed * dir;

//			if (animationPercent >= 1)
//			{
//				animationPercent = 1;
//				if (Time.time > endDelayTime) 
//				{
//					dir = -1;
//				}
//			}

			m_credits.anchoredPosition = Vector2.up * Mathf.Lerp (-650, 700, animationPercent);
			yield return null;
		}
	}

	IEnumerator Fade(Color a_from, Color a_to, float a_time)
	{
		float speed = 1 / a_time;
		float percent = 0;

		while (percent < 1) 
		{
			percent += Time.deltaTime * speed;
			m_fadePlane.color = Color.Lerp (a_from, a_to, percent);
			yield return null;
		}
	}

	public void SetLobbyStatusMessage (string message)
	{
		_lobbyMessage = message;
		m_lobbyMessageText.text = _lobbyMessage;
	}

	public void HideLobby ()
	{
		throw new System.NotImplementedException ();
	}
}
