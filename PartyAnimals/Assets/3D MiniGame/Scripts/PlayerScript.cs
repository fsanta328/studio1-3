﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public enum Hands
{
	HandsWithCrate = 1,
	NormalHands = 0,
	DroppingTheTrap = 1,
};

public class PlayerScript 
{
	internal float m_speed, m_distance, m_maxDistance = 10;
	internal GameObject m_crate, m_crateHoldingHand;
	internal Vector3 m_position;
	internal bool m_actionButton;
	internal Transform m_transform;
	internal Animator m_animator { get; set; }

	public PlayerScript(Transform a_transform,float a_speed, GameObject a_crateHoldingHand)
	{
		m_transform = a_transform;
		m_speed = a_speed;
		m_crateHoldingHand = a_crateHoldingHand;
	}

	public PlayerScript(){}

	internal Animator GetAnimator()
	{
		m_animator = m_transform.gameObject.GetComponent<Animator> ();

		return m_animator;
	}

	internal void SetAnimation(Animate a_animate)
	{
		m_animator.SetFloat ("PlayerAnim", (int)a_animate);	
	}

	//change animtaor values to reflect enum animate
	internal bool Is_AnimaitonState(Animate a_animation)
	{
		//NEW added
		//change to check between the two animatios so it checks correctly
		float checkNumber = m_animator .GetFloat ("PlayerAnim");
		return  (checkNumber == (int)a_animation);
			//&& checkNumber < 2 +(int)a_animation);
	}

	internal JoystickControl m_joystickControl()
	{
		GameObject a_virtualController = GameObject.Find("JoystickBackground");

		return a_virtualController.GetComponent<JoystickControl> ();
	}

	internal Vector3 GetPrimaryPosition()
	{
		return m_transform.position;
	}

	internal Vector3 SetPrimaryPosition(Vector3 a_position)
	{
		return m_transform.position = a_position;
	}

	internal Button ActionButton(string a_buttonTag, UnityEngine.Events.UnityAction action)
	{
		// This is going to find the attack button in hierarchy
		Button a_button = GameObject.FindGameObjectWithTag (a_buttonTag).GetComponent<Button> ();

		a_button.onClick.AddListener (action);

		return a_button;
	}

	// this Vecter 3 will help rotate character according to the virtual joystick
	internal Vector3 VirtualPosition()
	{
		return new Vector3
			(
				GetPrimaryPosition().x + m_joystickControl().GetPosition ().x,
				0,
				GetPrimaryPosition().z + m_joystickControl().GetPosition ().z
			);
	}

	internal float GetSpeed()
	{
		return m_speed * Time.deltaTime;
	}

	internal bool Is_characterThrowingFreezeBall()
	{
		return m_animator.GetCurrentAnimatorStateInfo (0).IsTag ("FreezeBall").Equals (true);
	}

	internal void Move()
	{
		SetAnimation (Animate.Walk);

		// distance between vitual position and the postion of the character
		Vector3 a_distance = VirtualPosition () - GetPrimaryPosition ();

		// the character will rotate at the diraction of the virtual position
		m_transform.rotation = Quaternion.LookRotation (new Vector3 (a_distance.x, 0, a_distance.z));

		// this will make sure character moves in the diraction as according to the virtual joysitck
		m_transform.position += new Vector3 
			(
				(m_joystickControl ().GetPosition ().x * GetSpeed ()), 
				0, 
				(m_joystickControl ().GetPosition ().z * GetSpeed ())
			);
	}

	internal bool Is_HandAniamtion(Hands a_hands)
	{
		return m_animator .GetLayerWeight (1) == (int)a_hands;
	}
}
