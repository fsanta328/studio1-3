﻿using UnityEngine;
using System.Collections;

public class ParticleHolder : MonoBehaviour {

	public ParticleSystem[] effects;
	public AudioSource[] clips;
	ParticlePool particlePool;



	void Start()
	{
		// Spawn 5 effects from each type. ( in this case 10 effects , 5 for normal crate , 5 for TNT)
		particlePool = new ParticlePool(effects[0], effects[1], 5);
	}

	// Call function to play the particle effect that you need
	public void playParticle(int particleNumber, Vector3 particlePos , int audioNumber)
	{
		ParticleSystem particleToPlay = particlePool.getAvailabeParticle(particleNumber);

		if (particleToPlay != null)
		{
			// if the effect is playing , stop it
			if (particleToPlay.isPlaying)
				particleToPlay.Stop();

			particleToPlay.transform.position = particlePos;
			// play effect and audio
			particleToPlay.Play();
			clips[audioNumber].Play();
		}

		}
}
