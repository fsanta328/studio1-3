﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class crateCrash : MonoBehaviour {

	public int noOfCrates;

	public Transform[] spawnPoints;

	private static crateCrash ccM_Instance;

	public float[] percentageOfCrateType;
	public GameObject[] TypeOfObstacles;

	float randomFloat;

	public List<Transform> freePositions;
	public List<Transform> TakenPositions;
	public List<GameObject>[] cratepool;

	//public float m_attackDamage = 5;

	GameObject obj;
	public float RespawnCrateDistancePostion;
	public float crateYpos;

	//NEW Added
	public List<Crate> m_listOfAllCrates;

	public static crateCrash Instance { get { return ccM_Instance; } }

	void Awake()
	{
		ccM_Instance = this;

		m_listOfAllCrates = new List<Crate> ();
		freePositions = new List<Transform>(spawnPoints);
		TakenPositions = new List<Transform>();

		//particle = GameObject.Find("ParticlesHolder").GetComponent<ParticleHolder>();

		cratepool = new List<GameObject>[TypeOfObstacles.Length];

		int m = 0;
		for (int i =0; i <cratepool.Length; i++)
		{
			cratepool[i] = new List<GameObject>();

			for (int j =0; j <noOfCrates; j++)
			{
				obj = Instantiate(TypeOfObstacles[i], Vector3.zero, Quaternion.identity) as GameObject;
				cratepool[i].Add(obj);
				m_listOfAllCrates.Add (obj.GetComponent <Crate>());
				//Debug.Log (obj.GetComponent <Crate>());
				obj.SetActive(false);
				obj.GetComponent <Crate>().id = m;
				m++;
				//Debug.Log (m);
			}
		}
	}

	public void InstantiateCrates()
	{
		MainSpawner();
	}

	void MainSpawner()
	{
		//Loop's Thorugh no. of crates and spawn then 
		for (int i = 0; i < noOfCrates; i++)
		{
			Spawn();
		}
	}
		
	public void Respawn(Crate a_Respawn)
	{
		//tag is renamed to make sure it can be reused again..
		a_Respawn.tag = "crate";

		a_Respawn.m_owner = null;
		a_Respawn.m_currentState = State.Ready;

		Crate a_crate = a_Respawn.GetComponent<Crate> ();

		//this will make sure that crate collides with its previous holding character
		a_crate.m_playerCapsuleCollider = null;
		a_crate.m_crateBehavior = false;

		Rigidbody a_rigidbody = a_Respawn.GetComponent<Rigidbody> ();

		//this is to assure crate do not get moved while on the ground
		a_rigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;

		for (int i = 0; i < TakenPositions.Count; i++)
		{
			// If our taken position is occupied then set it to free so crates can respawn again at that point.
			float distance = Vector3.Distance(TakenPositions[i].position,a_Respawn.OccupiedCratePosition);

			if (distance <=RespawnCrateDistancePostion)
			{
				// Add the taken position to my free
				freePositions.Add(TakenPositions[i]);
				TakenPositions.RemoveAt(i);
				break;
			}
		}
		cratepool[a_Respawn.typeOfCrate].Add(a_Respawn.gameObject);

		// Respawn the crate after "X" amount of time
		StartCoroutine(RespawnObjectAfterDelay(a_Respawn, 2.0f));
	}

	// Delay the respawn of the crate after a certain time.
	private IEnumerator RespawnObjectAfterDelay(Crate toRespawn, float delay)
	{
		yield return new WaitForSeconds(delay);
		SpawnNewObstacle(transform.position);
	}

	private void Spawn()
	{
		// Just handles a error
		if (freePositions.Count <= 0)
			return;

		// Returns value between 0-1
		randomFloat = Random.value;
		int pointsIndex = Random.Range(0, freePositions.Count);

		// Create crates depending on the percentage that we get. 0-0.8 = Normal , 0.8 - 1 = special 
		for (int j = 0; j < percentageOfCrateType.Length; j++)
		{
			if (randomFloat < percentageOfCrateType[j])
			{
				//FIX PSOTIONS GENERATION
				Vector3 tempPos = freePositions[pointsIndex].position;

				tempPos.y = crateYpos;

				cratepool[j][0].GetComponent<Crate>().OccupiedCratePosition = tempPos;

				// Remove the free position when we instantiate the crates. and put them into the taken position
				TakenPositions.Add(freePositions[pointsIndex]);
				freePositions.RemoveAt(pointsIndex);

				// Acticate the crates depending on the No. of crates.
				cratepool[j][0].SetActive(true);
				// Now this position is occupied and taken.
				cratepool[j][0].transform.position = tempPos;

				// Use the right effect depending on the type of crate
				// Normal Crate
				if (j == 0)
				{
					
					cratepool[j][0].GetComponent<Crate>().SetParticleArray(0);
					cratepool[j][0].GetComponent<Crate>().SetAudioArray(0);
				}

				//TNT crate
				if (j == 1)
				{
					cratepool[j][0].GetComponent<Crate>().SetParticleArray(1);
					cratepool[j][0].GetComponent<Crate>().SetAudioArray(1);

				}

				cratepool[j].RemoveAt(0);
				break;
			}
		}
	}

	public void SpawnNewObstacle(Vector3 freePos)
	{
		Spawn();
	}
}
