﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour 
{
	public float m_currentHealth = 0f;
	public float m_MaxPlayerHealth = 50f;

	public Image m_HealthBar;

	public GameObject freezball, m_trap , m_sleepTime;
	public Transform freePOs;

	internal PlayerScript m_playerScript;
	Button m_button;

	//Summay
	float speed = 5f;

	//NEW added
	private PlayerControls m_playerControls;
	public PowerUpManager m_powerUpMNGR;
	protected int m_powerUpNum = 1;

	// Use this for initialization
	void Start () 
	{
		m_playerScript = GetComponent<PlayerControls> ().m_playerScript;
		m_playerControls = GetComponent <PlayerControls> ();
		m_currentHealth = m_MaxPlayerHealth;

		m_HealthBar = GameObject.FindGameObjectWithTag ("bar").GetComponent<Image> ();

		m_powerUpMNGR = GameObject.FindObjectOfType <PowerUpManager> ();
		//Debug.Log ("power up mngr is" + m_powerUpMNGR);

		//Whenever the attack button is pressed, this will make sure the character will perform attack
		//m_button = m_playerScript.ActionButton ("SpecialAttackButton", DropTrap);

		m_button = m_playerScript.ActionButton ("SpecialAttackButton", 
			m_powerUpMNGR.PlayerActivatePowerUp);
	}

//	internal void ThrowFreezeBall()
//	{
//		m_playerScript.SetAnimation (Animate.ThrowFreezeBall);
//	}
//
////	internal bool Can_applyTrap()
////	{
////		bool Is_notHoldingCrate = m_playerScript.m_animator ().GetLayerWeight (1).Equals(0);
////
////		bool Is_inIdleState = m_playerScript.Is_AnimaitonState (Animate.Idle);
////
////		bool Is_inWalkingState = m_playerScript.Is_AnimaitonState (Animate.Walk);
////
////		bool Is_notInjured = !m_playerScript.Is_characterInjured ();
////
////		return  
////
////			// If character is in idle state, not holding crate and not in the state of injury..
////			Is_inIdleState.Equals(true) && Is_notHoldingCrate.Equals(true) && Is_notInjured.Equals(true)
////
////			//OR
////			||  
////			// If character is in walking state, not holding crate and not in the state of injury..
////			Is_inWalkingState.Equals(true) && Is_notHoldingCrate.Equals(true) && Is_notInjured.Equals(true);
////	}
//
//	internal void DropTrap()
//	{
//		// if certatin character is in certain state of animation..
////		if (Can_applyTrap()) 
////		{
//			//allow that certain character to drop a trap
//		m_playerControls.DroppingTrap (Hands.DroppingTheTrap);
////		}
//	}
//
//	internal void DropPotion()
//	{		
////		if (Can_applyTrap ()) 
////		{
//			//TODO
//			//Need to tap into playerScript and get (Dropping Potion)
////		}
//	}

//	internal GameObject SpecialPower(GameObject a_power)
//	{
//		GameObject Power;
//
//		Power = Instantiate (a_power , freePOs.position, Quaternion.identity) as GameObject;
//
//		return Power;
//	}
//
//	//This will be call as an event when player will press special attack button
//	internal void FreezeBallPower()
//	{
//		GameObject Power;
//
//		Power = SpecialPower (freezball);
//		//NEW added
//		//Power.GetComponent<FreezeBall>() .m_playerCapsuleCollider = this.GetComponent<CapsuleCollider> ();
//
//		Power.GetComponent<FreezeBall> ().SetDirection (transform.forward);
//
//		//new change
//		//m_button.gameObject.SetActive (false);
//	}

//	internal void SleepyTimePower()
//	{
//		GameObject Power;
//
//		Power = SpecialPower (m_sleepTime);
//
//		Power.GetComponent<SleepyTime>() .m_playerCapsuleCollider = this.GetComponent<CapsuleCollider> ();
//
//		m_button.gameObject.SetActive (false);
//	}

	//This will be call as an event when player will press special attack button
//	internal void TrapPower()
//	{
//		GameObject Power;
//
//		Power = SpecialPower (m_trap);
//
//		Power.GetComponent<Trap> ().m_playerCapsuleCollider = this.GetComponent<CapsuleCollider> ();
//
//		//if certain character was unsuccessful in dropping trap, this will give another chance to the player to apply trap
//		if(Power != null)
//		{
//			m_button.gameObject.SetActive (false);
//		}
//	}

//	bool Is_playerAlive()
//	{
//		return m_currentHealth >= 0;
//	}
//
//	internal void Dead()
//	{
//		if (!Is_playerAlive ())
//			m_playerScript.SetAnimation (Animate.Die);		
//		return;
//	}
//
//	public void TakenDamage(float a_DamageAmount)
//	{
//		Debug.Log ("health remaining" + m_currentHealth);
//		if (!Is_playerAlive ()) 
//		{
//			m_currentHealth = 0;
//		}
//
//		else 
//		{
//			m_currentHealth -= a_DamageAmount;
//			SetHealthBar();
//		}
//	}

	// Character's Health bar scaling.
	void SetHealthBar()
	{
		float BarHealth = m_currentHealth / m_MaxPlayerHealth;
		m_HealthBar.transform.localScale = new Vector3 (Mathf.Clamp (BarHealth,0f,1f) , m_HealthBar.transform.localScale.y,m_HealthBar.transform.localScale.z); 
	}
}
