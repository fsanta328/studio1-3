﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum State 
{
	Ready = 0,
	Carried = 1,
	Thrown = 2,
};

public class Crate : MonoBehaviour 
{
	protected Rigidbody m_rb;
	protected BoxCollider m_boxCollider;
	public float m_speed;
	internal bool m_crateBehavior{ get; set; }
	internal CapsuleCollider m_playerCapsuleCollider;

	int particleIndex = 0;
	int audioIndex = 0;
	public float m_attackDamage = 20;
	public Vector3 OccupiedCratePosition;
	ParticleHolder particle;

	public int typeOfCrate;

	//NEW added
	public List<byte> m_myByteList = new List<byte>();
	private byte m_messageVersion = 1;
	private Rigidbody controller;
	private Vector3 m_startPos;
	private Vector3 m_destinationPos;
	private Quaternion m_startRot;
	private Quaternion m_destinationRot;
	public float drag = 0.5f;
	public float rotSpeed = 25.0f;
	private bool isActive = false;
	public int id;
	public State m_currentState;

	public GameObject m_owner;
	bool m_lastState;
	//get who is owner of box
	//on collision enter in host game
	//send special event

	void Start()
	{
		m_currentState = State.Ready;
		m_crateBehavior = false;
		
		particle = GameObject.Find("ParticlesHolder").GetComponent<ParticleHolder>();

		m_rigidbody ();

		m_boxCollider = this.gameObject.GetComponent<BoxCollider> ();

		//NEW added
		controller = GetComponent <Rigidbody>();
		controller.maxAngularVelocity = rotSpeed;
		controller.drag = drag;

		m_startPos = transform.position;
		m_startRot = transform.rotation;

		m_destinationPos = transform.position;
		m_destinationRot = transform.rotation;

		#if !UNITY_EDITOR
		if (!CrateCrashManager.Instance.m_isHost)
		{
			controller.isKinematic = true;
			GetComponent <BoxCollider>().enabled = false;
		}
		#endif
	}

	void Update () 
	{
		//this is to make sure the character's hands are doing holding animation
		//if this crate has a parent gameobject
		if(transform.parent != null)
		{
			//get the primary parent and make sure the hands are doing holding aniamtion
			transform.root.GetComponent<PlayerControls> ().HandAnimation (Hands.HandsWithCrate);
		}
		
		if (m_crateBehavior == true)
		{
			CrateBehaviour ();

			//make this gameobject move forward
			MoveForward ();

			//disable certain component after certain time period 
			StartCoroutine (Disabler (1.0f));
		} 

		#if !UNITY_EDITOR
		//do only if it is not host
		if (!CrateCrashManager.Instance.m_isHost)
		{
			transform.position = m_destinationPos;
			transform.rotation = m_destinationRot;
			//if it 
		}
		#endif
	}

	protected BoxCollider m_collider()
	{
		return GetComponent<BoxCollider> ();
	}

	protected Rigidbody m_rigidbody()
	{
		return GetComponent<Rigidbody> ();
	}

	protected void CrateBehaviour()
	{
		Physics.IgnoreCollision (m_collider(), m_playerCapsuleCollider);
		
		//if the gravity is disable
		if (m_rigidbody ().useGravity != true) 
		{
			//freeze all rotations
			m_rigidbody ().constraints = RigidbodyConstraints.FreezeRotation;
			//make gravity enable
			m_rigidbody ().useGravity = true;
			//enable the collider
			m_boxCollider.enabled = true;
		}
	}

	protected void MoveForward ()
	{
		transform.Translate(Vector3.forward * (m_speed * Time.deltaTime));
	}

	IEnumerator Disabler(float waitTime) 
	{
		yield return new WaitForSeconds(waitTime);

		m_crateBehavior = false;
	}

	public void SetParticleArray(int _particleArrayIndex)
	{
		particleIndex = _particleArrayIndex;
	}

	public void SetAudioArray(int _AudioArrayIndex)
	{
		audioIndex = _AudioArrayIndex;
	}

	public float GetPlayerDamage()
	{
		return m_attackDamage;
	}

	public void SetDamage(int a_damage)
	{
		m_attackDamage = a_damage;
	}

	bool Is_behaviorActive()
	{
		return m_crateBehavior == true;
	}

	bool Is_containingController(GameObject a_object)
	{
		return a_object.GetComponent<PlayerHealth> () != null;
	}

	bool Is_crateUsed()
	{
		GameObject a_crate = this.gameObject;

		return a_crate.tag == "UsedCrate";
	}

//	void OnCollisionStay(Collision a_object)
//	{
//		GameObject a_character = a_object.gameObject;
//
//		// if the behavior of the crate is enabled and certain object that crate is going to collide with is carring certain component and is not in certain animation..
//		if (Is_behaviorActive () && Is_containingController (a_character) && !a_character.GetComponent<PlayerControls> ().m_playerScript.Is_AnimaitonState (Animate.Injured)) 
//		{
//			//make character do injured aniamtion
//			a_character.GetComponent<PlayerControls> ().m_playerScript.SetAnimation (Animate.Injured);
//			//apply damage to character's health
//			a_character.GetComponent<PlayerHealth> ().TakenDamage (m_attackDamage);
//
//			CrashingBehaviour ();
//		} 
//		// if the behavior of the crate is enable, but the object that crate is going to collide with is not carring certain component..
//		else if (Is_behaviorActive() && !Is_containingController(a_character) || !Is_behaviorActive() && !Is_containingController(a_character) && Is_crateUsed())
//		{
//			CrashingBehaviour ();
//		}
//	}

	void OnCollisionEnter(Collision a_collision)
	{
		Debug.Log (m_currentState + "crate state is");
		//change collison for host to check and client play effects
		if (m_currentState == State.Ready) 
		{
			//Debug.Log ("crate is ready");
		}

		if (m_currentState == State.Thrown) 
		{
			Debug.Log ("crate is in thrown state");

			if (a_collision.gameObject != m_owner) 
			{
				//check collision object position in list
				if (a_collision.gameObject.GetComponent<PlayerControls> () !=null)
				{
					int numberInList = a_collision.gameObject.GetComponent<PlayerControls> ().posInList;
					Debug.Log (numberInList + "this is the number");
					#if !UNITY_EDITOR
					if (!CrateCrashManager.Instance.m_isHost) 
					{
						//play particle effects for crate
						particle.playParticle (particleIndex, transform.position, audioIndex);
						Debug.Log ("particle playing");
					} 

					else
					#endif
					{
//						for (int i = 0; i < CrateCrashManager.Instance.a_allPlayers.Count; i++) 
//						{
//							if (numberInList == i) 
							//{
								//if (a_collision.gameObject.GetComponent <OpponentController> () != null)
//								{
//									OpponentController a_opponent = a_collision.gameObject.GetComponent<OpponentController> ();
//									a_opponent.m_currentState = PlayerState.Hurt;
//									Debug.Log ("crate hit opponent");
//									a_opponent.TakenDamage (m_attackDamage);
//									Debug.Log ("opponent health is now" + a_collision.gameObject.GetComponent <OpponentController> ().m_currentHealth);
//									CrashingBehaviour ();
//								}
						if (a_collision.gameObject.GetComponent <PlayerControls> () != null) 
						{
							PlayerControls a_player = a_collision.gameObject.GetComponent <PlayerControls> ();
							a_player.m_currentState = PlayerState.Hurt;
							Debug.Log ("crate hit player");
							a_player.TakenDamage (m_attackDamage);
							Debug.Log ("player health is now" + a_collision.gameObject.GetComponent <PlayerControls> ().m_currentHealth);
							CrashingBehaviour ();
						}
							//}

//							else
//							{
								//CrashingBehaviour ();
							//}
						//}
				
//						string colliderID = CrateCrashManager.Instance.m_myParticipantID;
//						string a_nextID;
//						for (int i = 0; i < CrateCrashManager.Instance.a_allPlayers.Count; i++) 
//						{
//							a_nextID = CrateCrashManager.Instance.a_allPlayers [i].ParticipantId;
//							if (colliderID == a_nextID) 
//							{
//								Debug.Log ("crate collided with a player");
//
//								//opponents
//								if (a_collision.gameObject.GetComponent <OpponentController> () != null)
//								{
//									Debug.Log ("crate hit opponent");
//									Debug.Log ("oppnent health is" + a_collision.gameObject.GetComponent <OpponentController> ().m_currentHealth);
//									a_collision.gameObject.GetComponent <OpponentController> ().TakenDamage (m_attackDamage, i);
//									Debug.Log ("opponent health is" + a_collision.gameObject.GetComponent <OpponentController> ().m_currentHealth);
//									CrashingBehaviour ();
//								}
//
//								//host
//								else if (a_collision.gameObject.GetComponent <PlayerControls> () != null) 
//								{
//									Debug.Log ("crate hit player");
//									Debug.Log ("player health is" + a_collision.gameObject.GetComponent <PlayerControls> ().m_currentHealth);
//									a_collision.gameObject.GetComponent <PlayerControls> ().TakenDamage (m_attackDamage, i);
//									Debug.Log ("player health is" + a_collision.gameObject.GetComponent <PlayerControls> ().m_currentHealth);
//									CrashingBehaviour ();
//								}  
//							}
//
//							else
//							{
//								CrashingBehaviour ();
//							}
//						}
					}
				}

				else
				{
					CrashingBehaviour ();
				}
			} 

			else
			{
				CrashingBehaviour ();
			}
		}
	}

	void CrashingBehaviour()
	{
		Debug.Log ("crate owner is " + m_owner);
		Debug.Log ("crate state is now " + m_currentState);

		GameObject a_crate = this.gameObject;

		Debug.Log ("crate owner is now " + m_owner);
		Debug.Log ("crate state is now " + m_currentState);

		//respawn

		this.m_owner = null;
		this.m_currentState = State.Ready;
		crateCrash.Instance.Respawn (this);

		// do effects
		particle.playParticle (particleIndex, transform.position, audioIndex);

		//disable the crate
		a_crate.SetActive (false);
	}

	public List<byte> GetBytes()
	{
		if (controller == null)
		{
			controller = GetComponent <Rigidbody> ();
		}

		isActive = gameObject.activeSelf;

		//Debug.Log ("this crate is" + isActive);
		m_myByteList.Clear ();

		//1 byte message version
		//1 byte type of message
		//24 bytes for 6 floats
		//26 bytes
		m_myByteList.Add (m_messageVersion);
		m_myByteList.AddRange (System.BitConverter.GetBytes (isActive));
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.position.x));
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.position.y));
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.position.z));
		m_myByteList.AddRange (System.BitConverter.GetBytes (controller.velocity.x));
		m_myByteList.AddRange (System.BitConverter.GetBytes (controller.velocity.z));
		m_myByteList.AddRange (System.BitConverter.GetBytes (transform.rotation.y));

		//MultiplayerManager.Instance.ShowMPStatus ("converted my bytes");
		//MultiplayerManager.Instance.ShowMPStatus (controller.velocity.x.ToString ());
		return m_myByteList;
	}

	//readBytes(byte[])
	//change byte array into pos, rot, etc
	public void ReadBytes (byte[] a_byteMessage, int a_startPoint)
	{
		if (controller == null)
		{
			controller = GetComponent <Rigidbody> ();
		}

		//read through byte[] grab correct parts to udpates myself
		byte messageVersion = (byte)a_byteMessage[0];

		bool crateActive = (System.BitConverter.ToBoolean (a_byteMessage, a_startPoint+1));
		a_startPoint += 2;

		if (m_lastState && ! crateActive) 
		{
			//change of state from last read bytes
			//play the particle system
			Debug.Log("crate crashing behaviour");
			CrashingBehaviour();
		}

		if (crateActive) 
		{
			//Debug.Log ("crate being activated");
			this.gameObject.SetActive (true);
			float xPos = System.BitConverter.ToSingle (a_byteMessage, a_startPoint);
			float yPos = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 4);
			float zPos = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 8);
			float xVel = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 12);
			float zVel = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 16);
			float yRot = System.BitConverter.ToSingle (a_byteMessage, a_startPoint + 20);

			m_startPos = transform.position;
			m_startRot = transform.rotation;
			m_destinationPos = new Vector3 (xPos, yPos, zPos);
			m_destinationRot = Quaternion.Euler (0, yRot, 0);
		} 


		else 
		{
			//Debug.Log ("crate being deactivated");
			this.gameObject.SetActive (false);
		}
		m_lastState = crateActive;
	}
}
