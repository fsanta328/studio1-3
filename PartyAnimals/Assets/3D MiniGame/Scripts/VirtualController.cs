﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class VirtualController : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
	private Image bgImg, joystickImg;
	private Vector3 inputVector;

	private void Start()
	{ 	
		bgImg = GetComponent<Image> ();
		joystickImg = transform.GetChild (0).GetComponent<Image> ();
	}

	public virtual void OnDrag (PointerEventData a_ped)
	{
		Vector2 pos;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle (bgImg.rectTransform, a_ped.position, a_ped.pressEventCamera, out pos)) 
		{
			pos.x = (pos.x / bgImg.rectTransform.sizeDelta.x);
			pos.y = (pos.y / bgImg.rectTransform.sizeDelta.y);

			inputVector = new Vector3 (pos.x * 2 - 1, 0, pos.y * 2 - 1);
			inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;


//			if (inputVector.magnitude > 1.0f) 
//			{
//				inputVector = inputVector.normalized;
//			}
//			else
//			{
//				inputVector = inputVector;
//			}





			joystickImg.rectTransform.anchoredPosition = new Vector3(inputVector.x * (bgImg.rectTransform.sizeDelta.x/3),inputVector.z * (bgImg.rectTransform.sizeDelta.y/3));
		}
	}
	
	public virtual void OnPointerDown (PointerEventData a_ped)
	{
		OnDrag (a_ped);
	}

	public virtual void OnPointerUp (PointerEventData a_ped)
	{
		inputVector = Vector3.zero;
		joystickImg.rectTransform.anchoredPosition = Vector3.zero;
	}

	public Vector3 GetPosition()
	{
		return inputVector;
	}
}
