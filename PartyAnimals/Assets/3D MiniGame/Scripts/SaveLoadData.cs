﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SaveLoadData : MonoBehaviour {

	int timesPlayed;
	public Text timePlayed;
	public Text powerUpsUsed;

	void Awake()
	{
		// dont destroy this script so it keeps counting how many times u used powerup's 
		DontDestroyOnLoad(this.gameObject);
		timePlayed.GetComponent<Text>();
		powerUpsUsed.GetComponent<Text>();
	}

	void Start()
	{
		timePlayed.text = "" + timesPlayed;
		powerUpsUsed.text = "" + PowerUpManager.powerUpUsed;
		Debug.Log(PowerUpManager.powerUpUsed);
	}


	public void Save()
	{
		// Save variable that you need
		PlayerPrefs.SetInt("timesPlayed", timesPlayed);
		PlayerPrefs.SetInt("timespended", PowerUpManager.powerUpUsed);

	}

	//Load
	public void Load()
	{
		// Get the variable that you saved eariler (Load it)
		timesPlayed = PlayerPrefs.GetInt("timesPlayed");
		PowerUpManager.powerUpUsed = PlayerPrefs.GetInt("timespended");

	}

	//Load when Opening
	public void OnEnable()
	{
		Load();
	}

	//Save on Exit
	public void OnDisable()
	{
		Debug.Log("Existing!");
		timesPlayed++; //Increment how many times opened
		Save(); //save
	}
}
