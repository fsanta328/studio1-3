﻿using UnityEngine;
using System.Collections;

public class OnSelection : MonoBehaviour 
{
	public Material[] m_shaders;
	private float m_originalTexture = 0, m_grayishTexture = 1;
	private GameObject m_character;

	void Start()
	{
		m_character = this.gameObject;

		//if certain character is not selected, keep the original texture
		ConvertShader (false, m_originalTexture);
	}
	
	// Update is called once per frame
	void Update () 
	{
		//if certain character is selected, turn to grayish texture
		ConvertShader (true, m_grayishTexture);

		//Disable this script
		ScriptDisabler ();
	}

	public void ConvertShader(bool Is_characterSelected, float a_scale)
	{
		//Check: is certain character is in certain animation
		if(m_character.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsTag("Selected").Equals(Is_characterSelected))
		{
			GetMaterial (a_scale);
		}
	}

	public void GetMaterial(float a_scale)
	{
		//get each material of that certain character
		foreach(Material x in m_shaders)
		{
			//this will turn the original material to grayish material, and vice versa
			x.SetFloat("_WaveScale", a_scale);
		}
	}

	public void ScriptDisabler()
	{
		if (m_character.GetComponent<PlayerControls>().enabled == true)
		{
			ConvertShader (false, m_originalTexture);
			m_character.GetComponent<OnSelection> ().enabled = false;
		}
	}
}
