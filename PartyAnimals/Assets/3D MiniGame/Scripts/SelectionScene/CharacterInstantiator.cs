﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class CharacterInstantiator : MonoBehaviour 
{
	public GameObject[] m_characters;
	internal string m_selectedCharacterName;

	void Start()
	{
		m_selectedCharacterName = null;
	}

	// Use this for initialization
	void Update () 
	{	
		GetSelectedCharacter ();
	}

	protected void GetSelectedCharacter()
	{
		if(Is_inGameScene("crateCrash"))
		{
			InstantiateSelectedCharacter ();
		}
	}

	protected bool Is_inGameScene(string a_gameSceneName)
	{
		return SceneManager.GetActiveScene ().name == a_gameSceneName;
	}

	protected void InstantiateSelectedCharacter()
	{
		foreach(GameObject a_character in m_characters)
		{
			if (a_character.name == m_selectedCharacterName) 
			{
				Instantiate (a_character);
				this.gameObject.SetActive (false);
			}
		}
	}
}
