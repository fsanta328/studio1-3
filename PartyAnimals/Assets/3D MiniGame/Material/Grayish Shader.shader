﻿Shader "Grayish Shader" 
{
    Properties 
    {
        _MainTex ("Albedo", 2D) = "white" {}

//        _Color ("Color", Color) = (1,1,1,1)

	    //_Glossiness is a function that clamps it's input between 0 and 1.
        _WaveScale ("Gray Scale", Range (0,1)) = 0.1 // sliders

        //_Saturation ( "Freeze Scale", Range(0,1) ) = 0.0

         _Glossiness ("Smoothness", Range(0,1)) = 0.5

		_Metallic ("Metallic", Range(0,1)) = 0.0
    }

    SubShader 
    {
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

        sampler2D _MainTex;
       // fixed4 _Color;
        fixed4 _Color;
        half _WaveScale;
   //     half _Saturation;
        half _Glossiness;
		half _Metallic;

        struct Input 
        {
            float2 uv_MainTex;
        };

        fixed4 GrayColor()
        {
        	return _Color = (0.4, 0.4, 0.4);
        }

//        fixed4 BlueColor()
//        {
//        	return _Color = (0.0, 0.0, 1.0, 1.0);
//        }

        // Converts color to gray scale
        float GrayishTexture( float3 c )
        {
        	//This will going to return the dot protuct between the shaders
            return dot( c,  GrayColor());
        }

//                // Converts color to blue scale
//        float BluishTexture( float3 c )
//        {
//        	//This will going to return the dot protuct between the shaders
//            return dot( c,  BlueColor());
//        }
//
        void surf (Input In, inout SurfaceOutputStandard o)
        {
//          fixed4 OriginalTexture = tex2D (_MainTex, In.uv_MainTex)* _Color;
//			o.Albedo = OriginalTexture.rgb;

			fixed4 OriginalTexture = tex2D (_MainTex, In.uv_MainTex);

            //This will be able to slide between shaders
            o.Albedo = lerp(OriginalTexture.rgb, GrayishTexture(OriginalTexture.rgb), _WaveScale);
//            o.Albedo = lerp(OriginalTexture.rgb, BluishTexture(OriginalTexture.rgb), _Saturation);

            o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
        }

        ENDCG
    }

    FallBack "Diffuse"
}