﻿using UnityEngine;
using System.Collections;

public class BreakableWall : CharacterClass
{
    void Update()
    {
        if(health <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
