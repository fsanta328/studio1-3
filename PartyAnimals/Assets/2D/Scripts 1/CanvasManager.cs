﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour {
	
	// 0 = You Win Canvas
	// 1 = Death Canvas
	// 2 = Time up Canvas
	public Canvas[] Canvases;

	public Canvas HUDCanvas;

	GameObject player, GameManager;
	int repeat = 1;

	// Use this for initialization
	void Start () 
	{
		player = GameObject.FindGameObjectWithTag ("Player");

		GameManager = GameObject.FindGameObjectWithTag ("Game Manager");

		foreach (Canvas canvas in Canvases) 
		{
			canvas.gameObject.SetActive (false);
		}
	}

	// Update is called once per frame
	void Update () 
	{
		if (player.GetComponent<PlayerBehavior> ().healthViewer <= 0) 
		{
			Canvases [1].gameObject.SetActive (true);
		}
		if (GameManager.GetComponent<HUDManager> ().TimeToEnd <= 0) 
		{
            GameOverManager gom = FindObjectOfType<GameOverManager>();
            gom.gameIsOver = true;
			Canvases [2].gameObject.SetActive (true);
			if (repeat > 0) 
			{
				AudioManager.IsPlayerDead = true;
				repeat--;
			}
		}
		if (player.GetComponent<PlayerBehavior> ().HasPowerup) 
		{
			Canvases [0].gameObject.SetActive (true);
		}
	}
}
