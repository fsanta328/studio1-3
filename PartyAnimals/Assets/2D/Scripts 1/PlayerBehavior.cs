﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerBehavior : CharacterClass {

    public string playerName;
    public GameObject bulletManager;
	public float TrapDurration;
    public Vector3 velocity;
    public float maxCoolDown;
    bool IsTrapped, hasCooledDown;
	float TrapTimer, cooldown;
	GameObject[] BlueDoors, RedDoors, YellowDoors, WhiteDoors;
	Vector3 StartPosition;
    bool isIdle = true;
    public bool attack = false, IsStarting, HasBlueKey, HasRedKey, HasYellowKey, HasWhiteKey, HasPowerup, hasFinished;
    bool dead = false;
    Animator anim;
    public float healthViewer;
    public JoystickControl moveStick, attackStick;
    GameObject GameManager;
    Color EnabledColor;
	int repeat = 1;
    public Vector3 dir, dirF;
    // Use this for initialization
    void Awake () 
	{
		HasPowerup = false;
        EnabledColor = new Color(1, 1, 1, 1);
        GameManager = GameObject.FindGameObjectWithTag("Game Manager");
        moveStick = GameObject.Find("MoveJoystick").GetComponent<JoystickControl>();
        attackStick = GameObject.Find("AttackJoystick").GetComponent<JoystickControl>();
        ResetHealth(maxHealth);
        anim = GetComponentInChildren<Animator>();
        velocity = Vector3.zero;
		StartPosition = transform.position;
        bulletManager = GameObject.Find("PlayerPoolManager");
		IsTrapped = false;
		HasBlueKey = false;
		HasRedKey = false;
		HasYellowKey = false;
		HasWhiteKey = false;
		healthViewer = health / maxHealth;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
        if(!hasFinished)
            MoveCharacter();
    }
	void Update()
	{
        dir = moveStick.inputDirection;
        dirF = attackStick.inputDirection;
        if (TrapTimer > 0) 
		{
			IsTrapped = true;
			TrapTimer -= 1 * Time.deltaTime;
		}
		else 
		{
			IsTrapped = false;
		}

        if (velocity != Vector3.zero)
            isIdle = false;
        else
            isIdle = true;
        if (!dead)
            fireWhenReady();

        AnimateCharacter();
		if (health <= 0) 
		{
			if (repeat > 0)
			{
				AudioManager.IsPlayerDead = true;
				repeat--;
			}
			dead = true;
		}

		healthViewer = health / maxHealth;
    }
    // Moves the character
    void MoveCharacter()
    {
        if(!dead && !attack && !IsTrapped && !IsStarting)
        {
            velocity = dir;
            velocity *= speed;
            Vector3 translation = velocity * Time.deltaTime;

            transform.Translate(translation, Space.World);
        }
    }
    // Sets the various parameters for firing
    void AnimateCharacter()
    {
        if (!isIdle)
        {
            anim.SetBool("isIdle", false);

			if (dir.x >= 0.5f) // Right
                gameObject.transform.localEulerAngles = new Vector3(0, 0, 90);
			if (dir.y > 0.5f) // Up
                gameObject.transform.localEulerAngles = new Vector3(0, 0, 180);
			if (dir.x <= -0.5f) // Left
                gameObject.transform.localEulerAngles = new Vector3(0, 0, 270);
			if (dir.y < -0.5f) // Down
                gameObject.transform.localEulerAngles = new Vector3(0, 0, 0);
        }
        else
        {
            anim.SetBool("isIdle", true);
        }

        if (attack)
        {
            anim.SetTrigger("Attack");
        }
        if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            if (!isIdle)
                anim.SetBool("isIdle", false);
            else
                anim.SetBool("isIdle", true);

            attack = false;
        }

        if (dead)
        {
            anim.SetTrigger("Die");
        }
    }
    
    void OnTriggerEnter(Collider Col)
	{
		if (Col.gameObject.tag == "Trap") 
		{
			transform.position = Col.transform.position;
			AudioManager.TrapIsTriggered = true;
			TrapTimer = TrapDurration;
			Destroy (Col.gameObject);
		}
		else if (Col.gameObject.tag == "Blue Key") 
		{
			Destroy (Col.gameObject);
			AudioManager.KeyIsCollected = true;
			HasBlueKey = true;
			GameManager.GetComponent<HUDManager> ().KeyImages [0].color = EnabledColor;
		}
		else if (Col.gameObject.tag == "Red Key") 
		{
			Destroy (Col.gameObject);
			AudioManager.KeyIsCollected = true;
			HasRedKey = true;
			GameManager.GetComponent<HUDManager> ().KeyImages [1].color = EnabledColor;
		}
		else if (Col.gameObject.tag == "Yellow Key") 
		{
			Destroy (Col.gameObject);
			AudioManager.KeyIsCollected = true;
			HasYellowKey = true;
			GameManager.GetComponent<HUDManager> ().KeyImages [2].color = EnabledColor;
		}
		else if (Col.gameObject.tag == "White Key") 
		{
			Destroy (Col.gameObject);
			AudioManager.KeyIsCollected = true;
			HasWhiteKey = true;
			GameManager.GetComponent<HUDManager> ().KeyImages [3].color = EnabledColor;
		}
		else if (Col.gameObject.tag == "Teleporter 1")
		{
			AudioManager.TeleporterIsTriggered = true;
			transform.position = GameObject.FindGameObjectWithTag ("Location 1").transform.position;
		}
		else if (Col.gameObject.tag == "Teleporter 2")
		{
			AudioManager.TeleporterIsTriggered = true;
			transform.position = GameObject.FindGameObjectWithTag ("Location 2").transform.position;
		}
		else if (Col.gameObject.tag == "Beginning Trap")
		{
			AudioManager.BeginningTrapIsTriggered = true;
			transform.position = StartPosition;
		}
		else if (Col.gameObject.tag == "Speed Trap")
		{
			speed = speed - (speed / 4);
		}

		if (Col.gameObject.tag == "Powerup") 
		{
			Destroy (Col.gameObject);
			HasPowerup = true;
			//add code to set power up to be used in 3d world
			//int randomPower = Random.Range(0,4);
			int randomPower = 1;
			PlayerControls.m_powerUpNum = randomPower;
			AudioManager.PowerupIsCollected = true;
			AudioManager.PlayerWon = true;
		}
	}

	void OnCollisionEnter(Collision Col)
	{
		if (Col.gameObject.tag == "Blue Door" && HasBlueKey) 
		{
			Destroy (Col.gameObject);
		}
		else if (Col.gameObject.tag == "Red Door" && HasRedKey) 
		{
			Destroy (Col.gameObject);
		}
		else if (Col.gameObject.tag == "Yellow Door" && HasYellowKey) 
		{
			Destroy (Col.gameObject);
		}
		else if (Col.gameObject.tag == "White Door" && HasWhiteKey) 
		{
			Destroy (Col.gameObject);
		}
	}

    // handles the firing
    void fireWhenReady()
    {
        if (!IsStarting)
        {
            if (cooldown > 0)
                cooldown -= Time.deltaTime;
            if (cooldown <= 0)
                hasCooledDown = true;
            if (hasCooledDown)
            {
                if (dirF.x <= -0.5f || Input.GetKey(KeyCode.A))
                {
                    GameObject poolObj = getBullet();
                    gameObject.transform.localEulerAngles = new Vector3(0, 0, 270);
                    poolObj.GetComponent<BulletScript>().SetVelocity(Vector3.left);
					AudioManager.BulletIsShot = true;
                    ResetCoolDown();
                }
                else if (dirF.y > 0.5f || Input.GetKey(KeyCode.W))
                {
                    GameObject poolObj = getBullet();
                    gameObject.transform.localEulerAngles = new Vector3(0, 0, 180);
                    poolObj.GetComponent<BulletScript>().SetVelocity(Vector3.up);
					AudioManager.BulletIsShot = true;
                    ResetCoolDown();
                }
                else if (dirF.y < -0.5f || Input.GetKey(KeyCode.S))
                {
                    GameObject poolObj = getBullet();
                    gameObject.transform.localEulerAngles = new Vector3(0, 0, 0);
                    poolObj.GetComponent<BulletScript>().SetVelocity(Vector3.down);
					AudioManager.BulletIsShot = true;
                    ResetCoolDown();
                }
                else if (dirF.x >= 0.5f || Input.GetKey(KeyCode.D))
                {
                    GameObject poolObj = getBullet();
                    gameObject.transform.localEulerAngles = new Vector3(0, 0, 90);
                    poolObj.GetComponent<BulletScript>().SetVelocity(Vector3.right);
					AudioManager.BulletIsShot = true;
                    ResetCoolDown();
                }
            }
        }
    }

    // Gets a bullet from the bullet manager
    GameObject getBullet()
    {
        GameObject poolObj = bulletManager.GetComponent<PoolManager>().GetObjectFromPool(this.gameObject, damage);
        anim.SetTrigger("Attack");
        poolObj.transform.position = gameObject.transform.position;
        return poolObj;
    }

    void ResetCoolDown()
    {
        cooldown = maxCoolDown;
        hasCooledDown = false;
    }
}