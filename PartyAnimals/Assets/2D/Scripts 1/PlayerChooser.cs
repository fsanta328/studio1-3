﻿using UnityEngine;
using System.Collections.Generic;

public static class PlayerChooser
{
    public static GameObject SetPlayer(string playerName, List<GameObject> playerList)
    {
        GameObject player = null;
        if (playerList.Count != 0)
        {
            foreach (GameObject p in playerList)
            {
                if (p.GetComponent<PlayerBehavior>().playerName == playerName)
                {
                    player = p;
                }
            }

            if (player == null)
                player = playerList[0];

            return player;
        }
        else
            return null;
    }
}
