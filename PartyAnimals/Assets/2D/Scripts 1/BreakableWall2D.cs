﻿using UnityEngine;
using System.Collections;

public class BreakableWall2D : MonoBehaviour {

    public float health;
    public Sprite[] sprites;

    public void hit(float dmg)
    {
        health -= dmg;

        if (health <= 0)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            GetComponentInChildren<SpriteRenderer>().sprite = sprites[(int)health - 1];
        }

    }

}
