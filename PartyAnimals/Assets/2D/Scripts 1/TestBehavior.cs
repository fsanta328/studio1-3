﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class TestBehavior : MonoBehaviour
{
    public GameObject poolHandler;
    [SerializeField]
    GameObject poolObject;
    [SerializeField]
    int poolSize;
    [SerializeField]
    Queue<GameObject> pool;
    public bool hasCooledDown;
    float cooldown;
    public float maxCoolDown;

	// Use this for initialization
	void Start () {
        pool = new Queue<GameObject>();

        for (int x = 0; x < poolSize; x++)
        {
            GameObject poolClone = (GameObject)Instantiate(poolObject, poolHandler.transform.position, Quaternion.identity);
            poolClone.transform.SetParent(poolHandler.transform);
            pool.Enqueue(poolClone);
            poolClone.GetComponent<BulletScript>().poolParent = this.gameObject;
            poolClone.SetActive(false);
        }
	}

    GameObject GetObjectFromPool()
    {
        GameObject poolObj = pool.Dequeue();
        poolObj.SetActive(true);
        poolObj.transform.SetParent(null);

        return poolObj;
    }

    public void ReturnObjectToPool(GameObject poolObj)
    {
        poolObj.transform.SetParent(poolHandler.transform);
        poolObj.transform.position = poolHandler.transform.position;
        pool.Enqueue(poolObj);
        poolHandler.SetActive(false);
    }

    void ResetCoolDown()
    {
        cooldown = maxCoolDown;
        hasCooledDown = false;
    }

	// Update is called once per frame
	void Update ()
    {
        Debug.Log(pool.Count);
        if(cooldown > 0)
            cooldown -= Time.deltaTime;
        if (cooldown <= 0)
            hasCooledDown = true;

        if (hasCooledDown)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                GameObject poolObj = GetObjectFromPool();
                poolObj.transform.position = gameObject.transform.position;
                poolObj.GetComponent<BulletScript>().SetVelocity(Vector3.left);
                ResetCoolDown();
            }
            if (Input.GetKeyDown(KeyCode.W))
            {
                GameObject poolObj = GetObjectFromPool();
                poolObj.transform.position = gameObject.transform.position;
                poolObj.GetComponent<BulletScript>().SetVelocity(Vector3.up);
                ResetCoolDown();
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                GameObject poolObj = GetObjectFromPool();
                poolObj.transform.position = gameObject.transform.position;
                poolObj.GetComponent<BulletScript>().SetVelocity(Vector3.down);
                ResetCoolDown();
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                GameObject poolObj = GetObjectFromPool();
                poolObj.transform.position = gameObject.transform.position;
                poolObj.GetComponent<BulletScript>().SetVelocity(Vector3.right);
                ResetCoolDown();
            }
        }
	}
}
