﻿using UnityEngine;
using System.Collections;

public class TestScript : CharacterClass
{
    public GameObject target;
    public float myHealth;
    public Vector3 directionToEnemy;
    Enemy me;

    Repeat brain;
    Selector selector1, selector2;
    Sequence sequence1, sequence2;
    Rigidbody rb;
    Animator anim;
    bool isIdle, isAttacking;

    void Start()
    {
        ResetHealth(maxHealth);
        rb = GetComponent<Rigidbody>();
        anim = GetComponentInChildren<Animator>();
        target = GameObject.FindGameObjectWithTag("Player");
        me = new Enemy(this.gameObject, speed, range, health, damage, maxHealth);
        Behave();

        me.setNerve(brain);
        me.Init();

    }

    // Update is called once per frame
    void Update()
    {
        myHealth = getHealth();
        if (health > 0)
            me.Tick();
        if (rb.IsSleeping())
            isIdle = true;
        else
            isIdle = false;

        Animate();

        me.healthSet(this.myHealth);
    }

    void Behave()
    {
        selector1 = new Selector();
        selector2 = new Selector();

        sequence1 = new Sequence();
        sequence2 = new Sequence();

        sequence2.addNerve(new IsHealthNotHalf());
        sequence2.addNerve(new MoveTo(target, range));
        sequence2.addNerve(new MeleeAttack(target, damage));

        selector2.addNerve(sequence2);
        selector2.addNerve(new RunAway(target, range, range + (range / 2)));

        sequence1.addNerve(new IsPlayerSeen(range));
        sequence1.addNerve(selector2);

        selector1.addNerve(sequence1);
        selector1.addNerve(new GoToRandomNode());

        brain = new Repeat(selector1);
    }

    void Animate()
    {
        isAttacking = me.attacking;
        if (isIdle)
            anim.SetBool("Moving", false);
        else
            anim.SetBool("Moving", true);

        if (isAttacking)
            anim.SetTrigger("Attack");
        if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            if (!isIdle)
                anim.SetBool("Moving", true);
            else
                anim.SetBool("Moving", false);

            me.attacking = false;
            isAttacking = false;
        }

        if (health <= 0)
        {
            gameObject.GetComponent<Collider>().enabled = false;
            anim.SetTrigger("Die");
            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Die"))
            {
                StartCoroutine(Deactivate());

            }
        }
    }


    IEnumerator Deactivate()
    {
        yield return new WaitForSeconds(5);
        gameObject.SetActive(false);
    }
    //void OnDrawGizmos()
    //{
    //    if (target != null)
    //    {
    //        Gizmos.DrawWireSphere(transform.position, range);
    //        Gizmos.DrawLine(transform.position, target.transform.position);
    //        Gizmos.color = Color.red;
    //        Gizmos.DrawWireSphere(transform.position, 1f);
    //        Gizmos.color = Color.white;
    //    }
    //}
}
