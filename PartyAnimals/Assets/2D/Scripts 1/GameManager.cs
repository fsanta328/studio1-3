﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

public class GameManager : MonoBehaviour {
	
	public TextAsset Map;
    public GameObject board, objectGroup;
	// Objects[0] = Empty Space
	// Objects[1] = Wall
	// Objects[2] = Breakable Wall
	// Objects[3] = Trap
	// Objects[4] = Teleporter 1
	// Objects[5] = AI Space
	// Objects[6] = Teleporter 2
	// Objects[7] = Location 1
	// Objects[8] = Location 2
	public GameObject[] Objects;

    // 0 - 3 Space, 4 - 7 AI, 8 - 11 Wall Variant 1, 12 - 15 Wall Variant 2
    public GameObject[] Tiles;

    // Enemies[0] = Rat
    // Enemies[1] = Slime
    // Enemies[2] = Skeleton
    public GameObject[] Enemies;

	// Powerups[0] = Fire Powerup
	// Powerups[1] = Ice Powerup
	public GameObject[] Powerups;

	// Keys[0] = Blue Key
	// Keys[1] = Red Key
	// Keys[2] = Yellow Key
	// Keys[3] = White Key
	public GameObject[] Keys;

	// Doors[0] = Blue Door
	// Doors[1] = Red Door
	// Doors[2] = Yellow Door
	// Doors[3] = White Door
	public GameObject[] Doors;

	public GameObject Player;
    public string playerName;
	public GameObject Camera;
	private GameObject FoundPlayer;
    public List<GameObject> playerList = new List<GameObject>();

    string ReadEachLine;

	float x , y;

	// Use this for initialization
	void Awake () 
	{
        Player = PlayerChooser.SetPlayer(playerName, playerList);

		using (StringReader STR = new StringReader (Map.text)) 
		{
			while ((ReadEachLine = STR.ReadLine ()) != null) 
			{
				GameObject Current = null;
                bool isTile = true;
                bool isPlayer = false;
                GameObject boardObject = null;
                GameObject emptyTile = null;

                foreach (char c in ReadEachLine) 
				{
					switch (c) 
					{
                        // Spaces
					case '0':
                            isTile = false;
                            Current = Objects[0];
						break;

                        // Walls
					case '1':
						Current = Tiles[Random.Range(8, 11)];
						break;

						//AI Space
					case 'A':
                            isTile = false;
                            Current = Objects[5]; //Tiles [Random.Range(4, 7)];
						break;

						// Breakable Wall
					case '2':
						Current = Objects [2];
                            isTile = false;
						break;

						// Trap
					case 'T':
						Current	= Objects [3];
                            isTile = false;
                            break;

                        // Teleporter 1
					case 'L':
						Current = Objects [4];
                            isTile = false;
                            break;
                        
                        // Player Spawn Point
					case 'P':
						Current = Player;
                            isPlayer = true;
                            isTile = false;
                            break;

                        // Rat
					case 'R':
						Current = Enemies [0];
                            isTile = false;
                            break;
                        
                        // Slime
					case 'S':
						Current = Enemies [1];
                            isTile = false;
                            break;

                       // Slime
					case 'K':
						Current = Enemies [2];
                            isTile = false;
                            break;

                        // Power Up 1
					case 'F':
						Current = Powerups [0];
                            isTile = false;
                            break;

                        // Power Up 2
					case 'I':
						Current = Powerups [1];
                            isTile = false;
                            break;
                        
                        // Blue Key and Door
					case '3':
						Current = Keys [0];
                            isTile = false;
                            break;
					case '4':
						Current = Doors [0];
                            isTile = false;
                            break;

                        // Red Key and Door
					case '5':
						Current = Keys [1];
                            isTile = false;
                            break;
					case '6':
						Current = Doors [1];
                            isTile = false;
                            break;

                        // Yellow Key and Door
					case '7':
						Current = Keys [2];
                            isTile = false;
                            break;
					case '8':
						Current = Doors [2];
                            isTile = false;
                            break;
                        
                        // White Key and Door
					case '9':
						Current = Keys [3];
                            isTile = false;
                            break;
					case 'W':
						Current = Doors [3];
                            isTile = false;
                            break;
                        
                        // Teleporter 2
					case 'Q':
						Current = Objects [6];
                            isTile = false;
                            break;

                        // Location 1
					case 'X':
						Current = Objects [7];
                            isTile = false;
                            break;
                        // Location 2
					case 'Z':
						Current = Objects [8];
                            isTile = false;
                            break;

                        // Trap 1
					case 'J':
						Current = Objects [9];
                            isTile = false;
                            break;
                        // Trap 2
					case 'G':
						Current = Objects [10];
                            isTile = false;
                            break;

					default:
						Debug.Log ("Warning!! Invalid character detected in map file.");
						break;
					}
                    boardObject = (GameObject)Instantiate(Current, new Vector3(x, y, 0), Quaternion.identity);
                    if (boardObject.GetComponent<Node>())
                    {
                        boardObject.GetComponent<Node>().FindNeighbors();
                    }
                    if (!isTile)
                    {
                        emptyTile = (GameObject)Instantiate(Tiles[Random.Range(0, 3)], new Vector3(x, y, 0), Quaternion.identity);
                        emptyTile.transform.SetParent(board.transform);
                    }
                    if (isTile && !isPlayer)
                    {
                        boardObject.transform.SetParent(board.transform);
                    }
                    else if(!isTile && !isPlayer)
                    {
                        boardObject.transform.SetParent(objectGroup.transform);
                    }
                    isPlayer = false;
                    x += Current.transform.lossyScale.x;
				}
				y -= Current.transform.lossyScale.y;
				x = 0;
                isTile = true;
            }
		}
	}
}
