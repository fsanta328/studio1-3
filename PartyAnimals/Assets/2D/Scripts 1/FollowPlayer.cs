﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {

    public float z = -10;
    public bool isSprite, isPlayer;
    public GameObject target;

	// Use this for initialization
	void Start () 
	{
        if(isPlayer)
            target = GameObject.FindGameObjectWithTag("Player");
	}

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            gameObject.transform.position = new Vector3(target.transform.position.x, target.transform.position.y, z);
        }
    }
}
