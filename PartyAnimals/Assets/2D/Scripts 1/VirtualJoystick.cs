﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    private Image bgImage, joystickImage;
    private Vector3 inputVector;
    public bool isMove = false;

    private void Start()
    {
        bgImage = GetComponent<Image>();
        if (!isMove)
        {
            bgImage.rectTransform.anchoredPosition = new Vector3(-Screen.width + bgImage.rectTransform.sizeDelta.x, 0 , 0);
        }
        joystickImage = transform.GetChild(0).GetComponent<Image>();
    }

    public virtual void OnDrag(PointerEventData ped)
    {
        Vector2 pos;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(bgImage.rectTransform, 
            ped.position,
            ped.pressEventCamera, 
            out pos))
        {
            pos.x = (pos.x / bgImage.rectTransform.sizeDelta.x);
            pos.y = (pos.y / bgImage.rectTransform.sizeDelta.y);
            // Normalizes the value of x and y from 0 - 1
            inputVector = new Vector3(pos.x * 2 + 1, 0, pos.y * 2 - 1);
            inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;

            // Moves Joystick image
            joystickImage.rectTransform.anchoredPosition =
                new Vector3(inputVector.x * (bgImage.rectTransform.sizeDelta.x / 3),
                inputVector.z * (bgImage.rectTransform.sizeDelta.y / 3));
        }
    }

    public virtual void OnPointerDown(PointerEventData ped)
    {
        OnDrag(ped);
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        inputVector = Vector3.zero;
        joystickImage.rectTransform.anchoredPosition = Vector3.zero;
    }

    public float Horizontal()
    {
        if (inputVector.x != 0)
            return inputVector.x;
        else if (isMove)
            return Input.GetAxis("Horizontal");
        else
            return 0;
    }

    public float Vertical()
    {
        if (inputVector.x != 0)
            return inputVector.z;
        else if(isMove)
            return Input.GetAxis("Vertical");
        else
            return 0;
    }
}
