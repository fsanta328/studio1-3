﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class PoolManager : MonoBehaviour {

    public int poolSize;
    public string poolObjName;
    public GameObject poolObject;
    public GameObject poolParent;
    Queue<GameObject> pool;

    void Start()
    {
        pool = new Queue<GameObject>();
        CreatePool();
    }

    void CreatePool()
    {
        for (int x = 0; x < poolSize; x++)
        {
            GameObject poolClone = (GameObject)Instantiate(poolObject, poolParent.transform.position, Quaternion.identity);
            poolClone.transform.SetParent(poolParent.transform);
            pool.Enqueue(poolClone);
            poolClone.GetComponent<BulletScript>().poolParent = this.gameObject;
            poolClone.SetActive(false);
        }
    }

    public GameObject GetObjectFromPool(GameObject shoot)
    {
        GameObject poolObj = pool.Dequeue();
        poolObj.GetComponent<BulletScript>().shooter = shoot;
        poolObj.SetActive(true);
        poolObj.transform.SetParent(null);

        return poolObj;
    }

    public GameObject GetObjectFromPool(GameObject shoot, float dmg)
    {
        GameObject poolObj = pool.Dequeue();
        poolObj.GetComponent<BulletScript>().shooter = shoot;
        poolObj.GetComponent<BulletScript>().damage = dmg;
        poolObj.SetActive(true);
        poolObj.GetComponent<BulletScript>().ignoreLayer();
        poolObj.transform.SetParent(null);

        return poolObj;
    }

    public void ReturnObjectToPool(GameObject poolObj)
    {
        if (!pool.Contains(poolObj))
        {
            poolObj.transform.SetParent(poolParent.transform);
            poolObj.transform.position = poolParent.transform.position;
            poolObj.GetComponent<BulletScript>().reset();
            pool.Enqueue(poolObj);
            poolParent.SetActive(false);
        }
    }
    public void setSpeed(float setSpeed)
    {
        foreach (GameObject g in pool)
        {
            g.GetComponent<BulletScript>().speed = setSpeed;
        }
    }
}
