﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DikstrasAlgorithm
{
    public static Stack<GameObject> dijkstra(GameObject[] graph,GameObject source, GameObject target)
    {
        Dictionary<GameObject, float> dist = new Dictionary<GameObject, float>();
        Dictionary<GameObject, GameObject> previous = new Dictionary<GameObject, GameObject>();
        List<GameObject> Q = new List<GameObject>();

        foreach (GameObject v in graph)
        {
            dist[v] = Mathf.Infinity; // Sets value of each object to infinity
            previous[v] = null;

            Q.Add(v); // adds the set of all nodes in the Q
        }

        dist[source] = 0;

        while (Q.Count > 0) // Find the shortest distance and remove it from the Q
        {
            float shortestDistance = Mathf.Infinity;
            GameObject shortestDistanceNode = null;
            foreach (GameObject obj in Q)
            {
                if (dist[obj] < shortestDistance)
                {
                    shortestDistance = dist[obj];
                    shortestDistanceNode = obj;
                }
            }

            GameObject u = shortestDistanceNode;
            Q.Remove(u);

            // Check to see if we made it to the target
            if (u == target)
            {
                Stack<GameObject> S = new Stack<GameObject>();

                while (previous[u] != null)
                {
                    S.Push(u);
                    u = previous[u];
                }
                return S; // Returns the shortest path
            }

            if (dist[u] == Mathf.Infinity)
            {
                break;
            }

            foreach (GameObject v in u.GetComponent<Node>().neighbors) // Gets the neighbors
            {
                float alt = dist[u] + (u.transform.position - v.transform.position).magnitude;

                if (alt < dist[v])
                {
                    dist[v] = alt;
                    previous[v] = u;
                }
            }
        }

        return null;

    }
    /*
      function Dijkstra(Graph, source):

          create vertex set Q

          for each vertex v in Graph:             // Initialization
              dist[v] ← INFINITY                  // Unknown distance from source to v
              prev[v] ← UNDEFINED                 // Previous node in optimal path from source
              add v to Q                          // All nodes initially in Q (unvisited nodes)

          dist[source] ← 0                        // Distance from source to source

          while Q is not empty:
              u ← vertex in Q with min dist[u]    // Source node will be selected first
              remove u from Q 

              for each neighbor v of u:           // where v is still in Q.
                  alt ← dist[u] + length(u, v)
                  if alt < dist[v]:               // A shorter path to v has been found
                      dist[v] ← alt 
                      prev[v] ← u 

          return dist[], prev[]

1  S ← empty sequence
2  u ← target
3  while prev[u] is defined:                  // Construct the shortest path with a stack S
4      insert u at the beginning of S         // Push the vertex onto the stack
5      u ← prev[u]                            // Traverse from target to source
6  insert u at the beginning of S             // Push the source onto the stack
    */
}
