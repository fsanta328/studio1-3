﻿using UnityEngine;
using System.Collections;

public class MeleeAttack : Nerve
{

    GameObject target;
    float myDmg;

    // Sets the default target to move to
    public MeleeAttack(GameObject targetObject, float damage)
    {
        target = targetObject;
        myDmg = damage;
    }

    public override void reset()
    {
        start();
    }

    public override void act(Enemy enemy)
    {
        // Checkes wethere this nerve is running
        if (isRunning())
        {
            if (enemy.getHealth() < 0)
            {
                fail();
                return;
            }
            if (atDestination(enemy))
            {
                Attack(enemy);
            }
        }
    }
    private bool atDestination(Enemy enemy)
    {
        float distance = Vector3.Distance(enemy.myObject.transform.position, target.transform.position);
        if (distance < 1.2f)
            return true;
        else
            return false;

    }
    // TODO: Attack towards and make a cone using something
    void Attack(Enemy enemy)
    {
        if (target.gameObject.GetComponent<CharacterClass>().getHealth() > 0)
        {
            enemy.attacking = true;
            target.gameObject.GetComponent<PlayerBehavior>().healthChange(-myDmg);
        }
        succeed();
    }
}
