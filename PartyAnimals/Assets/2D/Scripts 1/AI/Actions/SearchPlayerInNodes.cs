﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SearchPlayerInNodes : Nerve
{
    GameObject target;
    bool atDestination;
    // Sets the default target to move to
    public SearchPlayerInNodes(GameObject targetObject)
    {
        target = targetObject;
    }

    public override void reset()
    {
        start();
    }

    public override void act(Enemy enemy)
    {
        // Checkes wethere this nerve is running
        if (isRunning())
        {
            if (enemy.getHealth() < 0)
            {
                fail();
                return;
            }

            if (enemy.myObject.GetComponent<CurrentNode>().currentNode == target.gameObject.GetComponent<CurrentNode>().currentNode)
            {
                enemy.myObject.GetComponent<CurrentNode>().currentNode = null;
                atDestination = true;
            }
            else
                atDestination = false;

            if (!atDestination) // Moves the enemy
            {
                move(enemy);
            }
        }
    }

    private void move(Enemy enemy)
    {
        Stack<GameObject> path = DikstrasAlgorithm.dijkstra(
            GameObject.FindGameObjectsWithTag("Node"),
            enemy.myObject.GetComponent<CurrentNode>().currentNode,
            target.GetComponent<CurrentNode>().currentNode);

        GameObject goal = path.Pop();
        enemy.myObject.transform.position = Vector3.MoveTowards(enemy.myObject.transform.position, goal.transform.position, enemy.speed * Time.deltaTime);

        if (atDestination)
            succeed();
    }
}
