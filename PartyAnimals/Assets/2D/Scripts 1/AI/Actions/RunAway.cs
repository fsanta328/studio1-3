﻿using UnityEngine;
using System.Collections;

public class RunAway : Nerve
{

    GameObject target;
    float range = 5;
    int layerMask = 1 << 8;
    float isNearRange = 1f;

    public RunAway(GameObject targetObject, float range, float nearFactor)
    {
        target = targetObject;
        this.range = range;
        isNearRange = nearFactor;
    }
    public override void reset()
    {
        start();
    }

    public override void act(Enemy enemy)
    {
        // Checkes wethere this nerve is running
        if (isRunning())
        {
            if (enemy.getHealth() < 0)
            {
                fail();
                return;
            }
            if (!atDestination(enemy)) // Moves the enemy
            {
                Flee(enemy, isNearRange);
            }
            else
            {
                succeed();
            }
        }
    }

    private void moveEnemy(Enemy enemy)
    {
        float distance = Vector3.Distance(target.transform.position, enemy.myObject.transform.position);
        if (distance > range)
        {
            enemy.myObject.gameObject.GetComponent<CurrentNode>().SetNode();
            fail();
        }

        Vector3 vectorToTarget = target.transform.position - enemy.myObject.transform.position;
        
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(-angle, Vector3.forward);

        enemy.myObject.transform.rotation = Quaternion.Slerp(target.transform.rotation, q, 360);

        enemy.myObject.transform.position = Vector3.MoveTowards(enemy.myObject.transform.position, -vectorToTarget, enemy.speed * Time.deltaTime);

        if (atDestination(enemy))
        {
            enemy.myObject.gameObject.GetComponent<CurrentNode>().SetNode();
            succeed();
        }
    }

    // Checks wether the object is at the target
    private bool atDestination(Enemy enemy)
    {
        float distance = Vector3.Distance(enemy.myObject.transform.position, target.transform.position);

        if (distance >= isNearRange)
            return true;
        else
            return false;

    }

    private bool isSeen(Enemy enemy)
    {
        layerMask = ~layerMask;
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        RaycastHit hit;
        //Physics.SphereCast(enemy.myObject.transform.position, sphereRadius, player.transform.position - enemy.myObject.transform.position, out hit);
        Physics.Linecast(enemy.myObject.transform.position, player.transform.position, out hit, layerMask);

        float distance = Vector3.Distance(player.transform.position, enemy.myObject.transform.position);

        if (hit.collider != null && hit.collider.tag == "Player" && distance < range)
        {
            //enemy.myObject.GetComponent<CurrentNode>().currentNode = null;
            return true;
        }
        else
        {
            return false;
        }
    }

    void Flee(Enemy enemy, float distanceThreshold)
    {
        Vector3 ToTarget = enemy.myObject.transform.position - target.transform.position;

        if (Vector3.Distance(enemy.myObject.transform.position, ToTarget) < distanceThreshold)
        {
            ToTarget.Normalize();
            ToTarget = ToTarget * enemy.speed;
            enemy.myObject.transform.position += ToTarget * Time.deltaTime;
            float angle = Mathf.Atan2(ToTarget.y, ToTarget.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            enemy.myObject.transform.rotation = q;
        }
        else
            succeed();
    }
}
