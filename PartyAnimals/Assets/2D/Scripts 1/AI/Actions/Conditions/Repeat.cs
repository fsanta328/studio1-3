﻿using UnityEngine;
using System.Collections;
using System;

// Start node that repeats after each traversal
public class Repeat : Nerve {

    private Nerve nerve;
    private int times;
    private int originalTimes;

    public Repeat(Nerve nerve)
    {
        this.nerve = nerve;
        this.times = -1; // Infinite times
        this.originalTimes = times;
    }

    public Repeat(Nerve nerve, int times)
    {
        if (times < 1)
        {
            return;
        }

        this.nerve = nerve;
        this.times = times;
        this.originalTimes = times;
    }

    public override void start()
    {
        this.state = NerveState.RUNNING;
        this.nerve.start();
    }

    public override void reset()
    {
        // reset counters
        this.times = originalTimes;
    }

    public override void act(Enemy enemy)
    {
        // Checks if the nerve fails
        if (nerve.isFailure())
        {
            fail();
        }
        else if (nerve.isSuccess())
        {

            if (times == 0) // Finish the whole traversal
            {
                succeed();
                return;
            }
            if (times > 0 || times <= -1) // Restarts the traversal
            {
                times--;
                nerve.reset();
                nerve.start();
            }

        }
        if (nerve.isRunning()) // Starts this nerve
        {
            nerve.act(enemy);
        }

    }
}
