﻿using UnityEngine;
using System.Collections;
using System;

public class IsHealthNotHalf : Nerve
{

    public override void reset()
    {
        start();
    }

    public override void act(Enemy enemy)
    {
        if (enemy.getHealth() <= enemy.maxHealth / 2)
        {
            fail();
        }
        else if (enemy.getHealth() > enemy.maxHealth / 2)
        {

            succeed();
        }
        else
            fail();
    }

    private bool isHealthOk(Enemy enemy)
    {
        if (enemy.getHealth() <= enemy.getHealth() / 2)
        {
            return false;
        }
        else
        { 
            return true;
        }
    }
}
