﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System;

public class Selector : Nerve
{
    public override bool isSpecial
    {
        get { return true; }
        set { }
    }
    public Selector()
    {
        this.currentNerve = null;
    }

    private Nerve currentNerve;
    List<Nerve> nerves = new List<Nerve>();
    Queue<Nerve> nerveQueue = new Queue<Nerve>();

    public void addNerve(Nerve nerve)
    {
        nerves.Add(nerve);
    }

    public override void reset()
    {
        foreach (Nerve nerve in nerves)
        {
            nerve.reset();
        }
    }
    public override void start()
    {
        this.state = NerveState.RUNNING;
        nerveQueue.Clear();

        // Moves the list to the queue and resets the nerve from setup
        foreach (Nerve nerve in nerves)
        {
            nerveQueue.Enqueue(nerve);
            if (nerve.isSpecial)
            {
                nerve.start();
            }
        }
        currentNerve = nerveQueue.Dequeue();
        if(!currentNerve.isRunning())
            currentNerve.start();
    }

    public override void act(Enemy enemy)
    {
        currentNerve.act(enemy);

        // Carries on if still running
        if (currentNerve.isRunning())
        {
            return;
        }

        if (currentNerve != null && nerveQueue.Count != 0)
        {
            if (currentNerve.isFailure())
            {
                currentNerve = nerveQueue.Dequeue();
            }
            else if (currentNerve.isSuccess())
            {
                succeed();
            }
        }

        if (currentNerve.isFailure() && nerveQueue.Count == 0)
        {
            fail();
        }
        if (currentNerve.isSuccess() && nerveQueue.Count == 0)
        {
            succeed();
        }

        //// Checks if the nerve is successful and finish the sequence
        //if (currentNerve.isSuccess() && nerveQueue.Count == 0)
        //{
        //    succeed();
        //    //return;
        //}
        //// Checks if the nerve is successful and finish the sequence
        //if (currentNerve.isFailure() && nerveQueue.Count == 0)
        //{
        //    fail();
        //    //return;
        //}

        //// Moves the nerves forward
        //if (nerveQueue.Count == 0)
        //{
        //    this.state = currentNerve.getState();
        //}
        //else
        //{
        //    currentNerve = nerveQueue.Dequeue();
        //    currentNerve.start();
        //}

    }
}

