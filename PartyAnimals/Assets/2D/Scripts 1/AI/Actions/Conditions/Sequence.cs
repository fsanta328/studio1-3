﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

public class Sequence : Nerve
{
    public override bool isSpecial
    {
        get { return true; }
        set { }
    }
    public Sequence()
    {
        this.currentNerve = null;
    }

    private Nerve currentNerve;
    List<Nerve> nerves = new List<Nerve>();
    Queue<Nerve> nerveQueue = new Queue<Nerve>();

    public void addNerve(Nerve nerve)
    {
        nerves.Add(nerve);
    }

    public override void reset()
    {
        foreach (Nerve nerve in nerves)
        {
            nerve.reset();
        }
    }

    public override void start()
    {
        // starts the current sequence
        this.state = NerveState.RUNNING;
        
        // reset the current queue
        nerveQueue.Clear();
        foreach (Nerve nerve in nerves)
        {
            nerveQueue.Enqueue(nerve);

            // To Queue all nerves in special nerves before the game starts
            if (nerve.isSpecial)
            {
                nerve.start();
            }
        }
        currentNerve = nerveQueue.Dequeue();
        if (!currentNerve.isRunning())
            currentNerve.start();
    }

    public override void act(Enemy enemy)
    {
        currentNerve.act(enemy);
        // if is still running then it will carry on
        if (currentNerve.isRunning())
        {
            return;
        }

        if (currentNerve != null && nerveQueue.Count != 0)
        {
            if (currentNerve.isFailure())
            {
                fail();
            }
            else if (currentNerve.isSuccess())
            {
                currentNerve = nerveQueue.Dequeue();
            }
            else
                return;
        }

        if (currentNerve.isFailure() && nerveQueue.Count == 0)
        {
            fail();
        }
        if(currentNerve.isSuccess() && nerveQueue.Count == 0)
        {
            succeed();
        }
    }
}
