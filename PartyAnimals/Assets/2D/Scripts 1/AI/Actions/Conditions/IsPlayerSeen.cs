﻿using UnityEngine;
using System.Collections;
using System;

public class IsPlayerSeen : Nerve
{
    private float sphereRadius = 5f;
    int layerMask = 1 << 8;

    public IsPlayerSeen(float radius)
    {
        sphereRadius = radius;
    }

    public override void reset()
    {
        start();
    }

    public override void act(Enemy enemy)
    {
        if (isSeen(enemy))
        {
            succeed();
        }
        else
            fail();
    }
     
    private bool isSeen(Enemy enemy)
    {
        layerMask = ~layerMask;
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        RaycastHit hit;
        //Physics.SphereCast(enemy.myObject.transform.position, sphereRadius, player.transform.position - enemy.myObject.transform.position, out hit);
        Physics.Linecast(enemy.myObject.transform.position, player.transform.position, out hit, layerMask);

        float distance = Vector3.Distance(player.transform.position, enemy.myObject.transform.position);

        if (hit.collider != null && hit.collider.tag == "Player" && distance < sphereRadius && hit.collider.gameObject.GetComponent<CharacterClass>().getHealth() > 0)
        {
            //enemy.myObject.GetComponent<CurrentNode>().currentNode = null;
            return true;
        }
        else
        {
            return false;
        }
    }
}
