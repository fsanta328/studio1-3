﻿using UnityEngine;
using System.Collections;

public class FireAt : Nerve
{

    GameObject target;
    GameObject bulletPool;
    float range;
    float myDmg, projectileSpeed;

    // Sets the default target to move to
    public FireAt(GameObject targetObject, GameObject bPool, float dmg, float range, float projSpeed)
    {
        target = targetObject;
        bulletPool = bPool;
        myDmg = dmg;
        this.range = range;
        projectileSpeed = projSpeed;
    }

    public override void reset()
    {
        start();
    }

    public override void act(Enemy enemy)
    {
        // Checkes wethere this nerve is running
        if (isRunning())
        {
            if (enemy.getHealth() < 0)
            {
                fail();
                return;
            }
            if(stillAtRange(enemy))
                fireAtEnemy(enemy);
        }
    }

    private void fireAtEnemy(Enemy enemy)
    {
        enemy.attacking = true;
        Vector3 relativePos = target.transform.position - enemy.myObject.transform.position;
        Quaternion rot = Quaternion.LookRotation(relativePos);
        enemy.myObject.transform.rotation = rot;

        bulletPool.GetComponent<PoolManager>().setSpeed(projectileSpeed);
        if (target.gameObject.GetComponent<CharacterClass>().getHealth() > 0)
        {
            GameObject poolObj = bulletPool.GetComponent<PoolManager>().GetObjectFromPool(enemy.myObject, myDmg);
            poolObj.transform.position = enemy.myObject.transform.position;
            poolObj.GetComponent<BulletScript>().SetVelocity(relativePos);
        }

        succeed();
    }
    private bool stillAtRange(Enemy enemy)
    {
        float distance = Vector3.Distance(enemy.myObject.transform.position, target.transform.position);

        if (distance < range) 
            return true;
        else
            return false;

    }
}
