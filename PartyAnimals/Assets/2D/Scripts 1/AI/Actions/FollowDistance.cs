﻿using UnityEngine;
using System.Collections;

public class FollowDistance : Nerve
{

    GameObject target;
    float range;

    // Sets the default target to move to
    public FollowDistance(GameObject targetObject, float myRange)
    {
        target = targetObject;
        range = myRange;
    }

    public override void reset()
    {
        start();
    }

    public override void act(Enemy enemy)
    {
        // Checkes wethere this nerve is running
        if (isRunning())
        {
            if (enemy.getHealth() < 0)
            {
                fail();
                return;
            }
            if (!atDestination(enemy)) // Moves the enemy
            {
                followTarget(enemy);
            }
        }
    }

    private void followTarget(Enemy enemy)
    {
        enemy.myObject.transform.position = Vector3.MoveTowards(enemy.myObject.transform.position, target.transform.position, enemy.speed * Time.deltaTime);

        if (atDestination(enemy))
            succeed();

        float distance = Vector3.Distance(enemy.myObject.transform.position, target.transform.position);
        if (distance > range)
            fail();
    }

    // Checks wether the object is at the target
    private bool atDestination(Enemy enemy)
    {
        float distance = Vector3.Distance(enemy.myObject.transform.position, target.transform.position);
        if (distance < range/2)
            return true;
        else
            return false;

    }
}
