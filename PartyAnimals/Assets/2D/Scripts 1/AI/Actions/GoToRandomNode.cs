﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GoToRandomNode : Nerve
{
    GameObject target;
    // Sets the default target to move to

    public override void reset()
    {
        start();
    }
    public override void start()
    {
        this.state = NerveState.RUNNING;
    }
    public override void act(Enemy enemy)
    {
        if (target == null)
            getRandomNode(enemy);
        // Checkes wethere this nerve is running
        if (isRunning())
        {
            if (enemy.getHealth() < 0)
            {
                fail();
                return;
            }

            if (!atDestination(enemy)) // Moves the enemy
            {
                move(enemy);
            }
            else
            {
                target = null;
                succeed();
            }
        }
    }

    private void getRandomNode(Enemy enemy)
    {
        //Lists all nodes
        //GameObject[] nodes = GameObject.FindGameObjectsWithTag("Node");
        //target = nodes[Random.Range(0, nodes.Length)];
        Debug.Log("Hello");
        // Lists the neighbors of the current node
        List<GameObject> nodes = enemy.myObject.GetComponent<CurrentNode>().currentNode.gameObject.GetComponent<Node>().neighbors;
        target = nodes[Random.Range(0, nodes.Count)];
        
    }

    private void move(Enemy enemy)
    {

        Stack<GameObject> path = DikstrasAlgorithm.dijkstra(
            GameObject.FindGameObjectsWithTag("Node"),
            enemy.myObject.GetComponent<CurrentNode>().currentNode,
            target);
        if (target == enemy.myObject.GetComponent<CurrentNode>().currentNode)
            succeed();

        GameObject goal = path.Pop();

        Vector3 vectorToTarget = goal.transform.position - enemy.myObject.transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        enemy.myObject.transform.rotation = Quaternion.RotateTowards(goal.transform.rotation, q, 360);

        enemy.myObject.transform.position = Vector3.MoveTowards(enemy.myObject.transform.position, goal.transform.position, enemy.speed * Time.deltaTime);

        if (enemy.myObject.transform.position == goal.transform.position)
        {
            enemy.myObject.gameObject.GetComponent<CurrentNode>().SetNode();
        }
        if (atDestination(enemy))
        {
            succeed();
        }
    }

    private bool atDestination(Enemy enemy)
    {
        if (enemy.myObject.GetComponent<CurrentNode>().currentNode.gameObject == target.gameObject)
        {
            enemy.myObject.GetComponent<CurrentNode>().currentNode = null;
            return true;
        }
        else
            return false;
    }
}
