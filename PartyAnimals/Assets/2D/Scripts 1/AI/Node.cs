﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Node : MonoBehaviour
{
    public List<GameObject> neighbors = new List<GameObject>();

    public float nodeRadius = 50;
    public LayerMask nodeLayerMask;
    public LayerMask collisionLayerMask;

    public GameObject goal;

    void Start()
    {
        FindNeighbors();
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, 0.25f);
        //    Gizmos.DrawWireCube(transform.position, Vector3.one); // Draws a cube on the editor

        //    foreach (GameObject neighbor in neighbors)
        //    {
        //        Gizmos.DrawLine(transform.position, neighbor.transform.position);
        //        Gizmos.DrawWireSphere(neighbor.transform.position, 0.25f);
        //    }

        //    if (goal) // Finds the goal using dijkstras
        //    {
        //        GameObject current = gameObject;
        //        Stack<GameObject> path = DikstrasAlgorithm.dijkstra(GameObject.FindGameObjectsWithTag("Node"), gameObject, goal);

        //        foreach (GameObject obj in path)
        //        {
        //            Gizmos.color = Color.green;
        //            Gizmos.DrawWireSphere(obj.transform.position, 1.0f);

        //            Gizmos.DrawLine(current.transform.position, obj.transform.position);
        //            current = obj;
        //        }
        //    }
    }

[ContextMenu("Connect Node to Neighbors")]
    public void FindNeighbors() 
    {
        neighbors.Clear();

        Collider[] cols = Physics.OverlapSphere(transform.position, nodeRadius, nodeLayerMask );
        foreach (Collider collidedNode in cols) // Finds the nodes that can connect based on an overlapping sphere
        {
            if (collidedNode.gameObject != gameObject)
            {
                RaycastHit hit;
                Physics.Raycast(transform.position, (collidedNode.transform.position - transform.position), out hit, nodeRadius,collisionLayerMask);

                if (hit.transform != null)
                {
                    if (hit.transform.gameObject == collidedNode.gameObject)
                    {
                        neighbors.Add(collidedNode.gameObject);
                    }
                }
            }
        }
    }
}
