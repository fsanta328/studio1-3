﻿using UnityEngine;
using System.Collections;

public class Enemy : CharacterClass 
{
	public string enemyName { get; set; }
	public GameObject myObject { get; set; }
	public Vector3 myTransform { get; set;}
    public Nerve routine;
    public bool attacking = false;
	// Constructors
	public Enemy(GameObject myObject, float speed,float range, float health, float damage, float maxHealth)
	{
        this.maxHealth = maxHealth;
		this.myObject = myObject;
		this.speed = speed;
        this.range = range;
		this.health = health;
		this.damage = damage;
        ResetHealth(maxHealth);
    }

	public void setNerve(Nerve nerveOut)
	{
		this.routine = nerveOut;
	}

	public void Tick()
	{
        if (health > 0)
        {
            myTransform = myObject.transform.position;
            routine.act(this);
        }
	}

	public void Init()
	{
		routine.start ();
	}
}
