﻿using UnityEngine;
using System.Collections;

public class SlimeBehavior : CharacterClass
{

    public GameObject target, fireObject, poolManager;
    public float myHealth, projectileSpeed;
    Enemy me;

    Repeat brain;
    Selector selector1, selector2;
    Sequence sequence1, sequence2, sequence3;
    Animator anim;
    bool isIdle, isAttacking;
	int repeat = 1;

    void Start()
    {
        ResetHealth(maxHealth);
        poolManager = GameObject.Find("SlimePoolManager");
        target = GameObject.FindGameObjectWithTag("Player");
        me = new Enemy(this.gameObject, speed, range, health, damage, maxHealth);
        Behave();
        anim = GetComponentInChildren<Animator>();

        me.setNerve(brain);
        me.Init();

    }
    // Update is called once per frame
    void Update()
    {
        myHealth = getHealth();
        Animate();
        me.healthSet(this.myHealth);
        me.Tick();
    }

    void Behave()
    {
        selector1 = new Selector();
        selector2 = new Selector();

        sequence1 = new Sequence();
        sequence2 = new Sequence();

        sequence2.addNerve(new IsHealthNotHalf());
        sequence2.addNerve(new FireAt(target, poolManager, damage, range, projectileSpeed));

        selector2.addNerve(sequence2);
        selector2.addNerve(new RunAway(target, range, range + (range / 2)));

        sequence1.addNerve(new IsPlayerSeen(range));
        sequence1.addNerve(selector2);

        selector1.addNerve(sequence1);
        selector1.addNerve(new GoToRandomNode());

        brain = new Repeat(selector1);

    }

    void Animate()
    {
        isAttacking = me.attacking;
        if (isIdle)
            anim.SetBool("Moving", false);
        else
            anim.SetBool("Moving", true);

		if (isAttacking) 
		{
			anim.SetTrigger ("Attack");
			AudioManager.SlimeballIsThrown = true;
		}
        if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            if (!isIdle)
                anim.SetBool("Moving", true);
            else
                anim.SetBool("Moving", false);

            me.attacking = false;
            isAttacking = false;
        }

        if (health <= 0)
        {
			if (repeat > 0) 
			{
				AudioManager.SlimeIsDead = true;
				repeat--;
			}
            gameObject.GetComponent<Collider>().enabled = false;
            anim.SetTrigger("Die");
            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Die"))
            {
                StartCoroutine(Deactivate());

            }
        }
    }

    IEnumerator Deactivate()
    {
        yield return new WaitForSeconds(5);
        gameObject.SetActive(false);
    }

}
