﻿using UnityEngine;
using System.Collections;

public abstract class Nerve
{
    public virtual bool isSpecial { get; set; }
	public enum NerveState
	{
		SUCCESS,
		FAILURE,
		RUNNING
	}

	protected NerveState state;

	protected Nerve(){
	}

	public virtual void start()
	{
		this.state = NerveState.RUNNING;
	}

	public abstract void reset ();

	public abstract void act (Enemy enemy);

	protected void succeed()
	{
		this.state = NerveState.SUCCESS;
	}

	public void fail()
	{
		this.state = NerveState.FAILURE;
	}

	public bool isSuccess()
	{
		return state.Equals (NerveState.SUCCESS);
	}

	public bool isFailure()
	{
		return state.Equals (NerveState.FAILURE);
	}

	public bool isRunning()
	{
		return state.Equals (NerveState.RUNNING);
	}

	public NerveState getState()
	{
		return state;
	}
}
