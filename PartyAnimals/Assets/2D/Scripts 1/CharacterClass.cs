﻿using UnityEngine;
using System.Collections;

public class CharacterClass : MonoBehaviour
{
    public float maxHealth;
	protected float health;
    public float damage;
    public float speed;
    public float range;

    public void ResetHealth(float myHealth)
    {
        maxHealth = myHealth;
        health = maxHealth;
    }

    public float getHealth()
    {
        return health;
    }
    public void healthChange(float x)
    {
        health += x;
    }

    public void healthSet(float hp)
    {
        health = hp;
    }
}
