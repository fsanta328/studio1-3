﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour {

    public Vector3 velocity;
    public float damage;
    public float speed = 50;
    public GameObject poolParent;
    public GameObject shooter;
    Rigidbody rig;

    void Start()
    {
        rig = GetComponent<Rigidbody>();
    }

    public void ignoreLayer()
    {
        Physics.IgnoreCollision(this.GetComponent<Collider>(), shooter.GetComponent<Collider>());
    }

    void FixedUpdate()
    {
        rig.AddRelativeForce(velocity * speed);
    }

    public void SetVelocity(Vector3 vel)
    {
        velocity = vel;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.layer != shooter.gameObject.layer)
        {
			AudioManager.BulletIsColliding = true;
            poolParent.GetComponent<PoolManager>().ReturnObjectToPool(this.gameObject);
            if (col.gameObject.GetComponent<CharacterClass>() != null)
            {
                if (col.gameObject.GetComponent<CharacterClass>().getHealth() > 0)
                {
                    col.gameObject.GetComponent<CharacterClass>().healthChange(-damage);
                }
            }
            else if (col.gameObject.GetComponent<BreakableWall2D>() != null)
            {
                col.gameObject.GetComponent<BreakableWall2D>().hit(1);
            }
        }
    }

    public void reset()
    {
        rig.velocity = Vector3.zero;
        rig.angularVelocity = Vector3.zero;
    }
}
