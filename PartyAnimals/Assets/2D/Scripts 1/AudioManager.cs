﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	private AudioSource[] audioSources;

	public AudioClip[] audioClips;

	public static bool BulletIsColliding, BulletIsShot, PowerupIsCollected, RatIsAttacking, RatIsDead, BoneIsThrown, BoneIsColliding, SkeletonIsDead, IsPlayerDead;
	public static bool SlimeballIsThrown, SlimeballIsColliding, SlimeIsDead, KeyIsCollected, BeginningTrapIsTriggered, TeleporterIsTriggered, TrapIsTriggered, PlayerWon;

	public float AudioVolumeLow, AudioVolumeHigh;

	// Use this for initialization
	void Start () 
	{
		audioSources = GetComponents<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (BulletIsColliding) 
		{
			audioSources [1].PlayOneShot (audioClips [0], AudioVolumeHigh);
			BulletIsColliding = false;
		}
		if (BulletIsShot) 
		{
			audioSources [1].PlayOneShot (audioClips [1], AudioVolumeLow);
			BulletIsShot = false;
		}
		if (PowerupIsCollected) 
		{
			audioSources [1].PlayOneShot (audioClips [2], AudioVolumeLow);
			PowerupIsCollected = false;
		}
		if (RatIsAttacking) 
		{
			audioSources [1].PlayOneShot (audioClips [3], AudioVolumeLow);
			RatIsAttacking = false;
		}
		if (RatIsDead) 
		{
			audioSources [1].PlayOneShot (audioClips [4], AudioVolumeHigh);
			RatIsDead = false;
		}
		if (BoneIsThrown) 
		{
			audioSources [1].PlayOneShot (audioClips [5], AudioVolumeHigh);
			BoneIsThrown = false;
		}
		if (BoneIsColliding) 
		{
			audioSources [1].PlayOneShot (audioClips [6], AudioVolumeHigh);
			BoneIsColliding = false;
		}
		if (SkeletonIsDead) 
		{
			audioSources [1].PlayOneShot (audioClips [7], AudioVolumeHigh);
			SkeletonIsDead = false;
		}
		if (SlimeballIsThrown) 
		{
			audioSources [1].PlayOneShot (audioClips [8], AudioVolumeHigh);
			SlimeballIsThrown = false;
		}
		if (SlimeballIsColliding) 
		{
			audioSources [1].PlayOneShot (audioClips [9], AudioVolumeHigh);
			SlimeballIsColliding = false;
		}
		if (SlimeIsDead) 
		{
			audioSources [1].PlayOneShot (audioClips [10], AudioVolumeHigh);
			SlimeIsDead = false;
		}
		if (KeyIsCollected) 
		{
			audioSources [1].PlayOneShot (audioClips [11], AudioVolumeLow);
			KeyIsCollected = false;
		}
		if (TrapIsTriggered) 
		{
			audioSources [1].PlayOneShot (audioClips [12], AudioVolumeHigh);
			TrapIsTriggered = false;
		}
		if (BeginningTrapIsTriggered) 
		{
			audioSources [1].PlayOneShot (audioClips [13], AudioVolumeHigh);
			BeginningTrapIsTriggered = false;
		}
		if (TeleporterIsTriggered) 
		{
			audioSources [1].PlayOneShot (audioClips [14], AudioVolumeLow);
			TeleporterIsTriggered = false;
		}
		if (IsPlayerDead) 
		{
			audioSources [0].clip = null;
			audioSources [0].loop = false;
			audioSources [0].PlayOneShot (audioClips [15], AudioVolumeHigh);
			IsPlayerDead = false;
		}
		if (PlayerWon) 
		{
			audioSources [0].clip = null;
			audioSources [0].loop = false;
			audioSources [0].PlayOneShot (audioClips [16], AudioVolumeHigh);
			PlayerWon = false;
		}
	}
}