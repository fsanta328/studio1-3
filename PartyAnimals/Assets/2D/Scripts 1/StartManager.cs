﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StartManager : MonoBehaviour {

	public GameObject MoveJoyStick, AtkJoyStick;

	public Canvas StartCanvas, HudCanvas;

	public float TimeToStart;

	public bool IsPaused;

	GameObject Player;

	public Text TimeToStartDisplay;

	// Use this for initialization
	void Start () 
	{
		Player = GameObject.FindGameObjectWithTag ("Player");
		IsPaused = true;
		StartCanvas.enabled = true;
        HudCanvas.enabled = false;
		MoveJoyStick.SetActive (false);
		AtkJoyStick.SetActive (false);
		Player.GetComponent<PlayerBehavior> ().IsStarting = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (TimeToStart > 0) 
		{
			TimeToStart -= 1 * Time.deltaTime;
		}
		if (TimeToStart <= 0 && IsPaused)
		{
			TimeToStart = 0;
			IsPaused = false;
            HudCanvas.enabled = true;
			StartCanvas.enabled = false;
			AtkJoyStick.SetActive (true);
			MoveJoyStick.SetActive (true);
			Player.GetComponent<PlayerBehavior> ().IsStarting = false;
		}

		if (TimeToStart > 1) 
		{
			if (TimeToStart >= 3 && TimeToStart < 4) 
			{
				TimeToStartDisplay.color = Color.red;
			}
			else if (TimeToStart >= 2 && TimeToStart < 3) 
			{
				TimeToStartDisplay.color = Color.white;
			}
			else if (TimeToStart >= 1 && TimeToStart < 2) 
			{
				TimeToStartDisplay.color = Color.red;
			}

			TimeToStartDisplay.text = " " + (int)TimeToStart;
		}
		else 
		{
			TimeToStartDisplay.color = Color.white;
			TimeToStartDisplay.text = "GO!";
		}
	}
}
