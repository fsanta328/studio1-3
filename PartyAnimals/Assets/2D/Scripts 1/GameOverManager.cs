﻿using UnityEngine;
using System.Collections;

public class GameOverManager : MonoBehaviour {

    public bool gameIsOver;
	
	// Update is called once per frame
	void Update () 
	{
        if (gameIsOver)
        {
            PlayerBehavior p = GameObject.FindObjectOfType<PlayerBehavior>();
            p.hasFinished = true;
        }
	}
}
