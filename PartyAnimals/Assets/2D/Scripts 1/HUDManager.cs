﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HUDManager : MonoBehaviour {

	public Image[] KeyImages;

	Color DisabledColor;

	GameObject Player;

	public Text TimeToEndDisplay;

	public float TimeToEnd, TimeToStart;
    float setTime;
	GameObject GameManager;

	bool Started;

	public Image HealthDisplayImage, TimerDisplayImage;

	// Use this for initialization
	void Start () 
	{
		Started = false;
        setTime = TimeToEnd;
		Player = GameObject.FindGameObjectWithTag ("Player");

		GameManager = GameObject.FindGameObjectWithTag ("Game Manager");

		DisabledColor = new Color (1, 1, 1, 0.3f);

		foreach (Image Key in KeyImages) 
		{
			Key.color = DisabledColor;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		TimeToEndDisplay.text = "" + (int)TimeToEnd;

		if (TimeToEnd > 0 && Started) 
		{
			TimeToEnd -= 1 * Time.deltaTime;
		}
		if (TimeToEnd < 0) 
		{
			TimeToEnd = 0;
			//DO GAME END STUFF HERE
			//check if player got a power up to then use in 3d world?
			MultiplayerManager.Instance.RoundCount();
			SceneManager.LoadScene(1);
		}
		if (TimeToStart > 0) 
		{
			TimeToStart -= 1 * Time.deltaTime;
		}
		if (TimeToStart < 0) 
		{
			TimeToStart = 0;
			Started = true;
		}
		HealthDisplayImage.fillAmount = Player.GetComponent<PlayerBehavior> ().healthViewer;
        float timeFillAmount = TimeToEnd / setTime;
        TimerDisplayImage.fillAmount = timeFillAmount;
	}
}
