﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MoveToLeft : MonoBehaviour {

    private Image bgImage;

    void Start()
    {
        bgImage = GetComponent<Image>();
        bgImage.rectTransform.anchoredPosition = new Vector3(-Screen.width + bgImage.rectTransform.sizeDelta.x, 0, 0);
    }
}
